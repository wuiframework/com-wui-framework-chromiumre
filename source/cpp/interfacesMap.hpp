/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace ChromiumRE {
                class Application;
                class Loader;
                namespace Browser {
                    class BrowserWindow;
                    class BrowserWindowStdLinux;
                    class BrowserWindowStdMac;
                    class BrowserWindowStdWin;
                    class ClientApplicationBrowser;
                    class ClientHandler;
                    class ClientHandlerStd;
                    class ClientHandlerStdLinux;
                    class DebugWindow;
                    class DialogHandlerLinux;
                    class MainContext;
                    class MainContextImpl;
                    class MainMessageLoop;
                    class MainMessageLoopStd;
                    class NotifyIcon;
                    class NotifyIconLinux;
                    class NotifyIconMac;
                    class NotifyIconWin;
                    class RootWindow;
                    class RootWindowLinux;
                    class RootWindowMac;
                    class RootWindowManager;
                    class RootWindowWin;
                    class TaskBar;
                    class TaskBarMac;
                    class TaskBarWin;
                    class WindowTestRunner;
                    class WindowTestRunnerLinux;
                    class WindowTestRunnerMac;
                    class WindowTestRunnerWin;
                }
                namespace Commons {
                    class ClientApplication;
                    class ClientApplicationHybrid;
                    class ClientApplicationOther;
                    class Configuration;
                    template<class T> class CookieElement;
                    class CookieVisitor;
                    class DialogCallback;
                    class SchemeTestCommon;
                    class SourceVisitor;
                    class TestCookieCompletionCallback;
                    class TestSetCookieCallback;
                    class WaitableEvent;
                    class WindowPosition;
                }
                namespace Connectors {
                    class QueryContentHandler;
                    class QueryResponse;
                    class WindowHandler;
                    namespace Detail {
                        class WindowHandlerFileDialog;
                        class WindowHandlerNotifyBalloon;
                        class WindowHandlerNotifyIcon;
                        class WindowHandlerNotifyIconContextMenu;
                        class WindowHandlerNotifyIconContextMenuItem;
                    }
                }
                namespace Enums {
                    class NotifyBalloonIconType;
                    enum ProcessType : int;
                    class TaskBarProgressState;
                    class WindowStateType;
                }
                namespace Interfaces {
                    namespace Delegates {
                        class IBrowserWindowDelegate;
                        class IClientApplicationDelegate;
                        class IClientDelegate;
                        class IClientRendererDelegate;
                        class IWindowDelegate;
                    }
                }
                namespace Renderer {
                    class ClientApplicationRenderer;
                    class ClientRenderer;
                }
                namespace Structures {
                    class ChromiumArgs;
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACESMAP_HPP_  // NOLINT
