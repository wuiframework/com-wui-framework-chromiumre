/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_APPLICATION_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_APPLICATION_HPP_

namespace Com::Wui::Framework::ChromiumRE {
    /**
     * Application class contains chromiumre business logic.
     */
    class Application : public Com::Wui::Framework::XCppCommons::Application {
     public:
        int Run(int $argc, const char **$argv) override;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_APPLICATION_HPP_
