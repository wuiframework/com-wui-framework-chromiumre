/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Structures {
    namespace po = boost::program_options;

    ChromiumArgs::ChromiumArgs() {
        po::options_description appDescription("WuiChromiumRE options");

        appDescription.add_options()
                ("target", po::value<string>(&this->targetFilePath)->default_value(this->targetFilePath),
                 "Select target file path. Path could be absolute path of demand .html file or directory with index.html. "
                         "Target can also contains URL query and hash, but they will be replaced with --query or --hash if set.")
                ("connector-pid", po::value<int>(&this->connectorPID)->default_value(this->connectorPID),
                 "Attach connector to chromiumRE instance. Will be requested to closed before chromiumRE return.")
                ("query", po::value<string>(&this->query)->default_value(this->query),
                 "Specify query string for target page. Uri encoding will be applied by chromium but better approach"
                         " is to use correct format in argument.")
                ("hash", po::value<string>(&this->hash)->default_value(this->hash),
                 "Specify hash to be used in target URL.")
                ("remote-debugging-port", po::value<int>(&this->remoteDebuggingPort)->default_value(this->connectorPID),
                 "Specify remote debugging port for chrome debugger, then open http://localhost:<port> to open inspector from other"
                         "chrome browser.");

        this->getDescription()->add(appDescription);
    }

    const string &ChromiumArgs::getTarget() const {
        return this->targetFilePath;
    }

    int ChromiumArgs::getConnectorPID() const {
        return this->connectorPID;
    }

    const string &ChromiumArgs::getQuery() const {
        return this->query;
    }

    const string &ChromiumArgs::getHash() const {
        return this->hash;
    }

    int ChromiumArgs::getRemoteDebuggingPort() const {
        return this->remoteDebuggingPort;
    }

    void ChromiumArgs::setRemoteDebuggingPort(int $remoteDebuggingPort) {
        this->remoteDebuggingPort = $remoteDebuggingPort;
    }
}
