/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_STRUCTURES_CHROMIUMARGS_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_STRUCTURES_CHROMIUMARGS_HPP_

namespace Com::Wui::Framework::ChromiumRE::Structures {
    /**
     * ChromiumArgs class provides program options used in WuiChromiumRE application
     */
    class ChromiumArgs
            : public Com::Wui::Framework::XCppCommons::Structures::ProgramArgs {
     public:
        /**
         * Constructs args instance with configuration of all program options.
         */
        ChromiumArgs();

        /**
         * @return Returns specified application target path.
         */
        const string &getTarget() const;

        /**
         * @return Returns process ID of attached connector instance.
         */
        int getConnectorPID() const;

        /**
         * @return Returns URL query (attributes) to be used for target application page, will replace query items
         * specified in target if set.
         */
        const string &getQuery() const;

        /**
         * @return Returns URL hash (attributes) to be used for target application page, will replace query items
         * specified in target if set.
         */
        const string &getHash() const;

        int getRemoteDebuggingPort() const;

        void setRemoteDebuggingPort(int $remoteDebuggingPort);

     private:
        string targetFilePath = "./target";
        int connectorPID = 0;
        string query;
        string hash;
        int remoteDebuggingPort = 0;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_STRUCTURES_CHROMIUMARGS_HPP_
