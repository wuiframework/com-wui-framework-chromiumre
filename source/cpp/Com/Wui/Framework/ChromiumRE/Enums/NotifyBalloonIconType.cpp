/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Primitives::BaseEnum;
using Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType;

WUI_ENUM_IMPLEMENT(NotifyBalloonIconType);

WUI_ENUM_CONST_IMPLEMENT(NotifyBalloonIconType, NONE);
WUI_ENUM_CONST_IMPLEMENT(NotifyBalloonIconType, INFO);
WUI_ENUM_CONST_IMPLEMENT(NotifyBalloonIconType, WARNING);
WUI_ENUM_CONST_IMPLEMENT(NotifyBalloonIconType, ERROR);
WUI_ENUM_CONST_IMPLEMENT(NotifyBalloonIconType, USER);
