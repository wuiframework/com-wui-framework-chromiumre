/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_

#ifdef ERROR
#undef ERROR
#endif

namespace Com::Wui::Framework::ChromiumRE::Enums {
    class TaskBarProgressState
            : public Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState> {
     WUI_ENUM_DECLARE(TaskBarProgressState);
     public:
        static const TaskBarProgressState NOPROGRESS;
        static const TaskBarProgressState INDETERMINATE;
        static const TaskBarProgressState NORMAL;
        static const TaskBarProgressState ERROR;
        static const TaskBarProgressState PAUSED;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_
