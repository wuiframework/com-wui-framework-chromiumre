/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_PROCESSTYPE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_PROCESSTYPE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Enums {
    enum ProcessType : int {
        BrowserProcess,
        RendererProcess,
        ZygoteProcess,
        OtherProcess
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_PROCESSTYPE_HPP_
