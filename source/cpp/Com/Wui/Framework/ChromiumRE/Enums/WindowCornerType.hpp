/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWCORNERTYPE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWCORNERTYPE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Enums {
    enum class WindowCornerType : unsigned char {
        TOP_LEFT = 0,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWCORNERTYPE_HPP_
