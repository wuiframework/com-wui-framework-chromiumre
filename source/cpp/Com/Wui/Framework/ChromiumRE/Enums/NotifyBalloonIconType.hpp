/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_

#ifdef ERROR
#undef ERROR
#endif

namespace Com::Wui::Framework::ChromiumRE::Enums {
    class NotifyBalloonIconType
            : public Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType> {
     WUI_ENUM_DECLARE(NotifyBalloonIconType)

     public:
        static const NotifyBalloonIconType NONE;
        static const NotifyBalloonIconType INFO;
        static const NotifyBalloonIconType WARNING;
        static const NotifyBalloonIconType ERROR;
        static const NotifyBalloonIconType USER;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_
