/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Enums {
    class WindowStateType
            : public Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<Com::Wui::Framework::ChromiumRE::Enums::WindowStateType> {
     WUI_ENUM_DECLARE(WindowStateType);
     public:
        static const WindowStateType MINIMIZED;
        static const WindowStateType NORMAL;
        static const WindowStateType MAXIMIZED;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_
