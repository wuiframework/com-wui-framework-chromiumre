/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTTYPES_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTTYPES_HPP_

// TODO(nxf45876): Re-factor to strong C++ types

#ifdef LINUX_PLATFORM
#include <gtk/gtk.h>
#define ClientWindowHandle GtkWidget *
#elif MAC_PLATFORM
#define ClientWindowHandle BorderlessWindow *
#elif WIN_PLATFORM
#define ClientWindowHandle CefWindowHandle
#endif
#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTTYPES_HPP_

