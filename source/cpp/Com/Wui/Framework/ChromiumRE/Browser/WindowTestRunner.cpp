/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;
    using Com::Wui::Framework::ChromiumRE::Browser::MainContext;
    using Com::Wui::Framework::ChromiumRE::Browser::RootWindow;
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;
    using Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent;
    using Com::Wui::Framework::ChromiumRE::Commons::TestCookieCompletionCallback;
    using Com::Wui::Framework::ChromiumRE::Commons::CookieVisitor;
    using Com::Wui::Framework::ChromiumRE::Commons::DialogCallback;
    using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon;
    using Com::Wui::Framework::XCppCommons::Primitives::String;

    void WindowTestRunner::MoveTo(CefRefPtr<CefBrowser> $browser, int $x, int $y, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$x, $y](scoped_refptr<RootWindow> rootWindow) {
            const auto bounds = WindowTestRunner::getRectangleForMove(rootWindow->getWindowHandle(), $x, $y);

            rootWindow->SetBounds(bounds.x, bounds.y, bounds.width, bounds.height);

            return true;
        });
    }

    void WindowTestRunner::Minimize(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Minimize();

            return true;
        });
    }

    void WindowTestRunner::Maximize(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Maximize();

            return true;
        });
    }

    void WindowTestRunner::Restore(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Restore();

            return true;
        });
    }

    void WindowTestRunner::Resize(CefRefPtr<CefBrowser> $browser, WindowCornerType $fixedCorner, int $dx, int $dy,
                                  const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$dx, $dy, $fixedCorner](scoped_refptr<RootWindow> rootWindow) {
            const auto bounds = WindowTestRunner::getRectangleForResize(rootWindow->getWindowHandle(), $dx, $dy, $fixedCorner);

            rootWindow->SetBounds(bounds.x, bounds.y, bounds.width, bounds.height);

            return true;
        });
    }

    void WindowTestRunner::CanResize(CefRefPtr<CefBrowser> $browser, bool $canResize, const shared_ptr<QueryResponse> $response) {
        Configuration::getInstance().setCanResize($canResize);

        if ($response != nullptr) {
            $response->Send(true);
        }
    }

    void WindowTestRunner::getWindowState(CefRefPtr<CefBrowser> $browser, const string &$window,
                                          const shared_ptr<QueryResponse> $response) {
        const string id = $window.empty() ? "root" : $window;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById(id);

        if (rootWindow != nullptr && $response != nullptr) {
            $response->Send(rootWindow->getWindowState().toString());
        }
    }

    void WindowTestRunner::Open(CefRefPtr<CefBrowser> $browser, const string &$url, const json &$options,
                                const shared_ptr<QueryResponse> $response) {
        MainContext::Get()->getRootWindowManager()->CreateRootWindow(CefRect(), $url, $options, $response);
    }

    void WindowTestRunner::ScriptExecute(CefRefPtr<CefBrowser> $browser, const string &$window, const json &$options,
                                         const shared_ptr<QueryResponse> $response) {
        const string id = $window.empty() ? "root" : $window;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById(id);

        if (rootWindow != nullptr) {
            rootWindow->ExecuteScript($options, $response);
        } else {
            $response->Send(false);
        }
    }

    void WindowTestRunner::Close(CefRefPtr<CefBrowser> $browser, const string &$window,
                                 const shared_ptr<QueryResponse> $response) {
        if ($window.empty()) {
            MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);

            // TODO(nxf45876): Instead of calling the RootWindow::Close(), the RootWindowManager::Close() should exists, that would
            // perform some additional operations like destruction of window on OS X, but we don't have time now to change the architecture.
            // NotifyDestroyIfDone() is not called on OS X due to the bug in single-process mode on OS X
            WindowTestRunner::performOnCloseActions(MainContext::Get()->getRootWindowManager()->getWindowById("root"));
        } else {
            bool status = false;
            const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById($window);
            if (rootWindow != nullptr) {
                rootWindow->Close(true);
                WindowTestRunner::performOnCloseActions(rootWindow);

                status = true;
            }

            if ($response != nullptr) {
                $response->Send(status);
            }
        }
    }

    void WindowTestRunner::getCookies(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById($window);

        if (rootWindow != nullptr) {
            $browser = rootWindow->getBrowser();
        }

        WaitableEvent event;
        CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager(new TestCookieCompletionCallback(&event));
        event.Wait();

        string url = $browser->GetMainFrame()->GetURL().ToString();
        if (!String::StartsWith(url, "file://")) {
            url = String::Remove(url, "http://");
            url = String::Remove(url, "https://");
            url = String::Remove(url, "www.");
            unsigned int index = url.find("/");
            if (index > 0) {
                url = url.substr(0, index);
            }
        } else {
            url = "chromiumre.wuiframework.com";
        }

        std::vector<CefCookie> cookies;
        manager->VisitAllCookies(new CookieVisitor(&cookies, &event, false));
        event.Wait();

        json ret = json::array();
        for (size_t i = 0; i < cookies.size(); i++) {
            CefCookie *cookie = &cookies.at(i);
            if (String::Contains(CefString(&cookie->domain).ToString(), url)) {
                ret.insert(ret.end(), json({
                                               {"name",  CefString(&cookie->name).ToString()},
                                               {"value", CefString(&cookie->value).ToString()},
                                               {"path",  CefString(&cookie->path).ToString()}
                                           }));
            }
        }

        if ($response != nullptr) {
            $response->Send(ret);
        }
    }

    void WindowTestRunner::Show(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Show(WindowStateType::NORMAL);

            return true;
        });
    }

    void WindowTestRunner::Hide(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Hide();

            return true;
        });
    }

    void WindowTestRunner::CreateNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                            const shared_ptr<QueryResponse> $response) {
        WindowHandlerNotifyIcon notifyIconOptions;
        notifyIconOptions.Update($options);
        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->CreateNotifyIcon(
                notifyIconOptions, $response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::ModifyNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                            const shared_ptr<QueryResponse> $response) {
        WindowHandlerNotifyIcon notifyIconOptions;
        notifyIconOptions.Update($options);
        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->ModifyNotifyIcon(
                notifyIconOptions, $response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::DestroyNotifyIcon(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response) {
        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->
                DestroyNotifyIcon($response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::setTaskBarProgressState(CefRefPtr<CefBrowser> $browser,
                                                   const Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState $state,
                                                   const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$state](scoped_refptr<RootWindow> rootWindow) {
            WindowTestRunner::createTaskBarIfNotExists(rootWindow);

            if (rootWindow->getTaskBar() != nullptr) {
                return rootWindow->getTaskBar()->setProgressState($state);
            }
        });
    }

    void WindowTestRunner::setTaskBarProgressValue(CefRefPtr<CefBrowser> $browser, const int $completed, const int $total,
                                                   const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$completed, $total](scoped_refptr<RootWindow> rootWindow) {
            WindowTestRunner::createTaskBarIfNotExists(rootWindow);

            if (rootWindow->getTaskBar() != nullptr) {
                return rootWindow->getTaskBar()->setProgressValue($completed, $total);
            }
        });
    }

    void WindowTestRunner::ShowFileDialog(CefRefPtr<CefBrowser> $browser, const json &$settings,
                                          const shared_ptr<QueryResponse> $response) {
        Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerFileDialog fileDialog;
        fileDialog.Update($settings);

        cef_file_dialog_mode_t dialogMode;

        if (fileDialog.isFolderOnly()) {
            dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN_FOLDER;
        } else {
            if (fileDialog.isMultiSelect()) {
                dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN_MULTIPLE;
            } else if (fileDialog.isOpenOnly()) {
                dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN;
            } else {
                dialogMode = static_cast<cef_file_dialog_mode_t>(
                        CefBrowserHost::FileDialogMode::FILE_DIALOG_SAVE |
                        CefBrowserHost::FileDialogMode::FILE_DIALOG_OVERWRITEPROMPT_FLAG);
            }
        }

        std::vector<CefString> acceptFilters;
        std::for_each(fileDialog.getFilter().begin(), fileDialog.getFilter().end(), [&](const string &$item) {
            acceptFilters.push_back($item);
        });
        string initDir = fileDialog.getInitialDirectory();
        using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;

        if (!initDir.empty() && !FileSystem::Exists(initDir)) {
#ifdef WIN_PLATFORM
            char *buff = getenv("HOMEPATH");
            char *buff2 = getenv("HOMEDRIVE");
            if (buff != nullptr && buff2 != nullptr) {
                initDir = string(buff2) + string(buff) + "\\";
            }
            if (!FileSystem::Exists(initDir)) {
                initDir = FileSystem::getTempPath() + "\\";
            }
#else
            char *buff = getenv("HOME");
            if (buff != nullptr) {
                initDir = string(buff);
            }
            if (!FileSystem::Exists(initDir)) {
                initDir = FileSystem::getTempPath() + "/";
            }
#endif
        }

        boost::filesystem::path filePath(fileDialog.getPath());
        if (!filePath.empty()) {
            if (filePath.is_absolute()) {
                if (FileSystem::Exists(filePath.string())) {
                    initDir = filePath.string();
                } else {
                    initDir += filePath.filename().string();
                }
            } else {
                initDir += fileDialog.getPath();
            }
        }

        boost::filesystem::path initDirPath(initDir);
        $browser->GetHost()->RunFileDialog(dialogMode, fileDialog.getTitle(),
                                           initDirPath.normalize().make_preferred().string(),
                                           acceptFilters, fileDialog.getFilterIndex(),
                                           new DialogCallback($response));
    }

    void WindowTestRunner::ShowDebugConsole(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response) {
        bool status = false;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById("root");

        if (Com::Wui::Framework::ChromiumRE::Commons::Configuration::getInstance().getRemoteDebuggingPort() > 0) {
            MainContext::Get()->getRootWindowManager()->CreateDebugWindow($browser);
            status = true;
        }

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::performAction(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response,
                                         std::function<bool(scoped_refptr<RootWindow>)> $callback) {
        bool status = false;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier());

        if (rootWindow != nullptr) {
            const auto windowHandle = rootWindow->getWindowHandle();

            if (windowHandle != nullptr) {
                status = $callback(rootWindow);
            }
        }

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::createTaskBarIfNotExists(scoped_refptr<RootWindow> $rootWindow) {
        if ($rootWindow->getTaskBar() == nullptr) {
            shared_ptr<TaskBar> taskBar(TaskBar::Create($rootWindow));
            $rootWindow->setTaskBar(taskBar);
        }
    }
}
