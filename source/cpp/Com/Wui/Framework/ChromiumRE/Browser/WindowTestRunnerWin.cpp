/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Browser::TaskBar;
    using Com::Wui::Framework::ChromiumRE::Browser::TaskBarWin;
    using Com::Wui::Framework::ChromiumRE::Enums::WindowStateType;

    CefRect WindowTestRunner::getRectangleForMove(ClientWindowHandle $handle, int $x, int $y) {
        RECT rectangle = { 0 };
        GetWindowRect($handle, &rectangle);

        return { $x, $y, rectangle.right - rectangle.left, rectangle.bottom - rectangle.top };
    }

    CefRect WindowTestRunner::getRectangleForResize(ClientWindowHandle $handle, int $dx, int $dy, WindowCornerType $fixedCorner) {
        RECT currentFrame = { 0 };
        GetWindowRect($handle, &currentFrame);
        CefRect updatedFrame;

        switch ($fixedCorner) {
            case WindowCornerType::TOP_LEFT:
                updatedFrame = {
                    currentFrame.left + $dx,
                    currentFrame.top,
                    currentFrame.right - currentFrame.left - $dx,
                    currentFrame.bottom - currentFrame.top + $dy };
            case WindowCornerType::TOP_RIGHT:
                updatedFrame = {
                    currentFrame.left + $dx,
                    currentFrame.top,
                    currentFrame.right - currentFrame.left - $dx,
                    currentFrame.bottom - currentFrame.top + $dy };
            case WindowCornerType::BOTTOM_LEFT:
                updatedFrame = {
                    currentFrame.left,
                    currentFrame.top + $dy,
                    currentFrame.right - currentFrame.left + $dx,
                    currentFrame.bottom - currentFrame.top - $dy };
            case WindowCornerType::BOTTOM_RIGHT:
                updatedFrame = {
                    currentFrame.left + $dx,
                    currentFrame.top + $dy,
                    currentFrame.right - currentFrame.left - $dx,
                    currentFrame.bottom - currentFrame.top - $dy };
        }

        return updatedFrame;
    }

    void WindowTestRunner::performOnCloseActions(RootWindow *$rootWindow) {
    }
}

#endif  // WIN_PLATFORM
