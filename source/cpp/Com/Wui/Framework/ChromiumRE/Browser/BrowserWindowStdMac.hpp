/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_

#ifdef MAC_PLATFORM

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class BrowserWindowStdMac
            : public Com::Wui::Framework::ChromiumRE::Browser::BrowserWindow {
     public:
        BrowserWindowStdMac(Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *$delegate,
                            const string &$startupUrl);

        void CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect, const CefBrowserSettings &$settings,
                           const CefString &$url, CefRefPtr<CefRequestContext> $requestContext) override;

        void Show() override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        ClientWindowHandle GetWindowHandle() const override;

     private:
        DISALLOW_COPY_AND_ASSIGN(BrowserWindowStdMac);
    };
}

#endif  // MAC_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_
