/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#include <iomanip>  // NOLINT
#include <sstream>  // NOLINT

#include "include/base/cef_bind.h"
#include "include/cef_browser.h"
#include "include/cef_frame.h"
#include "include/cef_parser.h"
#include "include/wrapper/cef_closure_task.h"

using Com::Wui::Framework::ChromiumRE::Commons::Configuration;
using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientDelegate;

#if defined(OS_WIN)
#define NEWLINE "\r\n"
#else
#define NEWLINE "\n"
#endif

namespace {

    const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

    string GetTimeString(const CefTime &$value) {
        if ($value.GetTimeT() == 0)
            return "Unspecified";

        static const char *kMonths[] = {
                "January", "February", "March", "April", "May", "June", "July", "August",
                "September", "October", "November", "December"
        };
        string month;
        if ($value.month >= 1 && $value.month <= 12)
            month = kMonths[$value.month - 1];
        else
            month = "Invalid";

        std::stringstream ss;
        ss << month << " " << $value.day_of_month << ", " << $value.year << " " <<
           std::setfill('0') << std::setw(2) << $value.hour << ":" <<
           std::setfill('0') << std::setw(2) << $value.minute << ":" <<
           std::setfill('0') << std::setw(2) << $value.second;
        return ss.str();
    }

    string GetBinaryString(CefRefPtr<CefBinaryValue> $value) {
        if (!$value.get()) {
            return "&nbsp;";
        }

        const size_t size = $value->GetSize();
        string src;
        src.resize(size);
        $value->GetData(const_cast<char *>(src.data()), size, 0);

        return CefBase64Encode(src.data(), src.size());
    }

    string GetErrorString(cef_errorcode_t $code) {
#define CASE(code) case code: return #code

        switch ($code) {
            CASE(ERR_NONE);
            CASE(ERR_FAILED);
            CASE(ERR_ABORTED);
            CASE(ERR_INVALID_ARGUMENT);
            CASE(ERR_INVALID_HANDLE);
            CASE(ERR_FILE_NOT_FOUND);
            CASE(ERR_TIMED_OUT);
            CASE(ERR_FILE_TOO_BIG);
            CASE(ERR_UNEXPECTED);
            CASE(ERR_ACCESS_DENIED);
            CASE(ERR_NOT_IMPLEMENTED);
            CASE(ERR_CONNECTION_CLOSED);
            CASE(ERR_CONNECTION_RESET);
            CASE(ERR_CONNECTION_REFUSED);
            CASE(ERR_CONNECTION_ABORTED);
            CASE(ERR_CONNECTION_FAILED);
            CASE(ERR_NAME_NOT_RESOLVED);
            CASE(ERR_INTERNET_DISCONNECTED);
            CASE(ERR_SSL_PROTOCOL_ERROR);
            CASE(ERR_ADDRESS_INVALID);
            CASE(ERR_ADDRESS_UNREACHABLE);
            CASE(ERR_SSL_CLIENT_AUTH_CERT_NEEDED);
            CASE(ERR_TUNNEL_CONNECTION_FAILED);
            CASE(ERR_NO_SSL_VERSIONS_ENABLED);
            CASE(ERR_SSL_VERSION_OR_CIPHER_MISMATCH);
            CASE(ERR_SSL_RENEGOTIATION_REQUESTED);
            CASE(ERR_CERT_COMMON_NAME_INVALID);
            CASE(ERR_CERT_DATE_INVALID);
            CASE(ERR_CERT_AUTHORITY_INVALID);
            CASE(ERR_CERT_CONTAINS_ERRORS);
            CASE(ERR_CERT_NO_REVOCATION_MECHANISM);
            CASE(ERR_CERT_UNABLE_TO_CHECK_REVOCATION);
            CASE(ERR_CERT_REVOKED);
            CASE(ERR_CERT_INVALID);
            CASE(ERR_CERT_END);
            CASE(ERR_INVALID_URL);
            CASE(ERR_DISALLOWED_URL_SCHEME);
            CASE(ERR_UNKNOWN_URL_SCHEME);
            CASE(ERR_TOO_MANY_REDIRECTS);
            CASE(ERR_UNSAFE_REDIRECT);
            CASE(ERR_UNSAFE_PORT);
            CASE(ERR_INVALID_RESPONSE);
            CASE(ERR_INVALID_CHUNKED_ENCODING);
            CASE(ERR_METHOD_NOT_SUPPORTED);
            CASE(ERR_UNEXPECTED_PROXY_AUTH);
            CASE(ERR_EMPTY_RESPONSE);
            CASE(ERR_RESPONSE_HEADERS_TOO_BIG);
            CASE(ERR_CACHE_MISS);
            CASE(ERR_INSECURE_RESPONSE);
            default:
                return "UNKNOWN";
        }
    }

    string GetCertStatusString(cef_cert_status_t $status) {
#define FLAG(flag) if ($status & flag) result += string(#flag) + "<br/>"
        string result;

        FLAG(CERT_STATUS_COMMON_NAME_INVALID);
        FLAG(CERT_STATUS_DATE_INVALID);
        FLAG(CERT_STATUS_AUTHORITY_INVALID);
        FLAG(CERT_STATUS_NO_REVOCATION_MECHANISM);
        FLAG(CERT_STATUS_UNABLE_TO_CHECK_REVOCATION);
        FLAG(CERT_STATUS_REVOKED);
        FLAG(CERT_STATUS_INVALID);
        FLAG(CERT_STATUS_WEAK_SIGNATURE_ALGORITHM);
        FLAG(CERT_STATUS_NON_UNIQUE_NAME);
        FLAG(CERT_STATUS_WEAK_KEY);
        FLAG(CERT_STATUS_PINNED_KEY_MISSING);
        FLAG(CERT_STATUS_NAME_CONSTRAINT_VIOLATION);
        FLAG(CERT_STATUS_VALIDITY_TOO_LONG);
        FLAG(CERT_STATUS_IS_EV);
        FLAG(CERT_STATUS_REV_CHECKING_ENABLED);
        FLAG(CERT_STATUS_SHA1_SIGNATURE_PRESENT);
        FLAG(CERT_STATUS_CT_COMPLIANCE_FAILED);

        if (result.empty())
            return "&nbsp;";
        return result;
    }

    void LoadErrorPage(CefRefPtr<CefFrame> $frame, const string &$failedUrl, cef_errorcode_t $errorCode, const string &$otherInfo) {
        std::stringstream ss;
        ss << "<html><head><title>Page failed to load</title></head>"
                "<body bgcolor=\"white\">"
                "<h3>Page failed to load.</h3>"
                "URL: <a href=\"";
        ss << $failedUrl << "\">" << $failedUrl << "</a>"
                "<br/>Error: " << GetErrorString($errorCode) <<
           " (" << $errorCode << ")";

        if (!$otherInfo.empty()) {
            ss << "<br/>";
            ss << $otherInfo;
        }

        ss << "</body></html>";
    }
}  // namespace

namespace Com::Wui::Framework::ChromiumRE::Browser {

    class ClientDownloadImageCallback : public CefDownloadImageCallback {
     public:
        explicit ClientDownloadImageCallback(CefRefPtr<ClientHandler> $clientHandler)
                : clientHandler($clientHandler) {
        }

        void OnDownloadImageFinished(const CefString &$imageUrl, int $httpStatusCode, CefRefPtr<CefImage> $image) override {
        }

     private:
        CefRefPtr<ClientHandler> clientHandler;

        /**/IMPLEMENT_REFCOUNTING(ClientDownloadImageCallback);
        DISALLOW_COPY_AND_ASSIGN(ClientDownloadImageCallback);
    };

    ClientHandler::ClientHandler(IClientDelegate *$delegate, const string &$startupUrl)
            : startupUrl($startupUrl),
              downloadFaviconImages(false),
              delegate($delegate),
              browserCount(0),
              consoleLogFile(MainContext::Get()->getConsoleLogPath()),
              firstConsoleMessage(true),
              focusOnEditableField(false) {
        DCHECK(!consoleLogFile.empty());

        resourceManager = new CefResourceManager();
        mouseCursorChangeDisabled = false;
    }

    void ClientHandler::DetachDelegate() {
        if (!CURRENTLY_ON_MAIN_THREAD()) {
            MAIN_POST_CLOSURE(base::Bind(&ClientHandler::DetachDelegate, this));
            return;
        }

        DCHECK(this->delegate);
        this->delegate = nullptr;
    }

    bool ClientHandler::OnProcessMessageReceived(CefRefPtr<CefBrowser> $browser, CefProcessId $sourceProcess,
                                                 CefRefPtr<CefProcessMessage> $message) {
        CEF_REQUIRE_UI_THREAD();

        if (this->messageRouter->OnProcessMessageReceived($browser, $sourceProcess, $message)) {
            return true;
        }

        string messageName = $message->GetName();
        if (messageName == kFocusedNodeChangedMessage) {
            this->focusOnEditableField = $message->GetArgumentList()->GetBool(0);
            return true;
        }

        return false;
    }

    void ClientHandler::OnBeforeContextMenu(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                            CefRefPtr<CefContextMenuParams> $params, CefRefPtr<CefMenuModel> $model) {
        CEF_REQUIRE_UI_THREAD();

        if (($params->GetTypeFlags() & (CM_TYPEFLAG_PAGE | CM_TYPEFLAG_FRAME)) != 0) {
            $model->Clear();
        }
    }

    bool ClientHandler::OnContextMenuCommand(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                             CefRefPtr<CefContextMenuParams> $params, int $commandId, EventFlags $eventFlags) {
        CEF_REQUIRE_UI_THREAD();

        return false;
    }

    void ClientHandler::OnAddressChange(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$url) {
        CEF_REQUIRE_UI_THREAD();

        if ($frame->IsMain()) {
            NotifyAddress($url);
        }
    }

    void ClientHandler::OnTitleChange(CefRefPtr<CefBrowser> $browser, const CefString &$title) {
        CEF_REQUIRE_UI_THREAD();

        NotifyTitle($title);
    }

    void ClientHandler::OnFaviconURLChange(CefRefPtr<CefBrowser> $browser, const std::vector<CefString> &$iconUrls) {
        CEF_REQUIRE_UI_THREAD();

        if (!$iconUrls.empty() && this->downloadFaviconImages) {
            $browser->GetHost()->DownloadImage($iconUrls[0], true, 16, false, new ClientDownloadImageCallback(this));
        }
    }

    void ClientHandler::OnFullscreenModeChange(CefRefPtr<CefBrowser> $browser, bool $fullScreen) {
        CEF_REQUIRE_UI_THREAD();

        NotifyFullscreen($fullScreen);
    }

    bool ClientHandler::OnConsoleMessage(CefRefPtr<CefBrowser> $browser,  cef_log_severity_t $level, const CefString &$message,
                                         const CefString &$source, int $line) {
        CEF_REQUIRE_UI_THREAD();

        return false;
    }

    void ClientHandler::OnBeforeDownload(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDownloadItem> $downloadItem,
                                         const CefString &$suggestedName, CefRefPtr<CefBeforeDownloadCallback> $callback) {
        if (Configuration::getInstance().IsDownloadDialogEnabled()) {
            CEF_REQUIRE_UI_THREAD();
            $callback->Continue(MainContext::Get()->getDownloadPath($suggestedName), true);
        }
    }

    void ClientHandler::OnDownloadUpdated(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDownloadItem> $downloadItem,
                                          CefRefPtr<CefDownloadItemCallback> $callback) {
        CEF_REQUIRE_UI_THREAD();
    }

    bool ClientHandler::OnDragEnter(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDragData> $dragData,
                                    CefDragHandler::DragOperationsMask $mask) {
        CEF_REQUIRE_UI_THREAD();

        if ($mask & DRAG_OPERATION_LINK) {
            return true;
        }

        return false;
    }

    void ClientHandler::OnDraggableRegionsChanged(CefRefPtr<CefBrowser> $browser, const std::vector<CefDraggableRegion> &$regions) {
        CEF_REQUIRE_UI_THREAD();

        NotifyDraggableRegions($regions);
    }

    bool ClientHandler::OnPreKeyEvent(CefRefPtr<CefBrowser> $browser, const CefKeyEvent &$event, CefEventHandle $osEvent,
                                      bool *$isKeyboardShortcut) {
        CEF_REQUIRE_UI_THREAD();

        return false;
    }

    bool ClientHandler::OnBeforePopup(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$targetUrl,
                                      const CefString &$targetFrameName,
                                      CefLifeSpanHandler::WindowOpenDisposition $targetDisposition, bool $userGesture,
                                      const CefPopupFeatures &$popupFeatures, CefWindowInfo &$windowInfo,
                                      CefRefPtr<CefClient> &$client, CefBrowserSettings &$settings,
                                      bool *$noJavascriptAccess) {
        CEF_REQUIRE_IO_THREAD();

        MainContext::Get()->getRootWindowManager()->CreateRootWindow(CefRect(), $targetUrl.ToString());
        return true;
    }

    void ClientHandler::OnAfterCreated(CefRefPtr<CefBrowser> $browser) {
        CEF_REQUIRE_UI_THREAD();

        this->browserCount++;

        if (!this->messageRouter) {
            CefMessageRouterConfig config;
            this->messageRouter = CefMessageRouterBrowserSide::Create(config);

            Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler::RegisterMessageHandler(this->messageHandlerSet);

            MessageHandlerSet::const_iterator it = this->messageHandlerSet.begin();
            for (; it != this->messageHandlerSet.end(); ++it) {
                this->messageRouter->AddHandler(*(it), false);
            }
        }

        if (this->mouseCursorChangeDisabled) {
            $browser->GetHost()->SetMouseCursorChangeDisabled(true);
        }

        NotifyBrowserCreated($browser);
    }

    bool ClientHandler::DoClose(CefRefPtr<CefBrowser> $browser) {
        CEF_REQUIRE_UI_THREAD();

        NotifyBrowserClosing($browser);
        return false;
    }

    void ClientHandler::OnBeforeClose(CefRefPtr<CefBrowser> $browser) {
        CEF_REQUIRE_UI_THREAD();

        if (--this->browserCount == 0) {
            MessageHandlerSet::const_iterator it = this->messageHandlerSet.begin();
            for (; it != this->messageHandlerSet.end(); ++it) {
                this->messageRouter->RemoveHandler(*(it));
                delete *(it);
            }
            this->messageHandlerSet.clear();
            this->messageRouter = nullptr;
        }

        NotifyBrowserClosed($browser);
    }

    void ClientHandler::OnLoadingStateChange(CefRefPtr<CefBrowser> $browser, bool $isLoading, bool $canGoBack,
                                             bool $canGoForward) {
        CEF_REQUIRE_UI_THREAD();

        NotifyLoadingState($isLoading, $canGoBack, $canGoForward);
    }

    void ClientHandler::OnLoadError(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, ErrorCode $errorCode,
                                    const CefString &$errorText, const CefString &$failedUrl) {
        CEF_REQUIRE_UI_THREAD();

        if ($errorCode == ERR_ABORTED) {
            return;
        }

        if ($errorCode == ERR_UNKNOWN_URL_SCHEME) {
            string urlStr = $frame->GetURL();
            if (urlStr.find("spotify:") == 0) {
                return;
            }
        }

        LoadErrorPage($frame, $failedUrl, $errorCode, $errorText);
    }

    bool ClientHandler::OnBeforeBrowse(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                       CefRefPtr<CefRequest> $request, bool $userGesture, bool $isRedirect) {
        CEF_REQUIRE_UI_THREAD();

        this->messageRouter->OnBeforeBrowse($browser, $frame);
        return false;
    }

    bool ClientHandler::OnOpenURLFromTab(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$targetUrl,
                                         CefRequestHandler::WindowOpenDisposition $targetDisposition, bool $userGesture) {
        return false;
    }

    cef_return_value_t ClientHandler::OnBeforeResourceLoad(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                                           CefRefPtr<CefRequest> $request,
                                                           CefRefPtr<CefRequestCallback> $callback) {
        CEF_REQUIRE_IO_THREAD();

        return this->resourceManager->OnBeforeResourceLoad($browser, $frame, $request, $callback);
    }

    CefRefPtr<CefResourceHandler> ClientHandler::GetResourceHandler(CefRefPtr<CefBrowser> $browser,
                                                                    CefRefPtr<CefFrame> $frame,
                                                                    CefRefPtr<CefRequest> $request) {
        CEF_REQUIRE_IO_THREAD();

        return this->resourceManager->GetResourceHandler($browser, $frame, $request);
    }

    CefRefPtr<CefResponseFilter> ClientHandler::GetResourceResponseFilter(CefRefPtr<CefBrowser> $browser,
                                                                          CefRefPtr<CefFrame> $frame,
                                                                          CefRefPtr<CefRequest> $request,
                                                                          CefRefPtr<CefResponse> $response) {
        CEF_REQUIRE_IO_THREAD();

        return nullptr;
    }

    bool ClientHandler::OnQuotaRequest(CefRefPtr<CefBrowser> $browser, const CefString &$originUrl, int64 $newSize,
                                       CefRefPtr<CefRequestCallback> $callback) {
        CEF_REQUIRE_IO_THREAD();

        static const int64 maxSize = 1024 * 1024 * 20;

        $callback->Continue($newSize <= maxSize);
        return true;
    }

    void ClientHandler::OnProtocolExecution(CefRefPtr<CefBrowser> $browser, const CefString &$url, bool &$allowOsExecution) {
        CEF_REQUIRE_UI_THREAD();

        string urlStr = $url;

        if (urlStr.find("spotify:") == 0) {
            $allowOsExecution = true;
        }
    }

    bool ClientHandler::OnCertificateError(CefRefPtr<CefBrowser> $browser, ErrorCode $certError,
                                           const CefString &$requestUrl, CefRefPtr<CefSSLInfo> $sslInfo,
                                           CefRefPtr<CefRequestCallback> $callback) {
        CEF_REQUIRE_UI_THREAD();

        CefRefPtr<CefX509CertPrincipal> subject = $sslInfo->GetX509Certificate()->GetSubject();
        CefRefPtr<CefX509CertPrincipal> issuer = $sslInfo->GetX509Certificate()->GetIssuer();

        std::stringstream ss;
        ss << "X.509 Certificate Information:"
                "<table border=1><tr><th>Field</th><th>Value</th></tr>" <<
           "<tr><td>Subject</td><td>";
        ss << (subject.get() ? subject->GetDisplayName().ToString() : "&nbsp;") <<
           "</td></tr>"
                   "<tr><td>Issuer</td><td>" <<
           (issuer.get() ? issuer->GetDisplayName().ToString() : "&nbsp;") <<
           "</td></tr>"
                   "<tr><td>Serial #*</td><td>" <<
           GetBinaryString($sslInfo->GetX509Certificate()->GetSerialNumber()) << "</td></tr>"
                   "<tr><td>Status</td><td>" <<
           GetCertStatusString($sslInfo->GetCertStatus()) << "</td></tr>"
                   "<tr><td>Valid Start</td><td>" <<
           GetTimeString($sslInfo->GetX509Certificate()->GetValidStart()) << "</td></tr>"
                   "<tr><td>Valid Expiry</td><td>" <<
           GetTimeString($sslInfo->GetX509Certificate()->GetValidExpiry()) << "</td></tr>";

        CefX509Certificate::IssuerChainBinaryList derChainList;
        CefX509Certificate::IssuerChainBinaryList pemChainList;
        $sslInfo->GetX509Certificate()->GetDEREncodedIssuerChain(derChainList);
        $sslInfo->GetX509Certificate()->GetPEMEncodedIssuerChain(pemChainList);
        DCHECK_EQ(derChainList.size(), pemChainList.size());

        derChainList.insert(derChainList.begin(), $sslInfo->GetX509Certificate()->GetDEREncoded());
        pemChainList.insert(pemChainList.begin(), $sslInfo->GetX509Certificate()->GetPEMEncoded());

        for (size_t i = 0U; i < derChainList.size(); ++i) {
            ss << "<tr><td>DER Encoded*</td>"
                    "<td style=\"max-width:800px;overflow:scroll;\">";
            ss << GetBinaryString(derChainList[i]) << "</td></tr>"
                    "<tr><td>PEM Encoded*</td>"
                    "<td style=\"max-width:800px;overflow:scroll;\">" <<
               GetBinaryString(pemChainList[i]) << "</td></tr>";
        }

        ss << "</table> * Displayed value is base64 encoded.";

        LoadErrorPage($browser->GetMainFrame(), $requestUrl, $certError, ss.str());

        return false;
    }

    void ClientHandler::OnRenderProcessTerminated(CefRefPtr<CefBrowser> $browser, TerminationStatus $status) {
        CEF_REQUIRE_UI_THREAD();

        this->messageRouter->OnRenderProcessTerminated($browser);

        if (this->startupUrl.empty() || this->startupUrl == "chrome://crash") {
            return;
        }

        CefRefPtr<CefFrame> frame = $browser->GetMainFrame();
        string url = frame->GetURL();

        if (url.empty()) {
            return;
        }

        string tmpStartUrl = this->startupUrl;

        std::transform(url.begin(), url.end(), url.begin(), tolower);
        std::transform(tmpStartUrl.begin(), tmpStartUrl.end(), tmpStartUrl.begin(), tolower);

        if (url.find(tmpStartUrl) == 0) {
            return;
        }

        frame->LoadURL(this->startupUrl);
    }

    int ClientHandler::GetBrowserCount() const {
        CEF_REQUIRE_UI_THREAD();
        return this->browserCount;
    }

    void ClientHandler::ShowDevTools(CefRefPtr<CefBrowser> $browser, const CefPoint &$inspectElementAt) {
        if (!CefCurrentlyOn(TID_UI)) {
            CefPostTask(TID_UI, base::Bind(&ClientHandler::ShowDevTools, this, $browser, $inspectElementAt));
            return;
        }

        CefWindowInfo windowInfo;
        CefRefPtr<CefClient> client;
        CefBrowserSettings settings;

        CefRefPtr<CefBrowserHost> host = $browser->GetHost();

        bool hasDevtools = host->HasDevTools();
        if (hasDevtools) {
            host->ShowDevTools(windowInfo, client, settings, $inspectElementAt);
        }
    }

    void ClientHandler::CloseDevTools(CefRefPtr<CefBrowser> $browser) {
        $browser->GetHost()->CloseDevTools();
    }

    void ClientHandler::NotifyBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        // TODO(nxf45876): get rid of this code redundancy
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyBrowserCreated, this, $browser));
                return;
            }

            this->delegate->OnBrowserCreated($browser);
        }
    }

    void ClientHandler::NotifyBrowserClosing(CefRefPtr<CefBrowser> $browser) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyBrowserClosing, this, $browser));
                return;
            }

            this->delegate->OnBrowserClosing($browser);
        }
    }

    void ClientHandler::NotifyBrowserClosed(CefRefPtr<CefBrowser> $browser) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyBrowserClosed, this, $browser));
                return;
            }

            this->delegate->OnBrowserClosed($browser);
        }
    }

    void ClientHandler::NotifyAddress(const CefString &$url) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyAddress, this, $url));
                return;
            }

            this->delegate->OnSetAddress($url);
        }
    }

    void ClientHandler::NotifyTitle(const CefString &$title) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyTitle, this, $title));
                return;
            }

            this->delegate->OnSetTitle($title);
        }
    }

    void ClientHandler::NotifyFavicon(CefRefPtr<CefImage> $image) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyFavicon, this, $image));
                return;
            }

            this->delegate->OnSetFavicon($image);
        }
    }

    void ClientHandler::NotifyFullscreen(bool $fullscreen) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyFullscreen, this, $fullscreen));
                return;
            }

            this->delegate->OnSetFullscreen($fullscreen);
        }
    }

    void ClientHandler::NotifyLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyLoadingState, this, $isLoading, $canGoBack, $canGoForward));
                return;
            }

            this->delegate->OnSetLoadingState($isLoading, $canGoBack, $canGoForward);
        }
    }

    void ClientHandler::NotifyDraggableRegions(const std::vector<CefDraggableRegion> &$regions) {
        if (this->delegate != nullptr) {
            if (!CURRENTLY_ON_MAIN_THREAD()) {
                MAIN_POST_CLOSURE(base::Bind(&ClientHandler::NotifyDraggableRegions, this, $regions));
                return;
            }

            this->delegate->OnSetDraggableRegions($regions);
        }
    }
}
