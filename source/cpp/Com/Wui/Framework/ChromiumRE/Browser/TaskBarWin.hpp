/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARWIN_HPP_

#ifdef WIN_PLATFORM

#include "TaskBar.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class TaskBarWin : public TaskBar {
     public:
        explicit TaskBarWin(scoped_refptr<RootWindow> $owner);

        bool setProgressState(const Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState &$state) override;

        bool setProgressValue(const int $completed, const int $total) override;

        void Clear() override;

     private:
        ITaskbarList3 *taskBarList = nullptr;
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARWIN_HPP_
