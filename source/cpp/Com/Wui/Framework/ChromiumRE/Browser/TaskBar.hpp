/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBAR_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBAR_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class TaskBar {
     public:
        static TaskBar *Create(scoped_refptr<RootWindow> $owner);

        explicit TaskBar(scoped_refptr<RootWindow> $owner);

        virtual ~TaskBar() = default;

        scoped_refptr<RootWindow> getOwner() const;

        void setOwner(scoped_refptr<RootWindow> $owner);

        virtual bool setProgressState(const Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState &$state) = 0;

        virtual bool setProgressValue(const int $completed, const int $total) = 0;

        virtual void Clear() = 0;

     protected:
        scoped_refptr<RootWindow> owner = nullptr;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBAR_HPP_
