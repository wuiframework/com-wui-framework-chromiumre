/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

#include <gdk/gdkx.h>  // NOLINT
#include <X11/Xlib.h>  // NOLINT

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Enums::WindowStateType;
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;
    using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon;
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;
    using Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler;
    using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IWindowDelegate;
    using Com::Wui::Framework::ChromiumRE::Commons::WindowPosition;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;

    void RootWindowLinux::Show(WindowStateType /*$mode*/) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            if (this->options.empty() || !this->options.value("hidden", false)) {
                gtk_widget_show_all(this->handle);

                this->flushDisplay();
            }
        }
    }

    void RootWindowLinux::Hide() {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            gtk_widget_hide(this->handle);

            this->flushDisplay();
        }
    }

    void RootWindowLinux::SetBounds(int $x, int $y, size_t $width, size_t $height) {
        REQUIRE_MAIN_THREAD();

        if (this->canSetBounds()) {
            gtk_window_move(GTK_WINDOW(this->handle), $x, $y);
            gtk_window_resize(GTK_WINDOW(this->handle), $width, $height);
        }
    }

    void RootWindowLinux::Close(bool $force) {
        REQUIRE_MAIN_THREAD();

        RootWindow::Close($force);

        if (this->handle != nullptr) {
            GtkAllocation allocation;
            gtk_widget_get_allocation(this->handle, &allocation);
            int x = 0;
            int y = 0;
            gdk_window_get_origin(gtk_widget_get_window(this->handle), &x, &y);

            this->saveWindowPosition(x, y, allocation.width, allocation.height, this->getWindowState() == WindowStateType::MAXIMIZED);
        }

        this->closeBrowser();
    }

    void RootWindowLinux::createBrowserWindow(const string &$url) {
        this->browserWindow.reset(new BrowserWindowStdLinux(this, $url));
    }

    void RootWindowLinux::createRootWindow(const CefBrowserSettings &$settings) {
        REQUIRE_MAIN_THREAD();
        DCHECK(!this->handle);

        RootWindow::createRootWindow($settings);

        // TODO(nxf45876): handle more windows
        this->handle = gtk_window_new(GTK_WINDOW_TOPLEVEL);

        CHECK(this->handle);

        gtk_window_set_decorated(GTK_WINDOW(this->handle), FALSE);
        gtk_widget_set_events(
                this->handle, GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK |
                              GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK |
                              GDK_FOCUS_CHANGE_MASK | GDK_STRUCTURE_MASK);
        g_signal_connect(this->handle, "destroy", G_CALLBACK(&RootWindowLinux::OnDestroyed), this);
        g_signal_connect(this->handle, "delete-event", G_CALLBACK(&RootWindowLinux::OnClose), this);
        g_signal_connect(this->handle, "focus-in-event", G_CALLBACK(&RootWindowLinux::OnFocus), this);
        g_signal_connect(this->handle, "size-allocate", G_CALLBACK(&RootWindowLinux::OnSize), this);
        g_signal_connect(this->handle, "configure-event", G_CALLBACK(&RootWindowLinux::OnMove), this);
        g_signal_connect(this->handle, "window-state-event", G_CALLBACK(&RootWindowLinux::OnWindowStateChange), this);

        // TODO(nxf45876): handle secondary monitors according to window position
        CefRect displayRect(0, 0, gdk_screen_width(), gdk_screen_height());
        CefRect windowRect(this->winPosition.getX(), this->winPosition.getY(), this->winPosition.getWidth(), this->winPosition.getHeight());

        this->modifyBounds(displayRect, windowRect);

        this->winPosition.setX(windowRect.x);
        this->winPosition.setY(windowRect.y);
        this->winPosition.setWidth(windowRect.width);
        this->winPosition.setHeight(windowRect.height);

        this->SetBounds(this->winPosition.getX(), this->winPosition.getY(), this->winPosition.getWidth(), this->winPosition.getHeight());

        gtk_window_set_resizable(GTK_WINDOW(this->handle), Configuration::getInstance().IsCanResize());

        if (boost::iequals(this->id, "root") || this->isHidden) {
            this->Hide();
        }

        if (this->winPosition.getIsMaximized()) {
            this->updateWindowState(WindowStateType::MAXIMIZED);
        }

        // todo(nxf45876): Wait for proper realization
        gtk_widget_realize(this->handle);
        Display *xdisplay = GDK_WINDOW_XDISPLAY(gtk_widget_get_window(this->handle));
        static_cast<BrowserWindowStdLinux *>(this->browserWindow.get())->SetXdisplay(xdisplay);

        this->browserWindow->setIsHidden(this->isHidden);
        this->browserWindow->CreateBrowser(this->handle, windowRect, $settings, this->url, delegate->GetRequestContext(this));
    }

    void RootWindowLinux::createRootWindowWrapper(const CefBrowserSettings &$settings) {
        if (CURRENTLY_ON_MAIN_THREAD()) {
            createRootWindow($settings);
        } else {
            MAIN_POST_CLOSURE(base::Bind(&RootWindowLinux::createRootWindow, this, $settings));
        }
    }

    gboolean RootWindowLinux::OnFocus(GtkWidget *$widget, GdkEventFocus *$event, RootWindowLinux *$self) {
        CEF_REQUIRE_UI_THREAD();

        $self->onFocus();

        return FALSE;
    }

    void RootWindowLinux::OnSize(GtkWidget *$widget, GtkAllocation *$allocation, RootWindowLinux *$self) {
        CEF_REQUIRE_UI_THREAD();

        int x = 0;
        int y = 0;
        gdk_window_get_origin(gtk_widget_get_window($widget), &x, &y);

        WindowHandler::FireEvent("OnWindowChanged", json::array({json({
                                                                              {"x",      x},
                                                                              {"y",      y},
                                                                              {"width",  $allocation->width},
                                                                              {"height", $allocation->height},
                                                                              {"state",  $self->getWindowState().toString()}
                                                                      })}));
        $self->Show($self->windowState);

        if ($self->browserWindow != nullptr) {
            $self->browserWindow->Show();
            $self->browserWindow->SetBounds(0, 0, $allocation->width, $allocation->height);
        }
    }

    gboolean RootWindowLinux::OnMove(GtkWidget *$widget, GdkEvent *$event, RootWindowLinux *$self) {
        CEF_REQUIRE_UI_THREAD();

        $self->onMove();

        return FALSE;
    }

    gboolean RootWindowLinux::OnClose(GtkWidget *$window, GdkEvent *$event, RootWindowLinux *$self) {
        CEF_REQUIRE_UI_THREAD();

        const gboolean propagateCloseEventFurther = $self->closeBrowser();

        if (boost::iequals($self->id, "root")) {
            MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);
        }

        return propagateCloseEventFurther;
    }

    void RootWindowLinux::OnDestroyed(GtkWidget *$event, RootWindowLinux *$self) {
        $self->handle = nullptr;
        $self->windowDestroyed = true;
        $self->notifyDestroyedIfDone();
    }

    void RootWindowLinux::OnWindowStateChange(GtkWidget *$widget, GdkEvent *$event, RootWindowLinux *$self) {
        CEF_REQUIRE_UI_THREAD();

        if ($event->window_state.new_window_state & GDK_WINDOW_STATE_ICONIFIED) {
            $self->windowState = WindowStateType::MINIMIZED;
        } else if ($event->window_state.new_window_state & GDK_WINDOW_STATE_MAXIMIZED) {
            $self->windowState = WindowStateType::MAXIMIZED;
        } else {
            $self->windowState = WindowStateType::NORMAL;
        }
    }

    void RootWindowLinux::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();

        GtkAllocation allocation = { 0 };
        gtk_widget_get_allocation(this->handle, &allocation);
        RootWindowLinux::OnSize(this->handle, &allocation, this);

        if (this->isHidden) {
            MainContext::Get()->getRootWindowManager()->getWindowById("root")->getBrowser()->GetHost()->SetFocus(true);
        }
    }

    void RootWindowLinux::OnBrowserWindowDestroyed() {
        REQUIRE_MAIN_THREAD();

        this->browserWindow.reset();

        this->browserDestroyed = true;

        gtk_widget_destroy(this->handle);
    }

    void RootWindowLinux::OnSetTitle(const string &$title) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            gtk_window_set_title(GTK_WINDOW(this->handle), $title.c_str());
        }
    }

    WindowStateType RootWindowLinux::getWindowState() const {
        return this->windowState;
    }

    void RootWindowLinux::Minimize() {
        gtk_window_iconify(GTK_WINDOW(this->handle));
    }

    void RootWindowLinux::Maximize() {
        if (Configuration::getInstance().IsCanResize()) {
            gtk_window_maximize(GTK_WINDOW(this->handle));
        }
    }

    void RootWindowLinux::Restore() {
        if (this->windowState == WindowStateType::MINIMIZED) {
            gtk_window_deiconify(GTK_WINDOW(this->handle));
        } else if (this->windowState == WindowStateType::MAXIMIZED) {
            gtk_window_unmaximize(GTK_WINDOW(this->handle));
        }
    }

    void RootWindowLinux::flushDisplay() {
        GdkWindow *gdkWindow = gtk_widget_get_window(this->handle);
        GdkDisplay *display = gdk_window_get_display(gdkWindow);
        gdk_display_flush(display);
    }

    void RootWindowLinux::updateWindowState(WindowStateType $newState) {
        if ($newState == WindowStateType::MINIMIZED) {
            this->Minimize();
        } else if ($newState == WindowStateType::MAXIMIZED) {
            this->Maximize();
        }
    }
}

#endif  // LINUX_PLATFORM
