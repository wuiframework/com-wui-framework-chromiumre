/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_

#ifdef WIN_PLATFORM

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class BrowserWindowStdWin
            : public Com::Wui::Framework::ChromiumRE::Browser::BrowserWindow {
     public:
        BrowserWindowStdWin(Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *$delegate,
                            const string &$startupUrl);

        void CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect, const CefBrowserSettings &$settings,
                           const CefString &$url, CefRefPtr<CefRequestContext> $requestContext) override;

        void Show() override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        ClientWindowHandle GetWindowHandle() const override;

     private:
        DISALLOW_COPY_AND_ASSIGN(BrowserWindowStdWin);
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_
