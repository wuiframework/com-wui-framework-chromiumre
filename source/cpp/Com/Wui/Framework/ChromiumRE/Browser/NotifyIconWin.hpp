/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONWIN_HPP_

#ifdef WIN_PLATFORM

#include <shellapi.h>

#include "NotifyIcon.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class NotifyIconWin : public NotifyIcon {
     public:
        explicit NotifyIconWin(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner);

        bool Create(const WindowHandlerNotifyIcon &$options,
                    const RootWindow::NotificationIconContextMenu &$contextMenu) override;

        bool Destroy() override;

        bool Modify(const WindowHandlerNotifyIcon &$options,
                    const RootWindow::NotificationIconContextMenu &$contextMenu) override;

     private:
        GUID iconID;
        NOTIFYICONDATA iconData;

        bool updateNotifyIcon(const WindowHandlerNotifyIcon &$options) override;

        void loadIcon(HICON &$icon, const string &$path);
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONWIN_HPP_
