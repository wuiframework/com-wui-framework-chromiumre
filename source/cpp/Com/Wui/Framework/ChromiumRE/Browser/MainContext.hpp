/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXT_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXT_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Used to store global context in the browser process. The methods of this
     * class are thread-safe unless otherwise indicated.
     */
    class MainContext {
     public:
        static MainContext *Get();

        virtual string getConsoleLogPath() = 0;

        virtual string getDownloadPath(const string &$fileName) = 0;

        virtual string getAppWorkingDirectory() = 0;

        virtual cef_color_t getBackgroundColor() = 0;

        virtual bool UseViews() = 0;

        virtual void PopulateSettings(CefSettings *$settings) = 0;

        virtual void PopulateBrowserSettings(CefBrowserSettings *$settings) = 0;

        virtual RootWindowManager *getRootWindowManager() = 0;

     protected:
        MainContext();

        virtual ~MainContext();

     private:
        DISALLOW_COPY_AND_ASSIGN(MainContext);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXT_HPP_
