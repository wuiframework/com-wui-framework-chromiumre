/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BORDERLESSWINDOWMAC_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BORDERLESSWINDOWMAC_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSWindow.h>

@interface BorderlessWindow : NSWindow

- (id)Initialize:(NSRect)$rectangle;

- (void)Maximize;

- (void)Restore;

@property(assign) BOOL maximized;

@end;

#endif  // MAC_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_BORDERLESSWINDOWMAC_HPP_
