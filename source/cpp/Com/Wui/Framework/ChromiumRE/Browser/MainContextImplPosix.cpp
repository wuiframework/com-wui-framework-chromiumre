/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    string MainContextImpl::getDownloadPath(const string &$fileName) {
        // todo(nxf45876): Create method getHomePath(...) in the XCppCommons

        return {};
    }

    string MainContextImpl::getAppWorkingDirectory() {
        char cwd[PATH_MAX] = { 0 };
        if (getcwd(cwd, sizeof(cwd)) != nullptr) {
            return cwd;
        } else {
            throw std::runtime_error("Cannot get current working directory");
        }
    }
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
