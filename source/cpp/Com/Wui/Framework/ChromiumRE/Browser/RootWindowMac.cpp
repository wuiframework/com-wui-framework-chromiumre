/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#import <Cocoa/Cocoa.h>

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Enums::WindowStateType;
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;
    using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon;
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;
    using Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler;
    using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IWindowDelegate;
    using Com::Wui::Framework::ChromiumRE::Commons::WindowPosition;
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;

    namespace UtilMac = Com::Wui::Framework::ChromiumRE::Commons::UtilMac;

    RootWindowMac::~RootWindowMac() {
        if (this->handle != nil) {
            [[NSNotificationCenter defaultCenter] removeObserver:this->handle];

            [this->handle release];
            this->handle = nullptr;
        }
    }

    void RootWindowMac::Show(WindowStateType /*$mode*/) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            if (this->options.empty() || !this->options.value("hidden", false)) {
                [this->handle makeKeyAndOrderFront:nil];
            }
        }
    }

    void RootWindowMac::Hide() {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            [this->handle orderOut:nil];
        }
    }

    void RootWindowMac::SetBounds(int $x, int $y, size_t $width, size_t $height) {
        REQUIRE_MAIN_THREAD();

        if (this->canSetBounds()) {
            [this->handle setFrame:NSMakeRect($x, $y, $width, $height) display:YES animate:YES];
        }
    }

    void RootWindowMac::Close(bool $force) {
        REQUIRE_MAIN_THREAD();

        RootWindow::Close($force);

        if (this->handle != nullptr) {
            this->saveWindowPosition(this->handle.frame.origin.x, this->handle.frame.origin.y,
                                     this->handle.frame.size.width, this->handle.frame.size.height,
                                     this->getWindowState() == WindowStateType::MAXIMIZED);
        }

        this->closeBrowser();

        [this->handle performClose:nil];
    }

    void RootWindowMac::createBrowserWindow(const string &$url) {
        this->browserWindow.reset(new BrowserWindowStdMac(this, $url));
    }

    void RootWindowMac::createRootWindow(const CefBrowserSettings &$settings) {
        REQUIRE_MAIN_THREAD();
        DCHECK(!this->handle);

        RootWindow::createRootWindow($settings);

        // TODO(nxf45876): handle more windows
        this->handle = [[BorderlessWindow alloc] Initialize:NSMakeRect(this->winPosition.getX(),
                                                                       this->winPosition.getY(),
                                                                       this->winPosition.getWidth(),
                                                                       this->winPosition.getHeight())];
        CHECK(this->handle);

        [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowDidResizeNotification
                                              object:this->handle
                                              queue:nil
                                              usingBlock:^(NSNotification */*note*/) {
                                                  this->OnSize();
                                              }];

        [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowWillCloseNotification
                                              object:this->handle
                                              queue:nil
                                              usingBlock:^(NSNotification */*note*/) {
                                                  this->windowDestroyed = true;
                                              }];

        // TODO(nxf45876): handle secondary monitors according to window position
        // TODO(nxf45876): Extract this to base class
        CefRect displayRect(0, 0, UtilMac::GetVisibleFrame(this->handle).size.width, UtilMac::GetVisibleFrame(this->handle).size.height);
        CefRect windowRect(this->winPosition.getX(), this->winPosition.getY(), this->winPosition.getWidth(), this->winPosition.getHeight());

        this->modifyBounds(displayRect, windowRect);

        this->winPosition.setX(windowRect.x);
        this->winPosition.setY(windowRect.y);
        this->winPosition.setWidth(windowRect.width);
        this->winPosition.setHeight(windowRect.height);

        this->SetBounds(this->winPosition.getX(), this->winPosition.getY(), this->winPosition.getWidth(), this->winPosition.getHeight());

        if (boost::iequals(this->id, "root") || this->isHidden) {
            this->Hide();
        }

        if (this->winPosition.getIsMaximized()) {
            this->Maximize();
        }

        this->browserWindow->setIsHidden(this->isHidden);
        this->browserWindow->CreateBrowser(this->handle, windowRect, $settings, this->url, delegate->GetRequestContext(this));
    }

    void RootWindowMac::createRootWindowWrapper(const CefBrowserSettings &$settings) {
        if (CURRENTLY_ON_MAIN_THREAD()) {
            createRootWindow($settings);
        } else {
            MAIN_POST_CLOSURE(base::Bind(&RootWindowMac::createRootWindow, this, $settings));
        }
    }

    void RootWindowMac::OnSize() {
        WindowHandler::FireEvent("OnWindowChanged", json::array({json({
                                                                              {"x",      this->handle.frame.origin.x},
                                                                              {"y",      this->handle.frame.origin.y},
                                                                              {"width",  this->handle.frame.size.width},
                                                                              {"height", this->handle.frame.size.height},
                                                                              {"state",  this->getWindowState().toString()}
                                                                      })}));
        this->Show(this->getWindowState());

        if (this->browserWindow != nullptr) {
            this->browserWindow->Show();
            this->browserWindow->SetBounds(0, 0, this->handle.frame.size.width, this->handle.frame.size.height);
        }
    }

    void RootWindowMac::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();

        RootWindowMac::OnSize();

        if (this->isHidden) {
            MainContext::Get()->getRootWindowManager()->getWindowById("root")->getBrowser()->GetHost()->SetFocus(true);
        }
    }

    void RootWindowMac::OnBrowserWindowDestroyed() {
        REQUIRE_MAIN_THREAD();

        this->browserWindow.reset();

        this->browserDestroyed = true;
    }

    void RootWindowMac::OnSetTitle(const string &$title) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            this->handle.title = [NSString stringWithCString:$title.c_str() encoding:NSASCIIStringEncoding];
        }
    }

    WindowStateType RootWindowMac::getWindowState() const {
        if (this->handle.maximized) {
            return WindowStateType::MAXIMIZED;
        } else if (this->handle.miniaturized) {
            return WindowStateType::MINIMIZED;
        } else {
            return WindowStateType::NORMAL;
        }
    }

    void RootWindowMac::Minimize() {
        [this->handle miniaturize:nil];
    }

    void RootWindowMac::Maximize() {
        if (Configuration::getInstance().IsCanResize()) {
            if (this->getWindowState() != WindowStateType::MAXIMIZED) {
                [this->handle Maximize];
            }
        }
    }

    void RootWindowMac::Restore() {
        if (this->getWindowState() == WindowStateType::MINIMIZED) {
            [this->handle deminiaturize:nil];
        } else if (this->getWindowState() == WindowStateType::MAXIMIZED) {
            [this->handle Restore];
        }
    }
}

#endif  // MAC_PLATFORM
