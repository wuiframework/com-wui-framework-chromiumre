/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOP_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOP_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Represents the message loop running on the main application thread in the browser process.
     * This will be the same as the CEF UI thread on Linux, OS X and Windows when not using multi-threaded message loop mode. The methods of
     * this class are thread-safe unless otherwise indicated.
     */
    class MainMessageLoop {
     public:
        /**
         * @return Returns the singleton instance of this object.
         */
        static MainMessageLoop *Get();

        /**
         *  Run the message loop. The thread that this method is called on will be considered the main thread.
         *  This blocks until Quit() is called.
         * @return
         */
        virtual int Run() = 0;

        /**
         * Quit the message loop.
         */
        virtual void Quit() = 0;

        /**
         * Post a task for execution on the main message loop.
         * @param $task Specify task to be executed.
         */
        virtual void PostTask(CefRefPtr<CefTask> $task) = 0;

        /**
         * @return Returns true if this message loop runs tasks on the current thread.
         */
        virtual bool RunsTasksOnCurrentThread() const = 0;

#if defined(OS_WIN)

        /**
         * Set the current modeless dialog on Windows for proper delivery of dialog messages when using multi-threaded message loop mode.
         * This method must be called from the main thread. See http://support.microsoft.com/kb/71450 for background.
         * @param $hWndDialog Specify window handle.
         */
        virtual void SetCurrentModelessDialog(HWND $hWndDialog) = 0;

#endif

        /**
         *  Post a closure for execution on the main message loop.
         * @param $closure Specify closure.
         */
        void PostClosure(const base::Closure &$closure);

     protected:
        friend struct base::DefaultDeleter<MainMessageLoop>;

        MainMessageLoop();

        virtual ~MainMessageLoop();

     private:
        DISALLOW_COPY_AND_ASSIGN(MainMessageLoop);
    };
}

#define CURRENTLY_ON_MAIN_THREAD() \
    Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop::Get()->RunsTasksOnCurrentThread()

#define REQUIRE_MAIN_THREAD() DCHECK(CURRENTLY_ON_MAIN_THREAD())

#define MAIN_POST_TASK(task) \
    Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop::Get()->PostTask(task)

#define MAIN_POST_CLOSURE(closure) \
    Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop::Get()->PostClosure(closure)

struct DeleteOnMainThread {
    template<typename T>
    static void Destruct(const T *$x) {
        if (CURRENTLY_ON_MAIN_THREAD()) {
            delete $x;
        } else {
            Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop::Get()->PostClosure(
                    base::Bind(&DeleteOnMainThread::Destruct<T>, $x));
        }
    }
};

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOP_HPP_
