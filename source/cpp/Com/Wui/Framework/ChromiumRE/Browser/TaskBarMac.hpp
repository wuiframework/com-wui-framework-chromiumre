/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARMAC_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARMAC_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSImageView.h>
#import <Appkit/NSProgressIndicator.h>

@interface TaskBarWrapper : NSObject

@property(nonatomic, retain) NSImageView *dockView;

@property(nonatomic, retain) NSProgressIndicator *progressBar;

- (id)init;

- (void)ShowProgressBar;

- (void)HideProgressBar;

@end;

#include "TaskBar.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class TaskBarMac : public TaskBar {
     public:
        explicit TaskBarMac(scoped_refptr<RootWindow> $owner);

        ~TaskBarMac();

        bool setProgressState(const Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState &$state) override;

        bool setProgressValue(const int $completed, const int $total) override;

        void Clear() override;

     private:
        TaskBarWrapper *taskBar = nil;
    };
}

#endif  // MAC_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_TASKBARMAC_HPP_
