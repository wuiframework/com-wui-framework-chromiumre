/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNER_HPP_

#include "../Enums/WindowCornerType.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Implement this interface for different platforms. Methods will be called on
     * the browser process UI thread unless otherwise indicated.
     */
    class WindowTestRunner {
        typedef Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Com::Wui::Framework::ChromiumRE::Enums::WindowStateType WindowStateType;
        typedef Com::Wui::Framework::ChromiumRE::Enums::WindowCornerType WindowCornerType;

     public:
        static void MoveTo(CefRefPtr<CefBrowser> $browser, int $x, int $y, shared_ptr<QueryResponse> $response);

        static void Minimize(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response);

        static void Maximize(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response);

        static void Restore(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response);

        static void Resize(CefRefPtr<CefBrowser> $browser, WindowCornerType $fixedCorner, int $dx, int $dy,
                           const shared_ptr<QueryResponse> $response);

        static void CanResize(CefRefPtr<CefBrowser> $browser, bool $canResize, const shared_ptr<QueryResponse> $response);

        static void getWindowState(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response);

        static void Open(CefRefPtr<CefBrowser> $browser, const string &$url, const json &$options,
                         const shared_ptr<QueryResponse> $response);

        static void ScriptExecute(CefRefPtr<CefBrowser> $browser, const string &$window, const json &$options,
                                  const shared_ptr<QueryResponse> $response);

        static void Close(CefRefPtr<CefBrowser> $browser, const string &$window,
                          const shared_ptr<QueryResponse> $response);

        static void getCookies(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response);

        static void Show(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response);

        static void Hide(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response);

        static void CreateNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                     const shared_ptr<QueryResponse> $response);

        static void ModifyNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                     const shared_ptr<QueryResponse> $response);

        static void DestroyNotifyIcon(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response);

        static void setTaskBarProgressState(CefRefPtr<CefBrowser> $browser,
                                            const Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState $state,
                                            const shared_ptr<QueryResponse> $response);

        static void setTaskBarProgressValue(CefRefPtr<CefBrowser> $browser, const int $completed, const int $total,
                                            const shared_ptr<QueryResponse> $response);

        static void ShowFileDialog(CefRefPtr<CefBrowser> $browser, const json &$settings, const shared_ptr<QueryResponse> $response);

        static void ShowDebugConsole(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response);

        template <typename MethodName, typename... Arguments>
        void InvokeWithThreadSafety(MethodName $method, Arguments... $args) {
            if (CURRENTLY_ON_MAIN_THREAD()) {
                $method(std::forward<Arguments>($args)...);
            } else {
                MAIN_POST_CLOSURE(base::Bind($method, std::forward<Arguments>($args)...));
            }
        }

     private:
        static void performAction(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response,
                                  std::function<bool(scoped_refptr<RootWindow>)> $callback);

        static CefRect getRectangleForMove(ClientWindowHandle $handle, int $x, int $y);

        static CefRect getRectangleForResize(ClientWindowHandle $handle, int $dx, int $dy, WindowCornerType $fixedCorner);

        static void performOnCloseActions(RootWindow *$rootWindow);

        static void createTaskBarIfNotExists(scoped_refptr<RootWindow> $rootWindow);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNER_HPP_
