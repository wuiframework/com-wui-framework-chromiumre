/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

#include "strsafe.h"  // NOLINT(build/include)
#include "objbase.h"  // NOLINT(build/include)

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType;

    NotifyIconWin::NotifyIconWin(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner)
            : NotifyIcon($owner) {
        CoCreateGuid(&this->iconID);
        this->iconData = {};
        this->iconData.cbSize = sizeof(this->iconData);
        this->iconData.guidItem = this->iconID;
        this->iconData.uVersion = NOTIFYICON_VERSION_4;
    }

    bool NotifyIconWin::Create(const WindowHandlerNotifyIcon &$options,
                               const RootWindow::NotificationIconContextMenu &/*$contextMenu*/) {
        if (!this->created) {
            if (this->updateNotifyIcon($options)) {
                if (Shell_NotifyIcon(NIM_ADD, &this->iconData) == TRUE) {
                    Shell_NotifyIcon(NIM_SETVERSION, &this->iconData);
                    this->onCreate(true);
                    return true;
                } else {
                    this->onCreate(false);
                }
            }
        } else {
            LogIt::Warning("Only one notify icon is enabled for application at this time. Use modify or destroy.");
        }
        return false;
    }

    bool NotifyIconWin::Destroy() {
        if (this->created) {
            if (Shell_NotifyIcon(NIM_DELETE, &this->iconData)) {
                this->onDestroy(true);
                return true;
            } else {
                this->onDestroy(false);
            }
        }
        return false;
    }

    bool NotifyIconWin::Modify(const WindowHandlerNotifyIcon &$options,
                               const RootWindow::NotificationIconContextMenu &$contextMenu) {
        if (!this->created) {
            return this->Create($options, $contextMenu);
        } else {
            if (this->updateNotifyIcon($options)) {
                if (Shell_NotifyIcon(NIM_MODIFY, &this->iconData)) {
                    this->onModify(true);
                    return true;
                } else {
                    this->onModify(false);
                }
            }
        }
        return false;
    }

    bool NotifyIconWin::updateNotifyIcon(const WindowHandlerNotifyIcon &$options) {
        const auto loadIcon = [&](HICON &$data, const string &$iconPath) {
            if (this->iconFileExist($iconPath)) {
                this->loadIcon($data, $iconPath);
            }
        };

        this->iconData.hWnd = this->owner->getWindowHandle();
        this->iconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_SHOWTIP;
        this->iconData.uCallbackMessage = WM_USER;
        this->iconData.uVersion = NOTIFYICON_VERSION_4;
        this->iconData.hIcon = (HICON)GetClassLongPtr(this->owner->getWindowHandle(), GCLP_HICONSM);
        string tip = Com::Wui::Framework::ChromiumRE::Commons::Configuration::getInstance().getAppExeName();
        string tmp = $options.getTip();
        if (!tmp.empty()) {
            if (tmp.size() < sizeof(this->iconData.szTip) / sizeof(WCHAR)) {
                tip = tmp;
            } else {
                LogIt::Warning("Notify icon tooltip text exceeds maximum chars ({0}/{1}).", tmp.size(),
                               sizeof(this->iconData.szTip) / sizeof(WCHAR));
                return false;
            }
        }

        loadIcon(this->iconData.hIcon, $options.getIcon());

        if ($options.getBalloon()) {
            string infoTitle = $options.getBalloon()->getTitle();
            if (!infoTitle.empty()) {
                if (infoTitle.size() >= sizeof(this->iconData.szInfoTitle) / sizeof(WCHAR)) {
                    LogIt::Warning("Notify icon info text exceeds maximum chars ({0}/{1}).", infoTitle.size(),
                                   sizeof(this->iconData.szInfoTitle) / sizeof(WCHAR));
                    return false;
                } else {
#ifdef UNICODE
                    wcscpy_s(this->iconData.szInfoTitle, ARRAYSIZE(this->iconData.szInfoTitle),
                             std::wstring(infoTitle.begin(), infoTitle.end()).c_str());
#else
                    strcpy_s(this->iconData.szInfoTitle, ARRAYSIZE(this->iconData.szInfoTitle), infoTitle.c_str());
#endif
                    this->iconData.uFlags |= NIF_INFO;
                    this->iconData.dwInfoFlags = NIIF_USER | NIIF_LARGE_ICON;

                    string info = $options.getBalloon()->getMessage();
                    if (!info.empty()) {
                        if (info.size() >= sizeof(this->iconData.szInfo) / sizeof(WCHAR)) {
                            LogIt::Warning("Notify icon info text exceeds maximum chars ({0}/{1}).", info.size(),
                                           sizeof(this->iconData.szInfo) / sizeof(WCHAR));
                            return false;
                        } else {
#ifdef UNICODE
                            wcscpy_s(this->iconData.szInfo, ARRAYSIZE(this->iconData.szInfo),
                                     std::wstring(info.begin(), info.end()).c_str());
#else
                            strcpy_s(this->iconData.szInfo, ARRAYSIZE(this->iconData.szInfo), info.c_str());
#endif
                        }
                    }

                    auto type = $options.getBalloon()->getType();
                    if (type == NotifyBalloonIconType::NONE) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_NONE;
                    } else if (type == NotifyBalloonIconType::INFO) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_INFO;
                    } else if (type == NotifyBalloonIconType::WARNING) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_WARNING;
                    } else if (type == NotifyBalloonIconType::ERROR) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_ERROR;
                    } else if (type == NotifyBalloonIconType::USER) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_USER;

                        loadIcon(this->iconData.hBalloonIcon, $options.getBalloon()->getUserIcon());
                    }

                    if ($options.getBalloon()->isNoSound()) {
                        this->iconData.dwInfoFlags |= NIIF_NOSOUND;
                    } else {
                        this->iconData.dwInfoFlags &= ~NIIF_NOSOUND;
                    }
                }
            }
        }

#ifdef UNICODE
        wcscpy_s(this->iconData.szTip, ARRAYSIZE(this->iconData.szTip), std::wstring(tip.begin(), tip.end()).c_str());
#else
        strcpy_s(this->iconData.szTip, ARRAYSIZE(this->iconData.szTip), tip.c_str());
#endif
        this->iconData.guidItem = this->iconID;

        return true;
    }

    void NotifyIconWin::loadIcon(HICON &$icon, const string &$path) {
        const std::wstring path = std::wstring(std::cbegin($path), std::cend($path));
        $icon = (HICON)LoadImage(nullptr, path.c_str(), IMAGE_ICON, 0, 0,
                                 LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_DEFAULTCOLOR);
        LogIt::Info("Loaded notify icon from {0}", $path);
    }
}

#endif  // WIN_PLATFORM
