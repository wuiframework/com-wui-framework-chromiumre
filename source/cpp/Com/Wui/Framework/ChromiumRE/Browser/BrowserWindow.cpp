/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate;

namespace Com::Wui::Framework::ChromiumRE::Browser {
    BrowserWindow::~BrowserWindow() {
        if (this->clientHandler != nullptr) {
            this->clientHandler->DetachDelegate();
            this->clientHandler = nullptr;
        }
    }

    // cppcheck-suppress uninitMemberVar
    BrowserWindow::BrowserWindow(IBrowserWindowDelegate *$delegate)
            : delegate($delegate),
              isClosing(false) {
    }

    void BrowserWindow::SetFocus(bool $focus) {
        REQUIRE_MAIN_THREAD();

        if (this->browser) {
            this->browser->GetHost()->SetFocus($focus);
        }
    }

    CefRefPtr<CefBrowser> BrowserWindow::GetBrowser() const {
        REQUIRE_MAIN_THREAD();
        return this->browser;
    }

    bool BrowserWindow::IsClosing() const {
        REQUIRE_MAIN_THREAD();
        return this->isClosing;
    }

    void BrowserWindow::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();
        this->browser = $browser;

        this->delegate->OnBrowserCreated(this->browser);
    }

    void BrowserWindow::OnBrowserClosing(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();
        DCHECK_EQ($browser->GetIdentifier(), $browser->GetIdentifier());
        this->isClosing = true;
    }

    void BrowserWindow::OnBrowserClosed(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();
        if ($browser.get()) {
            DCHECK_EQ($browser->GetIdentifier(), $browser->GetIdentifier());
            $browser = nullptr;
        }

        this->clientHandler->DetachDelegate();
        this->clientHandler = nullptr;

        this->delegate->OnBrowserWindowDestroyed();
    }

    void BrowserWindow::OnSetAddress(const string &$url) {
        REQUIRE_MAIN_THREAD();
    }

    void BrowserWindow::OnSetTitle(const string &$title) {
        REQUIRE_MAIN_THREAD();
        this->delegate->OnSetTitle($title);
    }

    void BrowserWindow::OnSetFullscreen(bool) {
        REQUIRE_MAIN_THREAD();
    }

    void BrowserWindow::OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) {
        REQUIRE_MAIN_THREAD();
        this->delegate->OnSetLoadingState($isLoading, $canGoBack, $canGoForward);
    }

    void BrowserWindow::OnSetDraggableRegions(const std::vector<CefDraggableRegion> &$regions) {
        REQUIRE_MAIN_THREAD();
    }
}
