/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate;
using Com::Wui::Framework::XCppCommons::Utils::LogIt;

namespace Com::Wui::Framework::ChromiumRE::Browser {
    BrowserWindowStdWin::BrowserWindowStdWin(IBrowserWindowDelegate *$delegate, const string &$startupUrl)
            : BrowserWindow($delegate) {
        this->clientHandler = new ClientHandlerStd(this, $startupUrl);
    }

    void BrowserWindowStdWin::CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect,
                                            const CefBrowserSettings &$settings, const CefString &$url,
                                            CefRefPtr<CefRequestContext> $requestContext) {
        REQUIRE_MAIN_THREAD();

        CefWindowInfo windowInfo;
        RECT wndRect = {$rect.x, $rect.y, $rect.x + $rect.width, $rect.y + $rect.height};
        windowInfo.SetAsChild($parentHandle, wndRect);
        if (this->isHidden) {
            windowInfo.style = WS_CHILD | WS_DISABLED;
            windowInfo.ex_style = WS_EX_NOACTIVATE;
        }
        CefBrowserHost::CreateBrowser(std::move(windowInfo), this->clientHandler, $url, $settings, $requestContext);
        LogIt::Info("Context cookie path: {0}", $requestContext->GetCachePath().ToString());
    }

    void BrowserWindowStdWin::Show() {
        REQUIRE_MAIN_THREAD();
        if (!this->isHidden) {
            HWND hwnd = GetWindowHandle();

            if ((hwnd != nullptr) && (IsWindowVisible(hwnd) == FALSE)) {
                ShowWindow(hwnd, SW_SHOW);
                this->SetFocus(true);
            }
        }
    }

    void BrowserWindowStdWin::Hide() {
        REQUIRE_MAIN_THREAD();
        if (!this->isHidden) {
            HWND hwnd = GetWindowHandle();
            if (hwnd != nullptr) {
                SetWindowPos(hwnd, nullptr, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSIZE);
            }
        }
    }

    void BrowserWindowStdWin::SetBounds(int $x, int $y, size_t $width, size_t $height) {
        REQUIRE_MAIN_THREAD();

        HWND hwnd = GetWindowHandle();
        if (hwnd != nullptr) {
            if (!this->isHidden) {
                SetWindowPos(hwnd, nullptr, $x, $y, static_cast<int>($width), static_cast<int>($height), SWP_NOZORDER);
            }
        }
    }

    ClientWindowHandle BrowserWindowStdWin::GetWindowHandle() const {
        REQUIRE_MAIN_THREAD();

        if (this->browser) {
            return this->browser->GetHost()->GetWindowHandle();
        }
        return nullptr;
    }
}

#endif  // WIN_PLATFORM
