/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#include "include/base/cef_logging.h"
#include "include/cef_cookie.h"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    ClientApplicationBrowser::ClientApplicationBrowser() {
        CreateDelegates(this->delegates);
    }

    void ClientApplicationBrowser::OnBeforeCommandLineProcessing(const CefString &$processType, CefRefPtr<CefCommandLine> $commandLine) {
        if ($processType.empty()) {
            DelegateSet::iterator it = this->delegates.begin();
            for (; it != this->delegates.end(); ++it) {
                (*it)->OnBeforeCommandLineProcessing(this, $commandLine);
            }
        }
    }

    void ClientApplicationBrowser::OnContextInitialized() {
        CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager(nullptr);
        DCHECK(manager.get());

        this->cookieableSchemes.insert(this->cookieableSchemes.end(), CefString("file"));
        manager->SetSupportedSchemes(this->cookieableSchemes, nullptr);

        this->printHandler = CreatePrintHandler();

        DelegateSet::iterator it = this->delegates.begin();
        for (; it != this->delegates.end(); ++it) {
            (*it)->OnContextInitialized(this);
        }
    }

    void ClientApplicationBrowser::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> $commandLine) {
        DelegateSet::iterator it = this->delegates.begin();
        for (; it != this->delegates.end(); ++it) {
            (*it)->OnBeforeChildProcessLaunch(this, $commandLine);
        }
    }

    void ClientApplicationBrowser::OnRenderProcessThreadCreated(CefRefPtr<CefListValue> $extraInfo) {
        DelegateSet::iterator it = this->delegates.begin();
        for (; it != this->delegates.end(); ++it) {
            (*it)->OnRenderProcessThreadCreated(this, $extraInfo);
        }
    }

    void ClientApplicationBrowser::CreateDelegates(DelegateSet &$delegates) {}

    CefRefPtr<CefPrintHandler> ClientApplicationBrowser::CreatePrintHandler() {
        return nullptr;
    }
}
