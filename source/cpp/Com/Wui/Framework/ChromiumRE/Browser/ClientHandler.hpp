/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLER_HPP_

class ClientDownloadImageCallback;

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Client handler abstract base class. Provides common functionality shared by
     * all concrete client handler implementations.
     */
    class ClientHandler
            : public CefClient,
              public CefContextMenuHandler,
              public CefDisplayHandler,
              public CefDownloadHandler,
              public CefDragHandler,
              public CefKeyboardHandler,
              public CefLifeSpanHandler,
              public CefLoadHandler,
              public CefRequestHandler {
     public:
        typedef std::set<CefMessageRouterBrowserSide::Handler *> MessageHandlerSet;

        ClientHandler(Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientDelegate *$delegate, const string &$startupUrl);

        void DetachDelegate();

        CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() override {
            return this;
        }

        CefRefPtr<CefDisplayHandler> GetDisplayHandler() override {
            return this;
        }

        CefRefPtr<CefDownloadHandler> GetDownloadHandler() override {
            return this;
        }

        CefRefPtr<CefDragHandler> GetDragHandler() override {
            return this;
        }

        CefRefPtr<CefKeyboardHandler> GetKeyboardHandler() override {
            return this;
        }

        CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override {
            return this;
        }

        CefRefPtr<CefLoadHandler> GetLoadHandler() override {
            return this;
        }

        CefRefPtr<CefRequestHandler> GetRequestHandler() override {
            return this;
        }

        bool OnProcessMessageReceived(CefRefPtr<CefBrowser> $browser, CefProcessId $sourceProcess,
                                      CefRefPtr<CefProcessMessage> $message) override;

        void OnBeforeContextMenu(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                 CefRefPtr<CefContextMenuParams> $params, CefRefPtr<CefMenuModel> $model) override;

        bool OnContextMenuCommand(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                  CefRefPtr<CefContextMenuParams> $params, int $commandId, EventFlags $eventFlags) override;

        void OnAddressChange(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$url) override;

        void OnTitleChange(CefRefPtr<CefBrowser> $browser, const CefString &$title) override;

        void OnFaviconURLChange(CefRefPtr<CefBrowser> $browser, const std::vector<CefString> &$iconUrls) override;

        void OnFullscreenModeChange(CefRefPtr<CefBrowser> $browser, bool $fullScreen) override;

        bool OnConsoleMessage(CefRefPtr<CefBrowser> $browser,  cef_log_severity_t $level, const CefString &$message,
                              const CefString &$source, int $line) override;

        void OnBeforeDownload(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDownloadItem> $downloadItem,
                              const CefString &$suggestedName, CefRefPtr<CefBeforeDownloadCallback> $callback) override;

        void OnDownloadUpdated(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDownloadItem> $downloadItem,
                               CefRefPtr<CefDownloadItemCallback> $callback) override;

        bool OnDragEnter(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefDragData> $dragData,
                         CefDragHandler::DragOperationsMask $mask) override;

        void OnDraggableRegionsChanged(CefRefPtr<CefBrowser> $browser,
                                       const std::vector<CefDraggableRegion> &$regions) override;

        bool OnPreKeyEvent(CefRefPtr<CefBrowser> $browser, const CefKeyEvent &$event, CefEventHandle $osEvent,
                           bool *$isKeyboardShortcut) override;

        bool OnBeforePopup(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$targetUrl,
                           const CefString &$targetFrameName, CefLifeSpanHandler::WindowOpenDisposition $targetDisposition,
                           bool $userGesture, const CefPopupFeatures &$popupFeatures, CefWindowInfo &$windowInfo,
                           CefRefPtr<CefClient> &$client, CefBrowserSettings &$settings, bool *$noJavascriptAccess) override;

        void OnAfterCreated(CefRefPtr<CefBrowser> $browser) override;

        bool DoClose(CefRefPtr<CefBrowser> $browser) override;

        void OnBeforeClose(CefRefPtr<CefBrowser> $browser) override;

        void OnLoadingStateChange(CefRefPtr<CefBrowser> $browser, bool $isLoading, bool $canGoBack, bool $canGoForward) override;

        void OnLoadError(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, ErrorCode $errorCode,
                         const CefString &$errorText, const CefString &$failedUrl) override;

        bool OnBeforeBrowse(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, CefRefPtr<CefRequest> $request,
                            bool $userGesture, bool $isRedirect) override;

        bool OnOpenURLFromTab(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, const CefString &$targetUrl,
                              CefRequestHandler::WindowOpenDisposition $targetDisposition, bool $userGesture) override;

        cef_return_value_t OnBeforeResourceLoad(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, CefRefPtr<CefRequest> $request,
                                                CefRefPtr<CefRequestCallback> $callback) override;

        CefRefPtr<CefResourceHandler> GetResourceHandler(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                                         CefRefPtr<CefRequest> $request) override;

        CefRefPtr<CefResponseFilter> GetResourceResponseFilter(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                                               CefRefPtr<CefRequest> $request, CefRefPtr<CefResponse> $response) override;

        bool OnQuotaRequest(CefRefPtr<CefBrowser> $browser, const CefString &$originUrl, int64 $newSize,
                            CefRefPtr<CefRequestCallback> $callback) override;

        void OnProtocolExecution(CefRefPtr<CefBrowser> $browser, const CefString &$url, bool &$allowOsExecution) override;  // NOLINT

        bool OnCertificateError(CefRefPtr<CefBrowser> $browser, ErrorCode $certError, const CefString &$requestUrl,
                                CefRefPtr<CefSSLInfo> $sslInfo, CefRefPtr<CefRequestCallback> $callback) override;

        void OnRenderProcessTerminated(CefRefPtr<CefBrowser> $browser, TerminationStatus $status) override;

        int GetBrowserCount() const;

        void ShowDevTools(CefRefPtr<CefBrowser> $browser, const CefPoint &$inspectElementAt);

        void CloseDevTools(CefRefPtr<CefBrowser> $browser);

        Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientDelegate *getDelegate() const { return this->delegate; }

     private:
        friend class ClientDownloadImageCallback;

        void NotifyBrowserCreated(CefRefPtr<CefBrowser> $browser);

        void NotifyBrowserClosing(CefRefPtr<CefBrowser> $browser);

        void NotifyBrowserClosed(CefRefPtr<CefBrowser> $browser);

        void NotifyAddress(const CefString &$url);

        void NotifyTitle(const CefString &$title);

        void NotifyFavicon(CefRefPtr<CefImage> $image);

        void NotifyFullscreen(bool $fullScreen);

        void NotifyLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward);

        void NotifyDraggableRegions(const std::vector<CefDraggableRegion> &$regions);

        const string startupUrl;
        bool mouseCursorChangeDisabled;
        bool downloadFaviconImages;
        CefRefPtr<CefMessageRouterBrowserSide> messageRouter;
        CefRefPtr<CefResourceManager> resourceManager;
        Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientDelegate *delegate;
        int browserCount;
        const string consoleLogFile;
        bool firstConsoleMessage;
        bool focusOnEditableField;
        MessageHandlerSet messageHandlerSet;

        DISALLOW_COPY_AND_ASSIGN(ClientHandler);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLER_HPP_
