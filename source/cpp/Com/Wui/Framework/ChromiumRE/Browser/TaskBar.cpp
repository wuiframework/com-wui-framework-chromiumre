/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    TaskBar::TaskBar(scoped_refptr<RootWindow> $owner) {
        this->owner = $owner;
    }

    scoped_refptr<RootWindow> TaskBar::getOwner() const {
        return this->owner;
    }

    void TaskBar::setOwner(scoped_refptr<RootWindow> $owner) {
        this->owner = $owner;
    }
}
