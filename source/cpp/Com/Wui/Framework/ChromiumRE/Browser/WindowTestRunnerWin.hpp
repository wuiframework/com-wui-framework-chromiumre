/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_

#ifdef WIN_PLATFORM

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Windows platform implementation. Methods are safe to call on any browser
     * process thread.
     */
    class WindowTestRunnerWin : public Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunner {
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_
