/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICON_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICON_HPP_

#include "RootWindow.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * NotifyIcon class provides API to control status icon in system tray.
     */
    class NotifyIcon {
     protected:
        using WindowHandlerNotifyIcon = Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon;

     public:
        static NotifyIcon *Create(scoped_refptr<RootWindow> $owner);

        constexpr static int GetIndexModifier() {
            return 2000;
        }

        explicit NotifyIcon(scoped_refptr<RootWindow> $owner);

        virtual ~NotifyIcon() = default;

        virtual bool Create(const WindowHandlerNotifyIcon &$options,
                            const RootWindow::NotificationIconContextMenu &$contextMenu) = 0;

        virtual bool Destroy() = 0;

        virtual bool Modify(const WindowHandlerNotifyIcon &$options,
                            const RootWindow::NotificationIconContextMenu &$contextMenu);

        scoped_refptr<RootWindow> getOwner() const;

        void setOwner(scoped_refptr<RootWindow> $owner);

     protected:
        scoped_refptr<RootWindow> owner = nullptr;
        bool created = false;

        void onCreate(bool $success);

        void onDestroy(bool $success);

        void onModify(bool $success);

        virtual bool updateNotifyIcon(const WindowHandlerNotifyIcon &$options) = 0;

        bool iconFileExist(const string &$path) const;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICON_HPP_
