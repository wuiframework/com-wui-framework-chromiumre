/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONMAC_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONMAC_HPP_

#ifdef MAC_PLATFORM

@interface NotifyIconWrapper : NSObject

@property(nonatomic, retain) NSMenu *menu;

@property(nonatomic, retain) NSStatusItem *notificationIcon;

@end;

@interface NotifyContextMenuItem : NSMenuItem

@property(nonatomic, retain) NSString *name;

- (id)initWithTitle:(const char *)$title name:(const char *)$name;

@end

#include "NotifyIcon.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class NotifyIconMac : public NotifyIcon {
     public:
        using NotifyIcon::NotifyIcon;

        bool Create(const WindowHandlerNotifyIcon &$options,
                    const RootWindow::NotificationIconContextMenu &$contextMenu) override;

        bool Destroy() override;

     private:
        bool updateNotifyIcon(const WindowHandlerNotifyIcon &$options) override;

        NotifyIconWrapper *icon = nullptr;
    };
}

#endif  // MAC_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONMAC_HPP_
