/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {

    TaskBar *TaskBar::Create(scoped_refptr<RootWindow> $owner) {
#ifdef WIN_PLATFORM
        return new TaskBarWin($owner);
#elif LINUX_PLATFORM
#warning "TaskBar::Create is not implemented on this platform"

        return nullptr;
#elif MAC_PLATFORM
        return new TaskBarMac($owner);
#endif
    }
}
