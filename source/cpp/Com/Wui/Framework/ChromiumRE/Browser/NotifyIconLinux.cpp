/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler;
    using Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType;

    namespace UtilLinux = Com::Wui::Framework::ChromiumRE::Commons::UtilLinux;

    AppIndicator *NotifyIconLinux::indicator = nullptr;

    NotifyIconLinux::NotifyIconLinux(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner)
            : NotifyIcon($owner) {
        this->defaultIconPath = UtilLinux::GetAppIconPath();
    }

    bool NotifyIconLinux::Create(const WindowHandlerNotifyIcon &$options,
                                 const RootWindow::NotificationIconContextMenu &$contextMenu) {
        if (!this->created) {
            if (this->indicator == nullptr) {
                const string iconPath = $options.getIcon().empty() ? this->defaultIconPath : $options.getIcon();

                this->indicator = app_indicator_new("com-wui-framework-chromiumre",
                                                    iconPath.c_str(),
                                                    APP_INDICATOR_CATEGORY_APPLICATION_STATUS);
                app_indicator_set_title(this->indicator, $options.getTip().c_str());

                this->contextMenu = gtk_menu_new();

                for (size_t i = 0; i < $contextMenu.size(); ++i) {
                    const auto &item = $contextMenu.at(i + NotifyIcon::GetIndexModifier());

                    GtkWidget *menuItem = gtk_image_menu_item_new_with_label(item.getLabel().c_str());
                    g_signal_connect(menuItem, "activate", GTK_SIGNAL_FUNC(NotifyIconLinux::contextMenuItemClicked),
                                    (gpointer)item.getName().c_str());
                    gtk_menu_shell_insert(GTK_MENU_SHELL(this->contextMenu), menuItem, item.getPosition());
                }

                app_indicator_set_menu(this->indicator, GTK_MENU(this->contextMenu));

                gtk_widget_show_all(this->contextMenu);
            }

            if (this->updateNotifyIcon($options)) {
                this->onCreate(true);
                return true;
            }
        }
        return false;
    }

    bool NotifyIconLinux::Destroy() {
        if (this->created) {
            app_indicator_set_status(this->indicator, APP_INDICATOR_STATUS_PASSIVE);

            this->onDestroy(true);

            return true;
        }
        return false;
    }

    void NotifyIconLinux::contextMenuItemClicked(GtkWidget */*$menuItem*/, const char *$name) {
        if ($name != nullptr) {
            WindowHandler::FireEvent("OnNotifyIconContextItemSelected", {$name});
        }
    }

    bool NotifyIconLinux::updateNotifyIcon(const WindowHandlerNotifyIcon &/*$options*/) {
        app_indicator_set_status(this->indicator, APP_INDICATOR_STATUS_ACTIVE);

        return true;
    }
}

#endif  // LINUX_PLATFORM
