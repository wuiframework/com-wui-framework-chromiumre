/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOPSTD_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOPSTD_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Represents the main message loop in the browser process. This implementation is a light-weight wrapper around the Chromium UI thread.
     */
    class MainMessageLoopStd
            : public Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop {
     public:
        MainMessageLoopStd() = default;

        int Run() override;

        void Quit() override;

        void PostTask(CefRefPtr<CefTask> $task) override;

        bool RunsTasksOnCurrentThread() const override;

#if defined(OS_WIN)

        void SetCurrentModelessDialog(HWND $hWndDialog) override;

#endif

     private:
        DISALLOW_COPY_AND_ASSIGN(MainMessageLoopStd);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINMESSAGELOOPSTD_HPP_
