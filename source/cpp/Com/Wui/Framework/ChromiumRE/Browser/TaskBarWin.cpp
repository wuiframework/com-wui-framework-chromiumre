/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState;

    TaskBarWin::TaskBarWin(scoped_refptr<RootWindow> $owner)
        : TaskBar($owner) {
        CoInitialize(nullptr);
        CoCreateInstance(CLSID_TaskbarList, nullptr, CLSCTX_INPROC_SERVER, IID_ITaskbarList3,
                         reinterpret_cast<void **>(&this->taskBarList));
    }


    bool TaskBarWin::setProgressState(const TaskBarProgressState &$state) {
        bool retVal = false;
        TBPFLAG flag = TBPF_NOPROGRESS;
        if ($state == TaskBarProgressState::INDETERMINATE) {
            flag = TBPF_INDETERMINATE;
        } else if ($state == TaskBarProgressState::NORMAL) {
            flag = TBPF_NORMAL;
        } else if ($state == TaskBarProgressState::ERROR) {
            flag = TBPF_ERROR;
        } else if ($state == TaskBarProgressState::PAUSED) {
            flag = TBPF_PAUSED;
        }

        if ((this->taskBarList != nullptr) && (this->owner != nullptr)) {
            if (this->taskBarList->SetProgressState(this->owner->getWindowHandle(), flag) == S_OK) {
                retVal = true;
            }
        }

        return retVal;
    }

    bool TaskBarWin::setProgressValue(const int $completed, const int $total) {
        bool retVal = false;

        if ((this->taskBarList != nullptr) && (this->owner != nullptr)) {
            if (this->taskBarList->SetProgressValue(this->owner->getWindowHandle(), static_cast<ULONGLONG>($completed),
                                                    static_cast<ULONGLONG>($total)) == S_OK) {
                retVal = true;
            }
        }

        return retVal;
    }

    void TaskBarWin::Clear() {
        if (this->taskBarList != nullptr) {
            CoUninitialize();
        }
    }
}

#endif  // WIN_PLATFORM
