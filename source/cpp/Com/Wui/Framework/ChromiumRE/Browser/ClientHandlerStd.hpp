/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTD_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTD_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Client handler implementation for windowed browsers. There will only ever be one browser per handler instance.
     */
    class ClientHandlerStd
            : public Com::Wui::Framework::ChromiumRE::Browser::ClientHandler {
     public:
        ClientHandlerStd(Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientDelegate *delegate, const string &startupUrl);

     private:
        IMPLEMENT_REFCOUNTING(ClientHandlerStd);
        DISALLOW_COPY_AND_ASSIGN(ClientHandlerStd);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTD_HPP_
