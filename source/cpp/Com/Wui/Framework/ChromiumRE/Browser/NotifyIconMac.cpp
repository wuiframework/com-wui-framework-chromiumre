/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

@implementation NotifyIconWrapper

- (id)init {
    if (self = [super init]) {
        self.menu = [[NSMenu alloc] initWithTitle:@""];

        self.notificationIcon = [NSStatusBar.systemStatusBar statusItemWithLength:NSSquareStatusItemLength];
        self.notificationIcon.title = nil;
        self.notificationIcon.image = Com::Wui::Framework::ChromiumRE::Commons::UtilMac::ScaleImage([NSImage imageNamed:@"icon"],
                                                                                                    NSMakeSize(16, 16));
        self.notificationIcon.menu = self.menu;
    }

    return self;
}

- (void)dealloc {
    [NSStatusBar.systemStatusBar removeStatusItem:self.notificationIcon];

    if (self.menu != nil) {
        [self.menu release];
        self.menu = nil;
    }

    if (self.notificationIcon != nil) {
        [self.notificationIcon.image release];
        [self.notificationIcon release];
        self.notificationIcon = nil;
    }

    [super dealloc];
}

@end;

@implementation NotifyContextMenuItem

- (id)initWithTitle:(const char *)$title name:(const char *)$name {
    if (self = [super initWithTitle:[NSString stringWithCString:$title]
                      action:@selector(contextMenuItemClicked:)
                      keyEquivalent:@""]) {
        self.target = self;
        self.name = [NSString stringWithCString:$name];
    }

    return self;
}

- (void)contextMenuItemClicked:(NotifyContextMenuItem *)$sender {  // NOLINT
    if ($sender != nil && $sender.title != nil) {
        Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler::FireEvent("OnNotifyIconContextItemSelected",
                                                                              {[$sender.name UTF8String]});
    }
}

- (void)dealloc {
    if (self.name != nil) {
        [self.name release];
        self.name = nil;
    }

    [super dealloc];
}

@end

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Connectors::WindowHandler;

    bool NotifyIconMac::Create(const WindowHandlerNotifyIcon &$options,
                               const RootWindow::NotificationIconContextMenu &$contextMenu) {
        if (!this->created) {
            if (this->icon == nullptr) {
                this->icon = [[NotifyIconWrapper alloc] init];

                // unfortunately, there's no other way to dynamically add items, we need to pre-allocate menu with dummy ones
                for (size_t i = 0; i < $contextMenu.size(); ++i) {
                    NSMenuItem *menuItem = [[[NSMenuItem alloc] initWithTitle:@""
                                                                action:nil
                                                                keyEquivalent:@""] autorelease];

                    [this->icon.menu addItem:menuItem];
                }

                for (size_t i = 0; i < $contextMenu.size(); ++i) {
                    const auto &item = $contextMenu.at(i + NotifyIcon::GetIndexModifier());


                    NSMenuItem *menuItem = [[[NotifyContextMenuItem alloc] initWithTitle:item.getLabel().c_str()
                                                                           name:item.getName().c_str()] autorelease];

                    [this->icon.menu removeItemAtIndex:item.getPosition()];
                    [this->icon.menu insertItem:menuItem
                                     atIndex:item.getPosition()];
                }
            }

            if (this->updateNotifyIcon($options)) {
                this->onCreate(true);

                return true;
            }
        }

        return false;
    }

    bool NotifyIconMac::Destroy() {
        if (this->created) {
            if (this->icon != nullptr) {
                [this->icon release];
                this->icon = nullptr;

                this->onDestroy(true);

                return true;
            }
        }

        return false;
    }

    bool NotifyIconMac::updateNotifyIcon(const WindowHandlerNotifyIcon &/*$options*/) {
        return true;
    }
}

#endif  // MAC_PLATFORM
