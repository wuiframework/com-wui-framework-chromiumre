/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWLINUX_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWLINUX_HPP_

#ifdef LINUX_PLATFORM

#include "RootWindow.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class RootWindowLinux
            : public Com::Wui::Framework::ChromiumRE::Browser::RootWindow {
        typedef Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Com::Wui::Framework::ChromiumRE::Enums::WindowStateType WindowStateType;
        typedef Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IWindowDelegate IWindowDelegate;

     public:
        using RootWindow::RootWindow;

        void Show(WindowStateType $mode) override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        void Close(bool $force) override;

        WindowStateType getWindowState() const override;

        void Minimize() override;

        void Maximize() override;

        void Restore() override;

     private:
        void createBrowserWindow(const string &$url) override;

        void createRootWindow(const CefBrowserSettings &$settings) override;

        void createRootWindowWrapper(const CefBrowserSettings &$settings) override;

        static gboolean OnClose(GtkWidget *$window, GdkEvent *$event, RootWindowLinux *$self);

        static void OnSize(GtkWidget *$widget, GtkAllocation *$allocation, RootWindowLinux *$self);

        static gboolean OnMove(GtkWidget *$widget, GdkEvent *$event, RootWindowLinux *$self);

        static gboolean OnFocus(GtkWidget *$widget, GdkEventFocus *$event, RootWindowLinux *$self);

        static void OnDestroyed(GtkWidget *$event, RootWindowLinux *$self);

        static void OnWindowStateChange(GtkWidget *$widget, GdkEvent *$event, RootWindowLinux *$self);

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserWindowDestroyed() override;

        void OnSetTitle(const string &$title) override;

        WindowStateType windowState = WindowStateType::NORMAL;

        void flushDisplay();

        void updateWindowState(WindowStateType $newState);

        DISALLOW_COPY_AND_ASSIGN(RootWindowLinux);
    };
}

#endif  // LINUX_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWLINUX_HPP_
