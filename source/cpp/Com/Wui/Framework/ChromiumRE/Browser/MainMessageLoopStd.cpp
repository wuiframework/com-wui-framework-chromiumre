/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {

    int MainMessageLoopStd::Run() {
        CefRunMessageLoop();
        return 0;
    }

    void MainMessageLoopStd::Quit() {
        CefQuitMessageLoop();
    }

    void MainMessageLoopStd::PostTask(CefRefPtr<CefTask> $task) {
        CefPostTask(TID_UI, $task);
    }

    bool MainMessageLoopStd::RunsTasksOnCurrentThread() const {
        return CefCurrentlyOn(TID_UI);
    }

#if defined(OS_WIN)

    void MainMessageLoopStd::SetCurrentModelessDialog(HWND $hWndDialog) {
        // Nothing to do here. The Chromium message loop implementation will
        // internally route dialog messages.
    }

#endif
}
