/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_

#ifdef LINUX_PLATFORM

#include "DialogHandlerLinux.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class ClientHandlerStdLinux
            : public Com::Wui::Framework::ChromiumRE::Browser::ClientHandlerStd {
     public:
        using ClientHandlerStd::ClientHandlerStd;

     private:
        CefRefPtr<CefDialogHandler> GetDialogHandler() override;

        CefRefPtr<DialogHandlerLinux> dialogHandler = new DialogHandlerLinux();

        IMPLEMENT_REFCOUNTING(ClientHandlerStdLinux);
        DISALLOW_COPY_AND_ASSIGN(ClientHandlerStdLinux);
    };
}

#endif  // LINUX_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_
