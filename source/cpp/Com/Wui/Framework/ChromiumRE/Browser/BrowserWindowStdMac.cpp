/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <AppKit/NSView.h>

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate;
using Com::Wui::Framework::XCppCommons::Utils::LogIt;

namespace Com::Wui::Framework::ChromiumRE::Browser {
    BrowserWindowStdMac::BrowserWindowStdMac(IBrowserWindowDelegate *$delegate, const string &$startupUrl)
            : BrowserWindow($delegate) {
        this->clientHandler = new ClientHandlerStd(this, $startupUrl);
    }

    void BrowserWindowStdMac::CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect,
                                            const CefBrowserSettings &$settings, const CefString &$url,
                                            CefRefPtr<CefRequestContext> $requestContext) {
        REQUIRE_MAIN_THREAD();

        CefWindowInfo windowInfo;
        windowInfo.SetAsChild($parentHandle.contentView, 0, 0, $rect.width, $rect.height);

        CefBrowserHost::CreateBrowser(std::move(windowInfo), this->clientHandler, $url, $settings, $requestContext);
    }

    void BrowserWindowStdMac::Show() {
    }

    void BrowserWindowStdMac::Hide() {
    }

    void BrowserWindowStdMac::SetBounds(int $x, int $y, size_t $width, size_t $height) {
    }

    ClientWindowHandle BrowserWindowStdMac::GetWindowHandle() const {
        return nullptr;
    }
}

#endif  // MAC_PLATFORM
