/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONLINUX_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONLINUX_HPP_

#ifdef LINUX_PLATFORM

#include <libappindicator/app-indicator.h>

#include "NotifyIcon.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class NotifyIconLinux : public NotifyIcon {
     public:
        explicit NotifyIconLinux(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner);

        bool Create(const WindowHandlerNotifyIcon &$options,
                    const RootWindow::NotificationIconContextMenu &$contextMenu) override;

        bool Destroy() override;

     private:
        // This has to be static, because there's no real way to destroy the AppIndicator instance, so we're just re-using
        // and updating (if necessary) this particular instance.
        static AppIndicator *indicator;
        GtkWidget *contextMenu = nullptr;
        string defaultIconPath;

        static void contextMenuItemClicked(GtkWidget *$menuItem, const char *$name);

        bool updateNotifyIcon(const WindowHandlerNotifyIcon &$options) override;
    };
}

#endif  // LINUX_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_NOTIFYICONLINUX_HPP_
