/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {

    scoped_refptr<RootWindow> RootWindow::Create(bool $useViews) {
        if ($useViews) {
            LOG(FATAL) << "Views framework is not supported on this platform.";
        }

#ifdef WIN_PLATFORM
        return new RootWindowWin();
#elif LINUX_PLATFORM
        return new RootWindowLinux();
#elif MAC_PLATFORM
        return new RootWindowMac();
#endif
    }
}
