/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

@implementation TaskBarWrapper

- (id)init {
    if (self = [super init]) {
        NSRect progressBarRectangle = NSMakeRect(0.0f, 0.0f, [NSApplication sharedApplication].dockTile.size.width, 10.0);
        self.progressBar = [[NSProgressIndicator alloc] initWithFrame:progressBarRectangle];
        self.progressBar.style = NSProgressIndicatorBarStyle;
        self.progressBar.indeterminate = NO;
        self.progressBar.minValue = 0;
        self.progressBar.maxValue = 100;
        self.progressBar.doubleValue = 0;

        self.dockView = [NSImageView imageViewWithImage:[NSApplication sharedApplication].applicationIconImage];
        [self.dockView addSubview:self.progressBar];
    }

    return self;
}

- (void)ShowProgressBar {
    [NSApplication sharedApplication].dockTile.contentView = self.dockView;

    [[NSApplication sharedApplication].dockTile display];
}

- (void)HideProgressBar {
    [NSApplication sharedApplication].dockTile.contentView = nil;

    [[NSApplication sharedApplication].dockTile display];
}

- (void)dealloc {
    self.dockView = nil;

    if (self.progressBar != nil) {
        [self.progressBar release];
        self.progressBar = nil;
    }

    [super dealloc];
}

@end

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState;

    TaskBarMac::TaskBarMac(scoped_refptr<RootWindow> $owner)
        : TaskBar($owner) {
        this->taskBar = [[TaskBarWrapper alloc] init];
    }

    TaskBarMac::~TaskBarMac() {
        this->Clear();
    }

    bool TaskBarMac::setProgressState(const TaskBarProgressState &$state) {
        return false;
    }

    bool TaskBarMac::setProgressValue(const int $completed, const int $total) {
        if (this->taskBar != nil) {
            this->taskBar.progressBar.maxValue = $total;
            this->taskBar.progressBar.doubleValue = $completed;

            if ($total == $completed) {
                [this->taskBar HideProgressBar];
            } else {
                [this->taskBar ShowProgressBar];
            }

            return true;
        } else {
            return false;
        }
    }

    void TaskBarMac::Clear() {
        if (this->taskBar != nil) {
            [this->taskBar release];
            this->taskBar = nil;
        }
    }
}

#endif  // MAC_PLATFORM
