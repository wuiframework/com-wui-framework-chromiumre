/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXTIMPL_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXTIMPL_HPP_

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Used to store global context in the browser process.
     */
    class MainContextImpl
            : public Com::Wui::Framework::ChromiumRE::Browser::MainContext {
     public:
        MainContextImpl(CefRefPtr<CefCommandLine> $commandLine, bool $terminateWhenAllWindowsClosed);

        string getConsoleLogPath() override;

        string getDownloadPath(const string &$fileName) override;

        string getAppWorkingDirectory() override;

        cef_color_t getBackgroundColor() override;

        bool UseViews() override;

        void PopulateSettings(CefSettings *$settings) override;

        void PopulateBrowserSettings(CefBrowserSettings *$settings) override;

        RootWindowManager *getRootWindowManager() override;

        /**
         * Initialize CEF and associated main context state. This method must be called on the same thread that created this object.
         * @param $args Specify input arguments.
         * @param $settings Specify CEF general settings.
         * @param $application Specify reference to main application instance.
         * @param $windowsSandboxInfo
         * @return Returns true if succeed, false otherwise.
         */
        bool Initialize(const CefMainArgs &$args,
                        const CefSettings &$settings,
                        CefRefPtr<CefApp> $application,
                        void *$windowsSandboxInfo);

        /**
         * Shut down CEF and associated context state. This method must be called on
         * the same thread that created this object.
         */
        void Shutdown();

     private:
        friend struct base::DefaultDeleter<MainContextImpl>;

        ~MainContextImpl();

        CefRefPtr<CefCommandLine> commandLine;
        const bool terminateWhenAllWindowsClosed;
        bool initialized;
        bool shutdown;
        cef_color_t backgroundColor;
        bool useViews;
        scoped_ptr<RootWindowManager> rootWindowManager;
        base::ThreadChecker threadChecker;

        DISALLOW_COPY_AND_ASSIGN(MainContextImpl);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_MAINCONTEXTIMPL_HPP_
