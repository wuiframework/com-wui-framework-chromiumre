/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_DEBUGWINDOWWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_DEBUGWINDOWWIN_HPP_

#ifdef WIN_PLATFORM

#include "RootWindowWin.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class DebugWindow
            : public Com::Wui::Framework::ChromiumRE::Browser::RootWindowWin {
        typedef Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IWindowDelegate IWindowDelegate;
        typedef Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Com::Wui::Framework::ChromiumRE::Enums::WindowStateType WindowStateType;

     public:
        using RootWindowWin::RootWindowWin;

        void Close(bool $force) override;

        void Show(WindowStateType $mode) override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        WindowStateType getWindowState() const override;

        void ExecuteScript(const json &$options, const shared_ptr<QueryResponse> $response) override;

        void SendScriptResponse(const string &$response) const override;

        bool CreateNotifyIcon(const Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon &$options,
                              const shared_ptr<QueryResponse> $response) override;

        bool ModifyNotifyIcon(const Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon &$options,
                              const shared_ptr<QueryResponse> $response) override;

        bool DestroyNotifyIcon(const shared_ptr<QueryResponse> $response) override;

        void Minimize() override;

        void Maximize() override;

        void Restore() override;

        void OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) override;

        static std::wstring getWindowTypeName() { return L"chromiumRE_DevTools"; }

     private:
        void CreateBrowserWindow(const string &$url);

        void CreateRootWindow(const CefBrowserSettings &$settings);

        static void RegisterRootClass(HINSTANCE $hInstance, const std::wstring &$windowClass, HBRUSH $backgroundBrush);

        static LRESULT CALLBACK RootWndProc(HWND $hWnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

        static void CALLBACK DelayedSingleClick(HWND $hwnd, UINT, UINT_PTR $id, DWORD);

        void OnPaint();

        void OnFocus();

        void OnSize(bool $minimized);

        void OnMove();

        bool OnEraseBkgnd();

        void OnFindEvent();

        bool OnClose();

        void OnDestroyed();

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserWindowDestroyed() override;

        void OnSetTitle(const string &$title) override;

        DISALLOW_COPY_AND_ASSIGN(DebugWindow);
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_DEBUGWINDOWWIN_HPP_
