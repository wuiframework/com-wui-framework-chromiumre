/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWWIN_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWWIN_HPP_

#ifdef WIN_PLATFORM

#include "RootWindow.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    class RootWindowWin
            : public Com::Wui::Framework::ChromiumRE::Browser::RootWindow {
        typedef Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Com::Wui::Framework::ChromiumRE::Enums::WindowStateType WindowStateType;
        typedef Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IWindowDelegate IWindowDelegate;

     public:
        using RootWindow::RootWindow;

        void Show(WindowStateType $mode) override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        void Close(bool $force) override;

        WindowStateType getWindowState() const override;

        void Minimize() override;

        void Maximize() override;

        void Restore() override;

        static std::wstring getWindowTypeName() { return L"chromiumRE"; }

     protected:
        static BOOL CALLBACK EnumWindowsProc(HWND $hwnd, LPARAM $lParam);

        static std::vector<HWND> getWinHandles();

        HWND findHwnd = nullptr;
        UINT findMessageId = 0;
        WNDPROC findWndprocOld = nullptr;
        FINDREPLACE findState = { 0 };
        WCHAR findBuff[80] = { 0 };
        std::wstring findWhatLast;
        bool findNext = false;
        bool findMatchCaseLast = false;

     private:
        void createBrowserWindow(const string &$url) override;

        void createRootWindow(const CefBrowserSettings &$settings) override;

        void createRootWindowWrapper(const CefBrowserSettings &$settings) override;

        static void RegisterRootClass(HINSTANCE $hInstance, const std::wstring &$windowClass, HBRUSH $backgroundBrush);

        static LRESULT CALLBACK RootWndProc(HWND $hWnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

        static void CALLBACK DelayedSingleClick(HWND $hwnd, UINT, UINT_PTR $id, DWORD);

        void OnPaint();

        void OnSize(bool $minimized);

        bool OnEraseBkgnd();

        void OnFindEvent();

        bool OnClose();

        void OnDestroyed();

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserWindowDestroyed() override;

        void OnSetTitle(const string &$title) override;

        void setShadow(HWND $hwnd, bool $enabled);

        DISALLOW_COPY_AND_ASSIGN(RootWindowWin);
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_ROOTWINDOWWIN_HPP_
