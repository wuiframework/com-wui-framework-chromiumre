/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    namespace fs = boost::filesystem;

    NotifyIcon::NotifyIcon(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner)
            : owner($owner) {
    }

    bool NotifyIcon::Modify(const WindowHandlerNotifyIcon &$options, const RootWindow::NotificationIconContextMenu &$contextMenu) {
        if (!this->created) {
            return this->Create($options, $contextMenu);
        } else {
            if (this->updateNotifyIcon($options)) {
                this->onModify(true);
                return true;
            }
        }
        return false;
    }

    scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> NotifyIcon::getOwner() const {
        return this->owner;
    }

    void NotifyIcon::setOwner(scoped_refptr<Com::Wui::Framework::ChromiumRE::Browser::RootWindow> $owner) {
        if ($owner) {
            this->owner = $owner;
        }
    }

    void NotifyIcon::onCreate(const bool $success) {
        if ($success) {
            this->created = true;
            LogIt::Info("Tray icon created for window: {0}", this->owner->getId());
        } else {
            LogIt::Error("Tray icon creation failed.");
        }
    }

    void NotifyIcon::onDestroy(const bool $success) {
        if ($success) {
            this->created = false;
            LogIt::Info("Tray icon destroyed for window: {0}.", this->owner->getId());
        } else {
            LogIt::Error("Tray icon can not be deleted.");
        }
    }

    void NotifyIcon::onModify(const bool $success) {
        if ($success) {
            LogIt::Info("Modified tray icon attached to window: {0}", this->owner->getId());
        } else {
            LogIt::Error("Notify icon can not be modified.");
        }
    }

    bool NotifyIcon::iconFileExist(const string &$path) const {
        if ($path.empty()) {
            LogIt::Error("Empty path given for icon file");

            return false;
        } else {
            boost::system::error_code ec;
            const bool exists = fs::exists($path, ec);

            if (!exists) {
                LogIt::Error("Can not find file {0}. Error: {1}.", $path, ec != nullptr ? ec.message() : "unknown");
            }

            return exists;
        }
    }
}
