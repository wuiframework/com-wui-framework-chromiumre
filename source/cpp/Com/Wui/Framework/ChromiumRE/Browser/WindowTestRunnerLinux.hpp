/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERLINUX_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERLINUX_HPP_

#ifdef LINUX_PLATFORM

namespace Com::Wui::Framework::ChromiumRE::Browser {
    /**
     * Linux platform implementation. Methods are safe to call on any browser
     * process thread.
     */
    class WindowTestRunnerLinux : public Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunner {
    };
}

#endif  // LINUX_PLATFORM

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERLINUX_HPP_
