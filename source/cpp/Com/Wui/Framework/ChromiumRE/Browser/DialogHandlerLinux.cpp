/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    namespace {
        string GetDescriptionFromMimeType(const string &$mimeType) {
            static const struct {
                const char *mimeType = nullptr;
                const char *label = nullptr;
            } kWildCardMimeTypes[] = {
                    {"audio", "Audio Files"},
                    {"image", "Image Files"},
                    {"text",  "Text Files"},
                    {"video", "Video Files"},
            };

            for (size_t i = 0; i < sizeof(kWildCardMimeTypes) / sizeof(kWildCardMimeTypes[0]); ++i) {
                if ($mimeType == std::string(kWildCardMimeTypes[i].mimeType) + "/*")
                    return string(kWildCardMimeTypes[i].label);
            }

            return {};
        }

        void AddFilters(GtkFileChooser *$chooser,
                        const std::vector<CefString> &$acceptFilters,
                        bool $includeAllFiles,
                        std::vector<GtkFileFilter *> *$filters) {
            bool hasFilter = false;

            for (const auto &filter : $acceptFilters) {
                const auto filterString = filter.ToString();

                if (filter.empty()) {
                    continue;
                }

                std::vector<string> extensions;
                string description;

                const size_t separatorIndex = filterString.find('|');
                if (separatorIndex != string::npos) {
                    description = filterString.substr(0, separatorIndex);

                    auto extensions = filterString.substr(separatorIndex + 1);
                    size_t last = 0;
                    size_t size = extensions.size();
                    for (size_t i = 0; i <= size; ++i) {
                        if (i == size || extensions[i] == ';') {
                            string ext(extensions, last, i - last);
                            if (!ext.empty() && ext[0] == '.') {
                                extensions.append(ext);
                            }
                            last = i + 1;
                        }
                    }
                } else if (filterString.front() == '.') {
                    extensions.emplace_back(std::move(filterString));
                } else {
                    description = GetDescriptionFromMimeType(filter);

                    std::vector<CefString> ext;
                    CefGetExtensionsForMimeType(filter, ext);
                    for (const auto &ext : ext) {
                        extensions.emplace_back("." + ext.ToString());
                    }
                }

                if (extensions.empty()) {
                    continue;
                }

                GtkFileFilter *gtkFilter = gtk_file_filter_new();

                string ext_str;
                for (size_t x = 0; x < extensions.size(); ++x) {
                    const string &pattern = "*" + extensions[x];
                    if (x != 0) {
                        ext_str += ";";
                    }
                    ext_str += pattern;
                    gtk_file_filter_add_pattern(gtkFilter, pattern.c_str());
                }

                if (description.empty()) {
                    description = ext_str;
                } else {
                    description += " (" + ext_str + ")";
                }

                gtk_file_filter_set_name(gtkFilter, description.c_str());
                gtk_file_chooser_add_filter($chooser, gtkFilter);
                if (!hasFilter) {
                    hasFilter = true;
                }

                $filters->push_back(gtkFilter);
            }

            if ($includeAllFiles && hasFilter) {
                GtkFileFilter *filter = gtk_file_filter_new();
                gtk_file_filter_add_pattern(filter, "*");
                gtk_file_filter_set_name(filter, "All Files (*)");
                gtk_file_chooser_add_filter($chooser, filter);
            }
        }

        GtkWindow *GetWindow(CefRefPtr<CefBrowser> $browser) {
            REQUIRE_MAIN_THREAD();
            scoped_refptr<RootWindow> rootWindow = RootWindow::GetForBrowser($browser->GetIdentifier());
            if (rootWindow != nullptr) {
                GtkWindow *window = GTK_WINDOW(rootWindow->getWindowHandle());

                return window;
            }

            return nullptr;
        }

        void RunCallback(base::Callback<void(GtkWindow *)> $callback, GtkWindow *$window) {
            $callback.Run($window);
        }

    }

    bool DialogHandlerLinux::OnFileDialog(CefRefPtr<CefBrowser> $browser,
                                          FileDialogMode $mode,
                                          const CefString &$title,
                                          const CefString &$defaultFilePath,
                                          const std::vector<CefString> &$acceptFilters,
                                          int $selectedAcceptFilter,
                                          CefRefPtr<CefFileDialogCallback> $callback) {
        CEF_REQUIRE_UI_THREAD();

        OnFileDialogParams params;
        params.browser = $browser;
        params.mode = $mode;
        params.title = $title;
        params.defaultFilePath = $defaultFilePath;
        params.acceptFilters = $acceptFilters;
        params.selectedAcceptFilter = $selectedAcceptFilter;
        params.callback = $callback;

        getWindowAndContinue($browser, base::Bind(&DialogHandlerLinux::onFileDialogContinue, this, std::move(params)));

        return true;
    }

    void DialogHandlerLinux::onFileDialogContinue(OnFileDialogParams $params, GtkWindow *$window) {
        CEF_REQUIRE_UI_THREAD();

        std::vector<CefString> files;
        GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
        const gchar *acceptButton = nullptr;

        const FileDialogMode modeType = static_cast<FileDialogMode>($params.mode & FILE_DIALOG_TYPE_MASK);

        if (modeType == FILE_DIALOG_OPEN || modeType == FILE_DIALOG_OPEN_MULTIPLE) {
            action = GTK_FILE_CHOOSER_ACTION_OPEN;
            acceptButton = GTK_STOCK_OPEN;
        } else if (modeType == FILE_DIALOG_OPEN_FOLDER) {
            action = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
            acceptButton = GTK_STOCK_OPEN;
        } else if (modeType == FILE_DIALOG_SAVE) {
            action = GTK_FILE_CHOOSER_ACTION_SAVE;
            acceptButton = GTK_STOCK_SAVE;
        } else {
            $params.callback->Cancel();
            return;
        }

        string title;
        if (!$params.title.empty()) {
            title = $params.title;
        } else {
            switch (modeType) {
                case FILE_DIALOG_OPEN:
                    title = "Open File";
                    break;
                case FILE_DIALOG_OPEN_MULTIPLE:
                    title = "Open Files";
                    break;
                case FILE_DIALOG_OPEN_FOLDER:
                    title = "Open Folder";
                    break;
                case FILE_DIALOG_SAVE:
                    title = "Save File";
                    break;
                default:
                    break;
            }
        }

        GtkWidget *dialog = gtk_file_chooser_dialog_new(title.c_str(),
                                                        GTK_WINDOW($window),
                                                        action,
                                                        GTK_STOCK_CANCEL,
                                                        GTK_RESPONSE_CANCEL,
                                                        acceptButton,
                                                        GTK_RESPONSE_ACCEPT,
                                                        nullptr);

        if (modeType == FILE_DIALOG_OPEN_MULTIPLE) {
            gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), TRUE);
        }

        if (modeType == FILE_DIALOG_SAVE) {
            gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog),
                                                           !!($params.mode & FILE_DIALOG_OVERWRITEPROMPT_FLAG));
        }

        gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(dialog), !($params.mode & FILE_DIALOG_HIDEREADONLY_FLAG));

        if (!$params.defaultFilePath.empty() && modeType == FILE_DIALOG_SAVE) {
            const string &filePath = $params.defaultFilePath;
            bool exists = false;

            struct stat sb = {0};
            if (stat(filePath.c_str(), &sb) == 0 && S_ISREG(sb.st_mode)) {
                gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(dialog), filePath.data());
                exists = true;
            }

            if (!exists) {
                string fileNameStr = filePath;
                const char *fileName = basename(const_cast<char *>(fileNameStr.data()));
                gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), fileName);
            }
        }

        std::vector<GtkFileFilter *> filters;
        AddFilters(GTK_FILE_CHOOSER(dialog), $params.acceptFilters, true, &filters);
        if ($params.selectedAcceptFilter < static_cast<int>(filters.size())) {
            gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filters[$params.selectedAcceptFilter]);
        }

        bool success = false;

        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            if (modeType == FILE_DIALOG_OPEN || modeType == FILE_DIALOG_OPEN_FOLDER || modeType == FILE_DIALOG_SAVE) {
                char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
                files.push_back(string(filename));
                success = true;
            } else if (modeType == FILE_DIALOG_OPEN_MULTIPLE) {
                GSList *filenames = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));
                if (filenames != nullptr) {
                    for (GSList *iter = filenames; iter != nullptr; iter = g_slist_next(iter)) {
                        string path(static_cast<char *>(iter->data));
                        g_free(iter->data);
                        files.push_back(path);
                    }

                    g_slist_free(filenames);
                    success = true;
                }
            }
        }

        int filterIndex = $params.selectedAcceptFilter;
        if (success) {
            GtkFileFilter *selectedFilter = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
            if (selectedFilter != nullptr) {
                for (size_t x = 0; x < filters.size(); ++x) {
                    if (filters[x] == selectedFilter) {
                        filterIndex = x;
                        break;
                    }
                }
            }
        }

        gtk_widget_destroy(dialog);

        if (success) {
            $params.callback->Continue(filterIndex, files);
        } else {
            $params.callback->Cancel();
        }
    }


    void DialogHandlerLinux::getWindowAndContinue(CefRefPtr<CefBrowser> $browser,
                                                  base::Callback<void(GtkWindow *)> $callback) {
        if (!CURRENTLY_ON_MAIN_THREAD()) {
            MAIN_POST_CLOSURE(base::Bind(&DialogHandlerLinux::getWindowAndContinue, this, $browser, $callback));
        } else {
            GtkWindow *window = GetWindow($browser);
            if (window != nullptr) {
                CefPostTask(TID_UI, base::Bind(RunCallback, $callback, window));
            }
        }
    }
}

#endif  // LINUX_PLATFORM
