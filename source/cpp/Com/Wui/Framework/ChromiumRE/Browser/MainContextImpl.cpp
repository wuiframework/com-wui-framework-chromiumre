/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Browser {
    using Com::Wui::Framework::ChromiumRE::Commons::Configuration;
    using Com::Wui::Framework::ChromiumRE::Browser::RootWindowManager;

// cppcheck-suppress uninitMemberVar
    MainContextImpl::MainContextImpl(CefRefPtr<CefCommandLine> $commandLine, bool $terminateWhenAllWindowsClosed)
            : commandLine($commandLine),
              terminateWhenAllWindowsClosed($terminateWhenAllWindowsClosed),
              initialized(false),
              shutdown(false),
              backgroundColor(CefColorSetARGB(255, 255, 255, 255)),
              useViews(false) {
    }

    MainContextImpl::~MainContextImpl() {}

    string MainContextImpl::getConsoleLogPath() {
        return getAppWorkingDirectory() + "console.log";
    }

    cef_color_t MainContextImpl::getBackgroundColor() {
        return this->backgroundColor;
    }

    bool MainContextImpl::UseViews() {
        return this->useViews;
    }

    void MainContextImpl::PopulateSettings(CefSettings *$settings) {
        $settings->background_color = this->backgroundColor;
        $settings->remote_debugging_port = $settings->remote_debugging_port;
    }

    void MainContextImpl::PopulateBrowserSettings(CefBrowserSettings *$settings) {
        $settings->file_access_from_file_urls = cef_state_t::STATE_ENABLED;
        $settings->local_storage = cef_state_t::STATE_ENABLED;
    }

    RootWindowManager *MainContextImpl::getRootWindowManager() {
        return this->rootWindowManager.get();
    }

    bool MainContextImpl::Initialize(const CefMainArgs &$args, const CefSettings &$settings, CefRefPtr<CefApp> $application,
                                     void *$windowsSandboxInfo) {
        DCHECK(this->threadChecker.CalledOnValidThread());
        DCHECK(!this->initialized);
        DCHECK(!this->shutdown);

        if (!CefInitialize($args, $settings, $application, $windowsSandboxInfo)) {
            return false;
        }

        this->rootWindowManager.reset(new RootWindowManager(this->terminateWhenAllWindowsClosed));

        this->initialized = true;

        return true;
    }

    void MainContextImpl::Shutdown() {
        DCHECK(this->threadChecker.CalledOnValidThread());
        DCHECK(this->initialized);
        DCHECK(!this->shutdown);

        this->rootWindowManager.reset();

        // known bug in CEF that is stucks during the "CefShutdown" on MAC
#ifndef MAC_PLATFORM
        CefShutdown();
#endif

        this->shutdown = true;
    }
}
