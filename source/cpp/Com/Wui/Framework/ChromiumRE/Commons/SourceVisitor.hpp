/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SOURCEVISITOR_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SOURCEVISITOR_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class SourceVisitor : public CefStringVisitor {
        using QueryResponse = Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;

     public:
        void Visit(const CefString &$data) override;

        void setResponse(const shared_ptr<QueryResponse> $response);

        void setBrowser(CefRefPtr<CefBrowser> $browser);

        void setRootId(const string &$id);

        IMPLEMENT_REFCOUNTING(SourceVisitor);

     private:
        shared_ptr<QueryResponse> response = nullptr;
        CefRefPtr<CefBrowser> browser = nullptr;
        string rootId;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SOURCEVISITOR_HPP_
