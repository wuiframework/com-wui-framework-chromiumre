/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {

    void WaitableEvent::Signal() {
        {
            std::lock_guard<decltype(this->cv_protection)> cv_lock(this->cv_protection);

            this->completed = true;
        }

        this->cv.notify_one();
    }

    void WaitableEvent::Wait() {
        std::unique_lock<decltype(this->cv_protection)> cv_lock(this->cv_protection);

        this->cv.wait(cv_lock, [&] {
            return this->completed;
        });
    }
}
