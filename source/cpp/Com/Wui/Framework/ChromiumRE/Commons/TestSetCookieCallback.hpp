/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTSETCOOKIECALLBACK_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTSETCOOKIECALLBACK_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class TestSetCookieCallback : public CefSetCookieCallback {
     public:
        TestSetCookieCallback(bool $expectedSuccess, Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *$event)
                : expectedSuccess($expectedSuccess),
                  event($event) {}

        void OnComplete(bool $success) OVERRIDE {
            event->Signal();
        }

     private:
        bool expectedSuccess;
        Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *event;

        /**/IMPLEMENT_REFCOUNTING(TestSetCookieCallback);
        DISALLOW_COPY_AND_ASSIGN(TestSetCookieCallback);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTSETCOOKIECALLBACK_HPP_
