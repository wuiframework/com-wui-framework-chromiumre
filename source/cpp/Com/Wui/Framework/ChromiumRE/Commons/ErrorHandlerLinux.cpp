/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons::ErrorHandler {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    int XErrorHandlerImpl(Display */*$display*/, XErrorEvent *$event) {
        LogIt::Warning("X error received of type {0}, serial {1}, error code {2}",
                       $event->type, $event->serial, std::to_string($event->error_code));

        return 0;
    }

    int XIOErrorHandlerImpl(Display */*$display*/) {
        return 0;
    }
}

#endif  // LINUX_PLATFORM
