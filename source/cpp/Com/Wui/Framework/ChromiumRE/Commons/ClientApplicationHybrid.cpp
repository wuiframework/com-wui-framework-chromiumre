/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    CefRefPtr<CefBrowserProcessHandler> ClientApplicationHybrid::GetBrowserProcessHandler() {
        return this->browserApp;
    }

    CefRefPtr<CefRenderProcessHandler> ClientApplicationHybrid::GetRenderProcessHandler() {
        return this->rendererApp;
    }
}
