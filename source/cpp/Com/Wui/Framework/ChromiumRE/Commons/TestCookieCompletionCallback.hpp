/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTCOOKIECOMPLETIONCALLBACK_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTCOOKIECOMPLETIONCALLBACK_HPP_

#include "WaitableEvent.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class TestCookieCompletionCallback : public CefCompletionCallback {
     public:
        explicit TestCookieCompletionCallback(Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *$event)
                : event($event) {}

        void OnComplete() override {
            event->Signal();
        }

     private:
        Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *event;

        /**/IMPLEMENT_REFCOUNTING(TestCookieCompletionCallback);
        DISALLOW_COPY_AND_ASSIGN(TestCookieCompletionCallback);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_TESTCOOKIECOMPLETIONCALLBACK_HPP_
