/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {

    Configuration::Configuration() {}

    Configuration::~Configuration() {}

    const string &Configuration::getTargetFilePath() const {
        return this->targetFilePath;
    }

    void Configuration::setTargetFilePath(const string &$path) {
        this->targetFilePath = $path;
    }

    const string &Configuration::getApplicationQuery() const {
        return this->query;
    }

    void Configuration::setApplicationQuery(const string &$query) {
        this->query = $query;
    }

    const string &Configuration::getStartPage() const {
        return this->startPage;
    }

    void Configuration::setStartPage(const string &$url) {
        this->startPage = $url;
    }

    const string Configuration::getTargetUrl() const {
        string url = this->targetFilePath;
        if (!this->query.empty()) {
            url += "?" + this->query;
        }
        if (!this->startPage.empty()) {
            url += "#" + this->startPage;
        }
        return url;
    }

    const int &Configuration::getWindowWidth() const {
        return this->width;
    }

    void Configuration::setWindowWidth(const int &$value) {
        this->width = $value;
    }

    const int &Configuration::getWindowHeight() const {
        return this->height;
    }

    void Configuration::setWindowHeight(const int &$value) {
        this->height = $value;
    }

    bool Configuration::IsCanResize() const {
        return this->canResize;
    }

    void Configuration::setCanResize(bool $canResize) {
        this->canResize = $canResize;
    }

    bool Configuration::IsDownloadDialogEnabled() const {
        return this->downloadDialogEnabled;
    }

    void Configuration::setDownloadDialogEnabled(bool $value) {
        this->downloadDialogEnabled = $value;
    }

    bool Configuration::IsSingleProcess() const {
        return this->singleProcess;
    }

    void Configuration::setSingleProcess(bool $singleProcess) {
        this->singleProcess = $singleProcess;
    }

    const string &Configuration::getAppExeName() const {
        return this->appExeName;
    }

    void Configuration::setAppExeName(const string &$appExeName) {
        this->appExeName = $appExeName;
    }

    int Configuration::getRemoteDebuggingPort() const {
        return this->remoteDebuggingPort;
    }

    void Configuration::setRemoteDebuggingPort(int $remoteDebuggingPort) {
        this->remoteDebuggingPort = $remoteDebuggingPort;
    }

    bool Configuration::isMaximized() const {
        return this->maximized;
    }

    void Configuration::setMaximized(bool $maximized) {
        this->maximized = $maximized;
    }
}
