/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEELEMENT_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEELEMENT_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    template<class T>
    class CookieElement {
     private:
        CefCookie *cookie;

     public:
        // cppcheck-suppress uninitMemberVar
        CookieElement() {
        }

        explicit CookieElement(CefCookie *$cookie)
                : cookie($cookie) {
        }

        T getValue() {
            if (CefString(&this->cookie->value).empty()) {
                return T();
            }
            return boost::lexical_cast<T>(CefString(&this->cookie->value).ToString());
        }

        void setValue(T $val) {
            CefString(&this->cookie->value) = boost::lexical_cast<string>($val);
        }

        string getName() {
            if (CefString(&this->cookie->name).empty()) {
                return "";
            }
            return CefString(&this->cookie->name).ToString();
        }

        void setName(string $val) {
            CefString(&this->cookie->name) = $val;
        }

        CefCookie *getCookie() {
            return this->cookie;
        }

        void setCookie(CefCookie *$val) {
            this->cookie = $val;
        }

        string getPath() const {
            if (CefString(&this->cookie->path).empty()) {
                return "";
            }
            return CefString(&this->cookie->path).ToString();
        }

        void setPath(const string &$val) {
            CefString(&this->cookie->path) = $val;
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEELEMENT_HPP_
