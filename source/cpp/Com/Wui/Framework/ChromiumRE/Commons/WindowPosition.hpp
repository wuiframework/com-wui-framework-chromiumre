/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class WindowPosition {
     private:
        const CefString url = "http://chromiumre.wuiframework.com";
        const std::vector<string> keys = {"w.x", "w.y", "w.w", "w.h", "w.m"};

        int widthDefault = 1260;
        int heightDefault = 710;

        std::vector<CefCookie> cookies;
        std::map<string, Com::Wui::Framework::ChromiumRE::Commons::CookieElement<int>> map;

        string findValidKey(CefCookie *$cookie);

        int getKeyValue(string $key);

        void setKeyValue(string $key, int $val);

     public:
        WindowPosition() {}

        void LoadDefault();

        void Load(CefRefPtr<CefCookieManager> $manager);

        void Save(CefRefPtr<CefCookieManager> $manager);

        void getData(std::vector<CefCookie> *$cc);

        int getX();

        void setX(int $val);

        int getY();

        void setY(int $val);

        int getWidth();

        void setWidth(int $val);

        int getHeight();

        void setHeight(int $val);

        bool getIsMaximized();

        void setIsMaximized(bool $value);

        int getWidthDefault() const {
            return this->widthDefault;
        }

        int getHeightDefault() const {
            return this->heightDefault;
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_
