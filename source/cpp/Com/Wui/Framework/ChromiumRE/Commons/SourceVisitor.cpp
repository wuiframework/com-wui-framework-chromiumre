/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;

    void SourceVisitor::Visit(const CefString &$data) {
        if (this->response) {
            json data = {{"type",     "oncomplete"},
                         {"windowId", this->rootId},
                         {"url",      this->browser->GetMainFrame()->GetURL().ToString()},
                         {"data",     $data.ToString()}};
            this->response->Send(data);
        }
    }

    void SourceVisitor::setResponse(const shared_ptr<QueryResponse> $response) {
        this->response = $response;
    }

    void SourceVisitor::setBrowser(CefRefPtr<CefBrowser> $browser) {
        this->browser = $browser;
    }

    void SourceVisitor::setRootId(const string &$id) {
        this->rootId = $id;
    }
}
