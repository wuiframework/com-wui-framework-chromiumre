/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    using Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent;
    using Com::Wui::Framework::ChromiumRE::Commons::CookieElement;

    string WindowPosition::findValidKey(CefCookie *$cookie) {
        string retVal;
        for (const auto &key : keys) {
            if (boost::algorithm::iequals(key, CefString(&$cookie->name).ToString())) {
                retVal = key;
                break;
            }
        }
        return retVal;
    }

    void WindowPosition::setKeyValue(string $key, int $val) {
        if (this->map.find($key) == this->map.end()) {
            // key not found -> create new
            CookieElement<int> ce(new CefCookie());
            ce.setPath(Commons::Configuration::getInstance().getAppExeName());
            ce.setName($key);
            this->map.emplace($key, ce);
        }
        this->map[$key].setValue($val);
    }

    int WindowPosition::getKeyValue(string $key) {
        if (this->map.find($key) == this->map.end()) {
            // key not found -> create new
            CookieElement<int> ce(new CefCookie());
            ce.setPath(Commons::Configuration::getInstance().getAppExeName());
            ce.setName($key);
            this->map.emplace($key, ce);
        }
        return this->map[$key].getValue();
    }

    void WindowPosition::Load(CefRefPtr<CefCookieManager> $manager) {
        this->LoadDefault();

        WaitableEvent event;
        CefString cUrl = url.ToString() + Commons::Configuration::getInstance().getAppExeName();
        $manager->VisitUrlCookies(cUrl, false, new Commons::CookieVisitor(&cookies, &event));
        event.Wait();
        for (auto &cookie : cookies) {
            string key = this->findValidKey(&cookie);
            if (!key.empty()) {
                this->map[key].setCookie(&cookie);
            }
        }

        if (this->map.find("w.w") == this->map.end() || this->map.find("w.h") == this->map.end()) {
#ifdef WIN_PLATFORM
            this->setX(CW_USEDEFAULT);
            this->setY(CW_USEDEFAULT);
#elif LINUX_PLATFORM
            this->setX(0);
            this->setY(0);
#elif MAC_PLATFORM
            this->setX(0);
            this->setY(UtilMac::GetVisibleFrame(nullptr).size.height - this->getHeight() - this->getY());
#endif
            this->setWidth(Configuration::getInstance().getWindowWidth());
            this->setHeight(Configuration::getInstance().getWindowHeight());
            this->setIsMaximized(Commons::Configuration::getInstance().isMaximized());
        }
    }

    void WindowPosition::LoadDefault() {
        this->widthDefault = Commons::Configuration::getInstance().getWindowWidth();
        this->heightDefault = Commons::Configuration::getInstance().getWindowHeight();
    }

    void WindowPosition::Save(CefRefPtr<CefCookieManager> $manager) {
        std::vector<CefCookie> cc;
        this->getData(&cc);

        for (const auto &i : cc) {
            WaitableEvent event;
            $manager->SetCookie(url, i, new TestSetCookieCallback(true, &event));
            event.Wait();
        }

        WaitableEvent event;
        $manager->FlushStore(new TestCookieCompletionCallback(&event));
        event.Wait();
    }

    void WindowPosition::getData(std::vector<CefCookie> *$cc) {
        $cc->clear();
        // cppcheck-suppress postfixOperator
        for (std::map<string, CookieElement<int>>::const_iterator it = this->map.begin(); it != this->map.end(); it++) {
            CookieElement<int> item = it->second;
            CefCookie e(*item.getCookie());

            $cc->push_back(e);
        }
    }

    int WindowPosition::getX() {
        return this->getKeyValue("w.x");
    }

    void WindowPosition::setX(int $val) {
        this->setKeyValue("w.x", $val);
    }

    int WindowPosition::getY() {
        return this->getKeyValue("w.y");
    }

    void WindowPosition::setY(int $val) {
        this->setKeyValue("w.y", $val);
    }

    int WindowPosition::getWidth() {
        return this->getKeyValue("w.w");
    }

    void WindowPosition::setWidth(int $val) {
        this->setKeyValue("w.w", $val);
    }

    int WindowPosition::getHeight() {
        return this->getKeyValue("w.h");
    }

    void WindowPosition::setHeight(int $val) {
        this->setKeyValue("w.h", $val);
    }

    bool WindowPosition::getIsMaximized() {
        return static_cast<bool>(this->getKeyValue("w.m"));
    }

    void WindowPosition::setIsMaximized(bool $value) {
        this->setKeyValue("w.m", static_cast<int>($value));
    }
}
