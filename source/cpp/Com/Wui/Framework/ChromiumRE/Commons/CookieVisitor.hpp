/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_

#include "WaitableEvent.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class CookieVisitor : public CefCookieVisitor {
     public:
        CookieVisitor(std::vector<CefCookie> *$cookies, Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *$event,
                      bool $usePathFilter = true)
                : cookies($cookies),
                  event($event),
                  usePathFilter($usePathFilter) {
        }

        ~CookieVisitor() override {
            this->event->Signal();
        }

        bool Visit(const CefCookie &$cookie, int $count, int $total, bool &$deleteCookie) override {  // NOLINT
            if (!this->usePathFilter || boost::iequals(CefString(&$cookie.path).ToString(),
                                                       Commons::Configuration::getInstance().getAppExeName())) {
                this->cookies->push_back(CefCookie($cookie));
            }
            return true;  // return true to continue visiting (if other cookie exists);
        }

        std::vector<CefCookie> *getCookies() {
            return this->cookies;
        }

     private:
        std::vector<CefCookie> *cookies;
        Com::Wui::Framework::ChromiumRE::Commons::WaitableEvent *event;
        bool usePathFilter;

        IMPLEMENT_REFCOUNTING(CookieVisitor);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_
