/* ********************************************************************************************************* *
 *
 * Copyright (c) 2009 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SCHEMETESTCOMMON_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SCHEMETESTCOMMON_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class SchemeTestCommon {
     public:
        /**
         * Register the custom scheme name/type. This must be done in all processes.
         * @param $registrar Specify scheme registrar.
         * @param $cookiableSchemes Specify cookiable schemes.
         */
        static void RegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> $registrar, std::vector<CefString> &$cookiableSchemes);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_SCHEMETESTCOMMON_HPP_
