/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <AppKit/NSScreen.h>

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    NSRect UtilMac::GetVisibleFrame(NSWindow *$window) {
        NSScreen *screen = ($window == nil || $window.screen == nil) ? [NSScreen mainScreen] : $window.screen;

        return screen.visibleFrame;
    }

    NSImage *UtilMac::ScaleImage(NSImage *$image, NSSize $size) {
        NSImage *source = $image;
        source.scalesWhenResized = YES;

        if ([source isValid]) {
            NSImage * scaled = [[NSImage alloc] initWithSize:$size];
            [scaled lockFocus];
            [source setSize:$size];
            [NSGraphicsContext.currentContext setImageInterpolation:NSImageInterpolationHigh];
            [source drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, $size.width, $size.height)
                                            operation:NSCompositeCopy
                                            fraction:1.0];
            [scaled unlockFocus];

            return scaled;
        } else {
            return nil;
        }
    }
}

#endif  // MAC_PLATFORM
