/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class DialogCallback : public CefRunFileDialogCallback {
     public:
        explicit DialogCallback(shared_ptr<Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse> $response);

        void OnFileDialogDismissed(int $selectedAcceptFilter, const std::vector<CefString> &$filePaths) override;

     private:
         shared_ptr<Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse> response = nullptr;

         IMPLEMENT_REFCOUNTING(DialogCallback);
         DISALLOW_COPY_AND_ASSIGN(DialogCallback);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_
