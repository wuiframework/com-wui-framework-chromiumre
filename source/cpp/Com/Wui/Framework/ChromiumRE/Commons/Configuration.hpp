/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CONFIGURATION_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CONFIGURATION_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class Configuration {
     public:
        static Configuration &getInstance() {
            static Configuration instance;
            return instance;
        }

     private:
        Configuration();

        Configuration(Configuration const &) = delete;

        void operator=(Configuration const &) = delete;

        ~Configuration();

     public:
        const string &getTargetFilePath() const;

        void setTargetFilePath(const string &$path);

        const string &getApplicationQuery() const;

        void setApplicationQuery(const string &$query);

        const string &getStartPage() const;

        void setStartPage(const string &$url);

        const int &getWindowWidth() const;

        void setWindowWidth(const int &$value);

        const int &getWindowHeight() const;

        void setWindowHeight(const int &$value);

        bool isMaximized() const;

        void setMaximized(bool $maximized);

        const string getTargetUrl() const;

        bool IsCanResize() const;

        void setCanResize(bool $canResize);

        bool IsDownloadDialogEnabled() const;

        void setDownloadDialogEnabled(bool $value);

        bool IsSingleProcess() const;

        void setSingleProcess(bool $singleProcess);

        const string &getAppExeName() const;

        void setAppExeName(const string &$appExeName);

        int getRemoteDebuggingPort() const;

        void setRemoteDebuggingPort(int $remoteDebuggingPort);

     private:
        string targetFilePath;
        string query = "";
        string startPage = "";
        int width = 1260;
        int height = 710;
        bool maximized = false;
        bool canResize = true;
        bool downloadDialogEnabled = false;
        bool singleProcess = false;
        string appExeName = "WuiChromiumRE";
        int remoteDebuggingPort = 0;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CONFIGURATION_HPP_
