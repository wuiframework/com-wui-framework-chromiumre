/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WAITABLEEVENT_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WAITABLEEVENT_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class WaitableEvent {
     public:
        void Signal();

        void Wait();

     private:
        bool completed = false;
        std::mutex cv_protection;
        std::condition_variable cv;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_WAITABLEEVENT_HPP_
