/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <regex>

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    using Com::Wui::Framework::XCppCommons::EnvironmentArgs;
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    string UtilLinux::GetAppIconPath() {
        string iconPath;
        const string desktopFilePath = EnvironmentArgs::getInstance().getExecutablePath() + "/" +
                                       EnvironmentArgs::getInstance().getExecutableName() + ".desktop";

        if (FileSystem::Exists(desktopFilePath)) {
            std::ifstream desktopFile(desktopFilePath);
            const std::string content((std::istreambuf_iterator<char>(desktopFile)), std::istreambuf_iterator<char>());

            const std::regex patternToSearch("Icon=(.+)\n");
            std::smatch matches;
            if (std::regex_search(std::cbegin(content), std::cend(content), matches, patternToSearch)) {
                iconPath = matches[1];
            }
        }

        if (iconPath.empty() || !FileSystem::Exists(iconPath)) {
            iconPath = EnvironmentArgs::getInstance().getExecutablePath() + "/../../resource/graphics/icon.ico";

            LogIt::Warning("Icon path does not exist, falling back to the icon from resources: {0}", iconPath);
        }

        return iconPath;
    }
}

#endif  // LINUX_PLATFORM
