/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    using Com::Wui::Framework::ChromiumRE::Enums::ProcessType;

    const char kProcessType[] = "type";
    const char kRendererProcess[] = "renderer";
    const char kZygoteProcess[] = "zygote";

    ProcessType ClientApplication::GetProcessType(CefRefPtr<CefCommandLine> $commandLine) {
        // The command-line flag won't be specified for the browser process.
        if (!$commandLine->HasSwitch(kProcessType)) {
            return ProcessType::BrowserProcess;
        }

        const string &processType = $commandLine->GetSwitchValue(kProcessType);
        if (processType == kRendererProcess) {
            return ProcessType::RendererProcess;
        } else if (processType == kZygoteProcess) {
            return ProcessType::ZygoteProcess;
        } else {
            return ProcessType::OtherProcess;
        }
    }

    void ClientApplication::OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> $registrar) {
        RegisterCustomSchemes($registrar, cookieableSchemes);
    }

    void ClientApplication::RegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> $registrar, std::vector<CefString> &$cookiableSchemes) {
        SchemeTestCommon::RegisterCustomSchemes($registrar, $cookiableSchemes);
    }
}
