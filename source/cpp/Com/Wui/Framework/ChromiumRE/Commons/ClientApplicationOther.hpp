/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONOTHER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONOTHER_HPP_

namespace Com::Wui::Framework::ChromiumRE::Commons {
    /**
     * Client app implementation for other process types.
     */
    class ClientApplicationOther
            : public Com::Wui::Framework::ChromiumRE::Commons::ClientApplication {
     public:
        ClientApplicationOther();

     private:
        /**/IMPLEMENT_REFCOUNTING(ClientApplicationOther);
        DISALLOW_COPY_AND_ASSIGN(ClientApplicationOther);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONOTHER_HPP_
