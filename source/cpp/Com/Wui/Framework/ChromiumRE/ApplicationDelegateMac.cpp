/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#import "sourceFilesMap.hpp"

@implementation ApplicationDelegateMac

- (id)init {
    if (self = [super init]) {
        [NSApplication sharedApplication].delegate = self;

        [NSMenu setMenuBarVisible:YES];
    }

    return self;
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication*)__unused sender {  // NOLINT
    return NSTerminateNow;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)__unused application {  // NOLINT
    return NO;
}

@end

#endif  // MAC_PLATFORM
