/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "sourceFilesMap.hpp"

@implementation ApplicationHandlerMac

- (BOOL)isHandlingSendEvent {
    return currentlyHandlingSendEvent;
}

- (void)setHandlingSendEvent:(BOOL)handlingSendEvent {
    currentlyHandlingSendEvent = handlingSendEvent;
}

- (void)sendEvent:(NSEvent*)event {  // NOLINT
    CefScopedSendingEvent sendingEventScoper;

    [super sendEvent:event];
}

- (void)terminate:(id)__unused sender {
    Com::Wui::Framework::ChromiumRE::Browser::MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);
    Com::Wui::Framework::ChromiumRE::Browser::MainMessageLoop::Get()->Quit();
}

@end

#endif  // MAC_PLATFORM
