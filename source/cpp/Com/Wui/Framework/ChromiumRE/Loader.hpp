/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_LOADER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_LOADER_HPP_

namespace Com::Wui::Framework::ChromiumRE {
    class Loader : public Com::Wui::Framework::XCppCommons::Loader {
     public:
        /**
         * Contains application logic entry method.
         * @param $argc An argument count.
         * @param $argv An array of arguments.
         * @return Returns exit code.
         */
        static int Load(const int $argc, const char *$argv[]) {
            Com::Wui::Framework::ChromiumRE::Application application;
            return Com::Wui::Framework::XCppCommons::Loader::Load(application, $argc, $argv);
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_LOADER_HPP_
