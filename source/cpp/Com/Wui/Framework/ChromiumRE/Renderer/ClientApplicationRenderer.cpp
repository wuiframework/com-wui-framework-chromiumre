/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Renderer {
    ClientApplicationRenderer::ClientApplicationRenderer() {
        CreateDelegates(delegates);
    }

    void ClientApplicationRenderer::OnRenderThreadCreated(
            CefRefPtr<CefListValue> $extraInfo) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnRenderThreadCreated(this, $extraInfo);
        }
    }

    void ClientApplicationRenderer::OnWebKitInitialized() {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnWebKitInitialized(this);
        }
    }

    void ClientApplicationRenderer::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnBrowserCreated(this, $browser);
        }
    }

    void ClientApplicationRenderer::OnBrowserDestroyed(CefRefPtr<CefBrowser> $browser) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnBrowserDestroyed(this, $browser);
        }
    }

    CefRefPtr<CefLoadHandler> ClientApplicationRenderer::GetLoadHandler() {
        CefRefPtr<CefLoadHandler> loadHandler;
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end() && !loadHandler.get(); ++it) {
            loadHandler = (*it)->GetLoadHandler(this);
        }

        return loadHandler;
    }

    void ClientApplicationRenderer::OnContextCreated(CefRefPtr<CefBrowser> $browser,
                                                     CefRefPtr<CefFrame> $frame,
                                                     CefRefPtr<CefV8Context> $context) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnContextCreated(this, $browser, $frame, $context);
        }
    }

    void ClientApplicationRenderer::OnContextReleased(CefRefPtr<CefBrowser> $browser,
                                                      CefRefPtr<CefFrame> $frame,
                                                      CefRefPtr<CefV8Context> $context) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnContextReleased(this, $browser, $frame, $context);
        }
    }

    void ClientApplicationRenderer::OnUncaughtException(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                                        CefRefPtr<CefV8Context> $context,
                                                        CefRefPtr<CefV8Exception> $exception,
                                                        CefRefPtr<CefV8StackTrace> $stackTrace) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnUncaughtException(this, $browser, $frame, $context, $exception, $stackTrace);
        }
    }

    void ClientApplicationRenderer::OnFocusedNodeChanged(CefRefPtr<CefBrowser> $browser,
                                                         CefRefPtr<CefFrame> $frame,
                                                         CefRefPtr<CefDOMNode> $node) {
        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end(); ++it) {
            (*it)->OnFocusedNodeChanged(this, $browser, $frame, $node);
        }
    }

    bool ClientApplicationRenderer::OnProcessMessageReceived(CefRefPtr<CefBrowser> $browser, CefProcessId $sourceProcess,
                                                             CefRefPtr<CefProcessMessage> $message) {
        DCHECK_EQ($sourceProcess, PID_BROWSER);

        bool handled = false;

        DelegateSet::iterator it = delegates.begin();
        for (; it != delegates.end() && !handled; ++it) {
            handled = (*it)->OnProcessMessageReceived(this, $browser, $sourceProcess, $message);
        }

        return handled;
    }

    void ClientApplicationRenderer::CreateDelegates(DelegateSet &$delegates) {
        ClientRenderer::CreateDelegates($delegates);
    }
}
