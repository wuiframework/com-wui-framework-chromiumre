/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_RENDERER_CLIENTAPPLICATIONRENDERER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_RENDERER_CLIENTAPPLICATIONRENDERER_HPP_

namespace Com::Wui::Framework::ChromiumRE::Renderer {
    /**
     * Client app implementation for the renderer process.
     */
    class ClientApplicationRenderer
            : public Com::Wui::Framework::ChromiumRE::Commons::ClientApplication, public CefRenderProcessHandler {
     public:
        typedef std::set<CefRefPtr<Com::Wui::Framework::ChromiumRE::Interfaces::Delegates::IClientRendererDelegate>> DelegateSet;

        ClientApplicationRenderer();

     private:
        static void CreateDelegates(DelegateSet &$delegates);  // NOLINT

        CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override {
            return this;
        }

        void OnRenderThreadCreated(CefRefPtr<CefListValue> $extraInfo) override;

        void OnWebKitInitialized() override;

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserDestroyed(CefRefPtr<CefBrowser> $browser) override;

        CefRefPtr<CefLoadHandler> GetLoadHandler() override;

        void OnContextCreated(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                              CefRefPtr<CefV8Context> $context) override;

        void OnContextReleased(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                               CefRefPtr<CefV8Context> $context) override;

        void OnUncaughtException(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, CefRefPtr<CefV8Context> $context,
                                 CefRefPtr<CefV8Exception> $exception, CefRefPtr<CefV8StackTrace> $stackTrace) override;

        void OnFocusedNodeChanged(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                                  CefRefPtr<CefDOMNode> $node) override;

        bool OnProcessMessageReceived(CefRefPtr<CefBrowser> $browser, CefProcessId $sourceProcess,
                                      CefRefPtr<CefProcessMessage> $message) override;

     private:
        DelegateSet delegates;

        /**/IMPLEMENT_REFCOUNTING(ClientApplicationRenderer);
        DISALLOW_COPY_AND_ASSIGN(ClientApplicationRenderer);
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_RENDERER_CLIENTAPPLICATIONRENDERER_HPP_
