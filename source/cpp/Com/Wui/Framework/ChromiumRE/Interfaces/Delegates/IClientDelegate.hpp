/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTDELEGATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTDELEGATE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Interfaces::Delegates {
    /**
     * Implement this interface to receive notification of ClientHandler
     * events. The methods of this class will be called on the main thread.
     */
    class IClientDelegate {
     public:
        // Called when the browser is created.
        virtual void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) = 0;

        // Called when the browser is closing.
        virtual void OnBrowserClosing(CefRefPtr<CefBrowser> $browser) = 0;

        // Called when the browser has been closed.
        virtual void OnBrowserClosed(CefRefPtr<CefBrowser> $browser) = 0;

        // Set the window URL address.
        virtual void OnSetAddress(const string &$url) = 0;

        // Set the window title.
        virtual void OnSetTitle(const string &$title) = 0;

        // Set the Favicon image.
        virtual void OnSetFavicon(CefRefPtr<CefImage> $image) {}

        // Set fullscreen mode.
        virtual void OnSetFullscreen(bool $fullscreen) = 0;

        // Set the loading state.
        virtual void OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) = 0;

        // Set the draggable regions.
        virtual void OnSetDraggableRegions(const std::vector<CefDraggableRegion> &$regions) = 0;

     protected:
        virtual ~IClientDelegate() {}
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTDELEGATE_HPP_
