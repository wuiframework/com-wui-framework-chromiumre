/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IWINDOWDELEGATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IWINDOWDELEGATE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Interfaces::Delegates {
    /**
     * This interface is implemented by the owner of the RootWindow. The methods
     * of this class will be called on the main thread.
     */
    class IWindowDelegate {
     public:
        /**
         * Called to retrieve the CefRequestContext for browser. Only called for non-popup browsers.
         * @param $rootWindow Specify pointer to RootWindow of requested context.
         * @return Returns reference pointer to request context or NULL.
         */
        virtual CefRefPtr<CefRequestContext> GetRequestContext(Com::Wui::Framework::ChromiumRE::Browser::RootWindow *$rootWindow) = 0;

        /**
         * Called when the RootWindow has been destroyed.
         * @param $rootWindow A pointer to currently destroyed window.
         */
        virtual void OnRootWindowDestroyed(Com::Wui::Framework::ChromiumRE::Browser::RootWindow *$rootWindow) = 0;

     protected:
        virtual ~IWindowDelegate() = default;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IWINDOWDELEGATE_HPP_
