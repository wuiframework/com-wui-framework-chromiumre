/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IBROWSERWINDOWDELEGATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IBROWSERWINDOWDELEGATE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Interfaces::Delegates {
    /**
     *  This interface is implemented by the owner of the BrowserWindow.
     *  The methods of this class will be called on the main thread.
     */
    class IBrowserWindowDelegate {
     public:
        /**
         * Called when the browser has been created.
         * @param $browser Reference to browser from which delegate was called.
         */
        virtual void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) = 0;

        /**
         *  Called when the BrowserWindow has been destroyed.
         */
        virtual void OnBrowserWindowDestroyed() = 0;

        /**
         *  Set the window title.
         * @param $title Current window title.
         */
        virtual void OnSetTitle(const string &$title) = 0;

        /**
         * Set the loading state.
         * @param $isLoading True if loading, false if idle.
         * @param $canGoBack UNUSED.
         * @param $canGoForward UNUSED.
         */
        virtual void OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) = 0;

     protected:
        virtual ~IBrowserWindowDelegate() {}
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_IBROWSERWINDOWDELEGATE_HPP_
