/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTAPPLICATIONDELEGATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTAPPLICATIONDELEGATE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Interfaces::Delegates {
    /**
     * Interface for browser delegates. All Delegates must be returned via CreateDelegates. Do not perform work in the Delegate
     * constructor. See CefBrowserProcessHandler for documentation.
     */
    class IClientApplicationDelegate : public virtual CefBaseRefCounted {
        typedef Com::Wui::Framework::ChromiumRE::Browser::ClientApplicationBrowser ClientApplicationBrowser;

     public:
        virtual void OnBeforeCommandLineProcessing(CefRefPtr<ClientApplicationBrowser> $app,
                                                   CefRefPtr<CefCommandLine> $commandLine) {}

        virtual void OnContextInitialized(CefRefPtr<ClientApplicationBrowser> $app) {}

        virtual void OnBeforeChildProcessLaunch(CefRefPtr<ClientApplicationBrowser> $app,
                                                CefRefPtr<CefCommandLine> $commandLine) {}

        virtual void OnRenderProcessThreadCreated(CefRefPtr<ClientApplicationBrowser> $app,
                                                  CefRefPtr<CefListValue> $extraInfo) {}

     protected:
        virtual ~IClientApplicationDelegate() {}

        IMPLEMENT_REFCOUNTING(IClientApplicationDelegate)
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTAPPLICATIONDELEGATE_HPP_
