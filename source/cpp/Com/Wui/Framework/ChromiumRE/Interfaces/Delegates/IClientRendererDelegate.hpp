/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTRENDERERDELEGATE_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTRENDERERDELEGATE_HPP_

namespace Com::Wui::Framework::ChromiumRE::Interfaces::Delegates {
    /**
     * Interface for renderer delegates. All Delegates must be returned via CreateDelegates. Do not perform work in the Delegate
     * constructor. See CefRenderProcessHandler for documentation.
     */
    class IClientRendererDelegate : public virtual CefBaseRefCounted {
     public:
        typedef Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer ClientApplicationRenderer;

        virtual void OnRenderThreadCreated(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefListValue> $extraInfo) {}

        virtual void OnWebKitInitialized(CefRefPtr<ClientApplicationRenderer> $app) {}

        virtual void OnBrowserCreated(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser) {}

        virtual void OnBrowserDestroyed(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser) {}

        virtual CefRefPtr<CefLoadHandler> GetLoadHandler(
                CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app) {
            return nullptr;
        }

        virtual bool OnBeforeNavigation(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                        CefRefPtr<CefBrowser> $browser,
                                        CefRefPtr<CefFrame> $frame, CefRefPtr<CefRequest> $request,
                                        cef_navigation_type_t $navigationType,
                                        bool $isRedirect) {
            return false;
        }

        virtual void OnContextCreated(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                      CefRefPtr<CefBrowser> $browser,
                                      CefRefPtr<CefFrame> $frame,
                                      CefRefPtr<CefV8Context> $context) {}

        virtual void OnContextReleased(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                       CefRefPtr<CefBrowser> $browser,
                                       CefRefPtr<CefFrame> $frame,
                                       CefRefPtr<CefV8Context> $context) {}

        virtual void OnUncaughtException(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                         CefRefPtr<CefBrowser> $browser,
                                         CefRefPtr<CefFrame> $frame, CefRefPtr<CefV8Context> $context,
                                         CefRefPtr<CefV8Exception> $exception,
                                         CefRefPtr<CefV8StackTrace> $stackTrace) {}

        virtual void OnFocusedNodeChanged(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                          CefRefPtr<CefBrowser> $browser,
                                          CefRefPtr<CefFrame> $frame, CefRefPtr<CefDOMNode> $node) {}

        /**
         * Called when a process message is received. Return true if the message was handled and should not be passed on to other handlers.
         * Delegates should check for unique message names to avoid interfering with each other.
         */
        virtual bool OnProcessMessageReceived(CefRefPtr<Com::Wui::Framework::ChromiumRE::Renderer::ClientApplicationRenderer> $app,
                                              CefRefPtr<CefBrowser> $browser,
                                              CefProcessId $sourceProcess, CefRefPtr<CefProcessMessage> $message) {
            return false;
        }

     protected:
        virtual ~IClientRendererDelegate() {}

        IMPLEMENT_REFCOUNTING(IClientRendererDelegate)
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_INTERFACES_DELEGATES_ICLIENTRENDERERDELEGATE_HPP_
