/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_QUERYCONTENTHANDLER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_QUERYCONTENTHANDLER_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors {
    /**
     * QueryContentHandler class provides communication protocol for cef-query.
     */
    class QueryContentHandler {
     public:
        /**
         * Constructor, initiliazes member variables.
         */
        QueryContentHandler();

        /**
         * Parse data by reading them as JSON tree.
         * @param $data Data to read.
         */
        void Parse(const string &$data);

        /**
         * @return Return 'name' attribute of the tree.
         */
        const string &getName() const { return this->name; }

        /**
         * @param $name Specify name attribute.
         */
        void setName(const string &$name);

        /**
         * @return Returns parsed arguments in JSON.
         */
        const json &getArgs() const { return this->args; }

        /**
         * Set arguments based on given input value.
         * @param $val Input arguments.
         */
        void setArgs(const json &$val) { this->args = $val; }

        /**
         * @return Return return value in JSON.
         */
        const json &getReturnValue() const { return this->returnValue; }

        /**
         * Set return value based on input.
         * @param $val Input value to set.
         */
        void setReturnValue(const json &$val) { this->returnValue = $val; }

        /**
         * @return Returns query ID.
         */
        int getId() const;

        /**
         * @return Returns query client ID.
         */
        int getClientId() const;

        /**
         * @return Returns status.
         */
        int getStatus() const;

        /**
         * @param $status Specify status code.
         */
        void setStatus(int $status);

        /**
         * @return Returns query method type.
         */
        string getType() const;

        /**
         * @param $type Specify query method type.
         */
        void setType(string $type);

        /**
         * Stringify QueryContent to JSON string.
         * @return Returns stringified data.
         */
        string Stringify() const;

        /**
         * Stringify QueryContent from specified data property.
         * @param $data Specify data property.
         * @return Returns stringified data.
         */
        string Stringify(const string &$data) const;

     private:
        string type;
        string name;
        json args;
        json returnValue;
        int id;
        int clientId;
        string origin;
        int status;
        string token;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_QUERYCONTENTHANDLER_HPP_
