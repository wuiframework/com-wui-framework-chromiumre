/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Connectors {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Utils::ObjectDecoder;
    using Com::Wui::Framework::XCppCommons::Utils::ObjectEncoder;

    using std::stringstream;
    using boost::system::error_code;

    // cppcheck-suppress uninitMemberVar
    QueryContentHandler::QueryContentHandler()
            : type(""),
              name(""),
              args(json()),
              returnValue(json()),
              id(0),
              clientId(0) {
    }

    void QueryContentHandler::Parse(const string &$data) {
        try {
            json content = json::parse($data);
            string dataString = content.value("data", "");
            this->id = content.value("id", 0);
            this->type = content.value("type", "");
            this->clientId = content.value("clientId", 0);
            this->origin = "http://localhost";
            this->status = content.value("status", 0);
            this->token = content.value("token", "");

            if (!dataString.empty()) {
                dataString = ObjectDecoder::Base64(dataString);
                json data = json::parse(dataString);
                this->name = data.value("name", "");
                string argsString = data.value("args", "");
                boost::replace_all(dataString, "\"" + argsString + "\"", ObjectDecoder::Base64(argsString));

                data = json::parse(dataString);
                if (data.find("args") != data.end()) {
                    this->args = data["args"];
                } else {
                    this->args.clear();
                }
            }
        } catch (std::exception &ex) {
            LogIt::Error(ex);
        }
    }

    int QueryContentHandler::getId() const {
        return this->id;
    }

    int QueryContentHandler::getClientId() const {
        return this->clientId;
    }

    int QueryContentHandler::getStatus() const {
        return this->status;
    }

    void QueryContentHandler::setStatus(int $status) {
        this->status = $status;
    }

    string QueryContentHandler::getType() const {
        return this->type;
    }

    void QueryContentHandler::setType(string $type) {
        this->type = $type;
    }

    string QueryContentHandler::Stringify() const {
        json data = {{"name", this->name}};

        if (!this->returnValue.empty() && !(this->returnValue.is_array() && this->returnValue.empty())) {
            if (!this->returnValue.is_string()) {
                data["returnValue"] = ObjectEncoder::Base64(this->returnValue.dump());
            } else {
                data["returnValue"] = ObjectEncoder::Base64(this->returnValue);
            }
        } else {
            data["returnValue"] = this->returnValue.dump();
        }
        if (!this->args.empty() && !(this->args.is_array() && this->args.empty())) {
            data["args"] = ObjectEncoder::Base64(this->args.dump());
        } else {
            data["args"] = this->args.dump();
        }

        json output = {
                {"type",     this->type},
                {"id",       this->id},
                {"clientId", this->clientId},
                {"origin",   this->origin},
                {"status",   this->status},
                {"token",    this->token},
                {"data",     data}};

        LogIt::Debug("Response: {0}", output.dump());

        return output.dump();
    }

    string QueryContentHandler::Stringify(const string &$data) const {
        json output = {
                {"type",     this->type},
                {"id",       this->id},
                {"clientId", this->clientId},
                {"origin",   this->origin},
                {"status",   this->status},
                {"token",    this->token},
                {"data",     $data}};

        LogIt::Debug("Response: {0}", output.dump());

        return output.dump();
    }

    void QueryContentHandler::setName(const string &$name) {
        this->name = $name;
    }
}
