/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Connectors {
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryContentHandler;
    using Com::Wui::Framework::XCppCommons::Utils::ObjectEncoder;

    QueryResponse::QueryResponse(const shared_ptr<QueryContentHandler> $query,
                                 const CefRefPtr<CefMessageRouterBrowserSide::Callback> $callback)
            : query($query) {
        this->callback = $callback;
    }

    void QueryResponse::Send(bool $succeed, const json &$args) {
        this->send($succeed ? json(true) : json(false), $args);
    }

    void QueryResponse::Send(const string &$message, const json &$args) {
        this->send(json($message), $args);
    }

    void QueryResponse::Send(const json &$args) {
        this->send($args, json::array());
    }

    void QueryResponse::SendError(const std::exception &$exception) {
        this->SendError($exception.what());
    }

    void QueryResponse::SendError(string const &$message) {
        this->query->setStatus(500);
        this->query->setType("Request.Exception");
        this->callback->Success(this->query->Stringify($message));
    }

    const CefRefPtr<CefMessageRouterBrowserSide::Callback> &QueryResponse::getCallback() const {
        return this->callback;
    }

    void QueryResponse::setCallback(const CefRefPtr<CefMessageRouterBrowserSide::Callback> &$callback) {
        this->callback = $callback;
    }

    void QueryResponse::send(const json &$returnValue, const json &$args) {
        if (this->query) {
            this->query->setStatus(200);
            this->query->setReturnValue($returnValue);
            if ($args.empty() || $args.is_null() || ($args.is_array() && $args.size() == 0)) {
                this->query->setArgs(json::array());
            } else {
                this->query->setArgs($args);
            }
            string out = this->query->Stringify();
            this->callback->Success(out);
        }
    }

    void QueryResponse::SendData(const json &$data) {
        if (this->query) {
            string out = Com::Wui::Framework::XCppCommons::Utils::ObjectEncoder::Base64($data.dump());
            out = this->query->Stringify(out);
            this->callback->Success(out);
        }
    }

    void QueryResponse::SendSuccess(bool $succeed) {
        this->query->setStatus(200);
        this->SendData(json($succeed));
    }
}
