/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Connectors {

    using Com::Wui::Framework::ChromiumRE::Connectors::QueryContentHandler;
    using Com::Wui::Framework::ChromiumRE::Connectors::QueryResponse;

    using Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunner;
    using Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunnerLinux;
    using Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunnerMac;
    using Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunnerWin;
    using Com::Wui::Framework::ChromiumRE::Browser::MainContext;
    using Com::Wui::Framework::ChromiumRE::Browser::RootWindow;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Utils::ObjectDecoder;

    class EventSubscriber {
     public:
        shared_ptr<QueryContentHandler> protocol;
        shared_ptr<QueryResponse> connector;
    };

    static std::map<string, std::vector<EventSubscriber>> eventSubscribers;

    WindowHandler::WindowHandler() {
#ifdef WIN_PLATFORM
        this->runner.reset(new WindowTestRunnerWin());
#elif LINUX_PLATFORM
        this->runner.reset(new WindowTestRunnerLinux());
#elif MAC_PLATFORM
        this->runner.reset(new WindowTestRunnerMac());
#endif
    }

    void WindowHandler::RegisterMessageHandler(std::set<CefMessageRouterBrowserSide::Handler *> &$handlers) {
        $handlers.insert(new WindowHandler());
    }

    void WindowHandler::FireEvent(const string &$name, const json &$args) {
        if (eventSubscribers.find($name) != eventSubscribers.end()) {
            auto subscribers = eventSubscribers[$name];
            std::for_each(subscribers.begin(), subscribers.end(), [&](const EventSubscriber &$item) {
                $item.protocol->setStatus(200);
                $item.protocol->setType("FireEvent");
                $item.connector->SendData({{"name", $name},
                                           {"args", Com::Wui::Framework::XCppCommons::Utils::ObjectEncoder::Base64($args.dump())}});
            });
        }
    }

    bool WindowHandler::OnQuery(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, int64 $queryId,
                                const CefString &$request, bool $persistent,
                                CefRefPtr<CefMessageRouterBrowserSide::Callback> $callback) {
        bool isHandled = true;
        string request = $request.ToString();
        if (boost::iequals(MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->getId(), "root")) {
            LogIt::Debug("Processing query:\n{0}", request);
            shared_ptr<QueryContentHandler> queryHandler(new QueryContentHandler());
            queryHandler->Parse(request);

            $callback->Success("OK");

            string method = queryHandler->getName();
            json args = queryHandler->getArgs();
            shared_ptr<QueryResponse> queryResponse = std::make_shared<QueryResponse>(queryHandler, $callback);
            try {
                if (boost::iequals(queryHandler->getType(), "LiveContentWrapper.InvokeMethod")) {
                    if (!method.empty()) {
                        if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.MoveTo")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::MoveTo, $browser, args[0], args[1], queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Minimize")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Minimize, $browser, queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Maximize")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Maximize, $browser, queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Restore")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Restore, $browser, queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Resize")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Resize, $browser, args[0], args[1], args[2],
                                                                 queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.CanResize")) {
                            WindowTestRunner::CanResize($browser, args[0].get<bool>(), queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Open")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Open, $browser, args[0].get<string>(), args[1],
                                                                 queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.ScriptExecute")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::ScriptExecute, $browser, args[0].get<string>(), args[1],
                                                                 queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Close")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Close, $browser,
                                                                 args.size() > 0 ? args[0].get<string>() : "", queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.getCookies")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::getCookies, $browser,
                                                                 args.size() > 0 ? args[0].get<string>() : "", queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.CreateNotifyIcon")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::CreateNotifyIcon, $browser, args[0], queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.ModifyNotifyIcon")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::ModifyNotifyIcon, $browser, args[0], queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.DestroyNotifyIcon")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::DestroyNotifyIcon, $browser, queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Show")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Show, $browser, this->getDefaultId(args), queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.Hide")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::Hide, $browser, this->getDefaultId(args), queryResponse);
                        } else if (boost::iequals(method,
                                                  "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.setTaskBarProgressState")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::setTaskBarProgressState, $browser,
                                                                 Com::Wui::Framework::ChromiumRE::Enums::TaskBarProgressState::fromString(
                                                                     args[0]), queryResponse);
                        } else if (boost::iequals(method,
                                                  "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.setTaskBarProgressValue")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::setTaskBarProgressValue, $browser, args[0], args[1],
                                                                 queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.ShowFileDialog")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::ShowFileDialog, $browser, args[0], queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.getState")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::getWindowState, $browser, this->getDefaultId(args),
                                                                 queryResponse);
                        } else if (boost::iequals(method, "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler.ShowDebugConsole")) {
                            this->runner->InvokeWithThreadSafety(WindowTestRunner::ShowDebugConsole, $browser, queryResponse);
                        } else {
                            queryResponse->SendError("Unsupported method type \"" + queryHandler->getName() + "\".");
                            LogIt::Error("Received unknown function in request.\n{0}", queryHandler->getName());
                        }
                    } else {
                        queryResponse->SendError("Empty method field in query message.");
                        LogIt::Error("Empty method field in query message.\n{0}", request);
                    }
                } else if (boost::iequals(queryHandler->getType(), "AddEventListener")) {
                    if (eventSubscribers.find(queryHandler->getName()) == eventSubscribers.end()) {
                        eventSubscribers[queryHandler->getName()] = std::vector<EventSubscriber>();
                    }
                    eventSubscribers[queryHandler->getName()].push_back(
                            EventSubscriber{.protocol = queryHandler, .connector = queryResponse});
                    queryResponse->SendSuccess(true);
                } else {
                    queryResponse->SendError("Unsupported query type \"" + queryHandler->getType() + "\"");
                    LogIt::Error("Received unsupported query type: {0}.", queryHandler->getType());
                }
            } catch (std::exception &ex) {
                queryResponse->SendError(ex);
            }
        } else {
            LogIt::Debug("Sending script response:\n{0}", request);
            MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->SendScriptResponse(request);
        }

        return isHandled;
    }

    string WindowHandler::getDefaultId(const json &$args) const {
        if ($args.empty()) {
            return {};
        } else {
            return $args[0];
        }
    }
}
