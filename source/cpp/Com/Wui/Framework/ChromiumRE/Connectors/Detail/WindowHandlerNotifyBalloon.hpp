/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYBALLOON_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYBALLOON_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyBalloon class contains properties for NotifyBalloon included in NotifyIcon
     */
    class WindowHandlerNotifyBalloon {
        typedef Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType NotifyBalloonIconType;

     public:
        /**
         * Update NotifyBalloon properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        void Update(const json &$options);

        /**
         * @return Returns balloon title.
         */
        const string &getTitle() const;

        /**
         * @return Returns balloon message.
         */
        const string &getMessage() const;

        /**
         * @return Returns balloon icon type.
         */
        const NotifyBalloonIconType &getType() const;

        /**
         * @return Returns user icon path.
         */
        const string &getUserIcon() const;

        /**
         * @return Returns true if balloon sound is disabled, false otherwise.
         */
        bool isNoSound() const;

     private:
        string title = "";
        string message = "";
        NotifyBalloonIconType type = NotifyBalloonIconType::NONE;
        string userIcon = "";
        bool noSound = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYBALLOON_HPP_
