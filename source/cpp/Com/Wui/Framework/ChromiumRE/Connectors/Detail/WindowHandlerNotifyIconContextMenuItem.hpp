/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENUITEM_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENUITEM_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyIconContextMenuItem class contains properties of ContextMenuItem.
     */
    class WindowHandlerNotifyIconContextMenuItem {
     public:
        /**
         * ContextMenuItem properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        void Update(const json &$options);

        /**
         * @return Returns ContextMenuItem name. Name is used as unique identifier.
         */
        const string &getName() const;

        /**
         * @return Returns ContextMenuItem label.
         */
        const string &getLabel() const;

        /**
         * @return Returns ContextMenuItem position.
         */
        int getPosition() const;

     private:
        string name;
        string label;
        int position;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENUITEM_HPP_
