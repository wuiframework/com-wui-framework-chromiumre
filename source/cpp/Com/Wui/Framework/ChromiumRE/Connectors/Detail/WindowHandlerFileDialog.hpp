/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERFILEDIALOG_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERFILEDIALOG_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerFileDialog class contains properties of modal dialog to select file or directory.
     */
    class WindowHandlerFileDialog {
     public:
        /**
         * Update WindowHandlerFileDialog properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        void Update(const json &$options);

        /**
         * @return Returns dialog title.
         */
        const string &getTitle() const;

        /**
         * @param $title Specify new dialog title.
         */
        void setTitle(const string &$title);

        /**
         * @return Returns file extension filter. Unused for FolderOnly dialog mode.
         */
        const std::vector<string> &getFilter() const;

        /**
         * File format item could be in formats:
         * a) lower-cased MIME types (like "text/*", "image/*", ...)
         * b) individual file extension (like ".txt", ".zip", ...)
         * c) combined description and extension (like "Text files|.txt", "Raw bitmap|.bmp", ...)
         * NOTE: All files are added by default ("All Files|.*") and can not be turned off.
         * @param $filter Specify new file extension filter. Unused for FolderOnly dialog mode.
         */
        void setFilter(const std::vector<string> &$filter);

        /**
         * @return Returns default file extension filter zero based index.
         */
        int getFilterIndex() const;

        /**
         * @param $filterIndex Specify default file extension zero based index.
         */
        void setFilterIndex(int $filterIndex);

        /**
         * @return Returns default dialog path or filename.
         */
        const string &getPath() const;

        /**
         * If absolute path is specified property InitialDirectory is ignored. When relative path is selected than only last item
         * will be used like <initial-directory>/<last-path-item>.
         * @param $path Specify dialog path of filename.
         */
        void setPath(const string &$path);

        /**
         * @return Returns true if FolderOnly dialog mode is required, false otherwise.
         */
        bool isFolderOnly() const;

        /**
         * @param $folderOnly Specify true to require FolderOnly dialog mode, false otherwise.
         */
        void setFolderOnly(bool $folderOnly);

        /**
         * @return Returns true if multiple selected paths are allowed, false otherwise.
         */
        bool isMultiSelect() const;

        /**
         * @param $multiSelect Specify true to allow multiple selected paths, false otherwise.
         */
        void setMultiSelect(bool $multiSelect);

        /**
         * @return Returns initial directory.
         */
        const string &getInitialDirectory() const;

        /**
         * Last item of directory path must be ended with path separator, otherwise it will be detected as filename without extension
         * and thus parent path will be used as initial directory.
         * @param $initialDirectory Specify dialog initial directory (empty uses last directory entered by dialog before)
         */
        void setInitialDirectory(const string &$initialDirectory);

        /**
         * @return Returns true when dialog is limited to existing files, false otherwise.
         */
        bool isOpenOnly() const;

        /**
         * @param $openOnly Specify true to limit dialog to choose only existing files, false otherwise.
         */
        void setOpenOnly(bool $openOnly);

     private:
        string title = "";
        std::vector<string> filter = {};
        int filterIndex = 0;
        string path = "";
        bool folderOnly = false;
        bool multiSelect = false;
        string initialDirectory = "";
        bool openOnly = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERFILEDIALOG_HPP_
