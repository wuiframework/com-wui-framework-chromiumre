/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIcon;
using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyBalloon;
using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIconContextMenu;

void WindowHandlerNotifyIcon::Update(const json &$options) {
    if (!$options.empty()) {
        if ($options.find("tip") != $options.end()) {
            this->tip = $options["tip"];
        }
        if ($options.find("icon") != $options.end()) {
            this->icon = $options["icon"];
        }
        if ($options.find("balloon") != $options.end()) {
            if (!this->balloon) {
                this->balloon = std::make_unique<WindowHandlerNotifyBalloon>();
            }
            this->balloon->Update($options["balloon"]);
        }
        if ($options.find("contextMenu") != $options.end()) {
            if (!this->contextMenu) {
                this->contextMenu = std::make_unique<WindowHandlerNotifyIconContextMenu>();
            }
            this->contextMenu->Update($options["contextMenu"]);
        }
    }
}

const string &WindowHandlerNotifyIcon::getTip() const {
    return this->tip;
}

const string &WindowHandlerNotifyIcon::getIcon() const {
    return this->icon;
}

const unique_ptr<WindowHandlerNotifyBalloon> &WindowHandlerNotifyIcon::getBalloon() const {
    return this->balloon;
}

const unique_ptr<WindowHandlerNotifyIconContextMenu> &WindowHandlerNotifyIcon::getContextMenu() const {
    return this->contextMenu;
}
