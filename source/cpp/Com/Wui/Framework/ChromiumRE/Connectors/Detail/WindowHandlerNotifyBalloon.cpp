/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyBalloon;
using Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType;

void WindowHandlerNotifyBalloon::Update(const json &$options) {
    if (!$options.empty()) {
        if ($options.find("title") != $options.end()) {
            this->title = $options["title"];
        }
        if ($options.find("message") != $options.end()) {
            this->message = $options["message"];
        }
        nlohmann::json::const_iterator item = $options.find("type");
        if (item != $options.end()) {
            if (item.value().is_string()) {
                string tmp = $options.value("type", "");
                if (!tmp.empty()) {
                    this->type = Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<NotifyBalloonIconType>::fromString(
                            boost::to_upper_copy(tmp));
                }
            } else if (item.value().is_number()) {
                switch (item.value().get<int>()) {
                    case 1:
                        this->type = NotifyBalloonIconType::INFO;
                        break;
                    case 2:
                        this->type = NotifyBalloonIconType::WARNING;
                        break;
                    case 3:
                        this->type = NotifyBalloonIconType::ERROR;
                        break;
                    case 4:
                        this->type = NotifyBalloonIconType::USER;
                        break;
                    default:
                        this->type = NotifyBalloonIconType::NONE;
                        break;
                }
            }
        }
        if ($options.find("userIcon") != $options.end()) {
            this->userIcon = $options["userIcon"];
        }
        if ($options.find("noSound") != $options.end()) {
            this->noSound = $options["noSound"];
        }
    }
}

const string &WindowHandlerNotifyBalloon::getTitle() const {
    return this->title;
}

const string &WindowHandlerNotifyBalloon::getMessage() const {
    return this->message;
}

const WindowHandlerNotifyBalloon::NotifyBalloonIconType &WindowHandlerNotifyBalloon::getType() const {
    return this->type;
}

const string &WindowHandlerNotifyBalloon::getUserIcon() const {
    return this->userIcon;
}

bool WindowHandlerNotifyBalloon::isNoSound() const {
    return this->noSound;
}
