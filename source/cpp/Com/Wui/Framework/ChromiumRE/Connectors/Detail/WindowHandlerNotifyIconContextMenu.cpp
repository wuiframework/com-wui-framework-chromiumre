/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIconContextMenu;
using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIconContextMenuItem;
using Com::Wui::Framework::XCppCommons::Utils::LogIt;

void WindowHandlerNotifyIconContextMenu::Update(const json &$options) {
    if (!$options.empty()) {
        if ($options.find("items") != $options.end()) {
            json tmpItems = $options["items"];
            if (!tmpItems.empty() && tmpItems.is_array()) {
                this->items.clear();
                for (json::const_iterator it = tmpItems.cbegin(); it != tmpItems.cend(); ++it) {
                    WindowHandlerNotifyIconContextMenuItem item;
                    if (it.value().is_object()) {
                        item.Update(it.value());
                        this->items.push_back(item);
                    } else {
                        LogIt::Warning("NotifyIcon-ContextMenu item configuration is not object. ({0})", it.value().dump());
                    }
                }
            }
        }
    }
}

const std::vector<WindowHandlerNotifyIconContextMenuItem> &WindowHandlerNotifyIconContextMenu::getItems() const {
    return items;
}
