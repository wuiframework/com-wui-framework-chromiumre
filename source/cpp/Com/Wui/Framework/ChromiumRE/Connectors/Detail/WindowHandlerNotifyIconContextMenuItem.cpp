/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIconContextMenuItem;

void WindowHandlerNotifyIconContextMenuItem::Update(const json &$options) {
    if (!$options.empty()) {
        if ($options.find("name") != $options.end()) {
            this->name = $options["name"];
        }
        if ($options.find("label") != $options.end()) {
            this->label = $options["label"];
        }
        if ($options.find("position") != $options.end()) {
            this->position = $options["position"];
        }
    }
}

const string &WindowHandlerNotifyIconContextMenuItem::getName() const {
    return this->name;
}

const string &WindowHandlerNotifyIconContextMenuItem::getLabel() const {
    return this->label;
}

int WindowHandlerNotifyIconContextMenuItem::getPosition() const {
    return this->position;
}
