/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENU_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENU_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyIconContextMenu class contains properties for ContextMenu included in NotifyIcon
     */
    class WindowHandlerNotifyIconContextMenu {
     public:
        /**
         * Update ContextMenu properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        void Update(const json &$options);

        /**
         * @return Returns list of ContextMenu items.
         */
        const std::vector<WindowHandlerNotifyIconContextMenuItem> &getItems() const;

     private:
        std::vector<WindowHandlerNotifyIconContextMenuItem> items;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICONCONTEXTMENU_HPP_
