/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICON_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICON_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyIcon class contains properties of NotifyIcon.
     */
    class WindowHandlerNotifyIcon {
     public:
        /**
         * Update NotifyIcon properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        void Update(const json &$options);

        /**
         * @return Returns NotifyIcon tip.
         */
        const string &getTip() const;

        /**
         * @return Returns NotifyIcon icon file path.
         */
        const string &getIcon() const;

        /**
         * @return Returns pointer to NotifyBalloon.
         */
        const unique_ptr<WindowHandlerNotifyBalloon> &getBalloon() const;

        /**
         * @return Returns pointer to context menu instance defined for NotifyIcon.
         */
        const unique_ptr<WindowHandlerNotifyIconContextMenu> &getContextMenu() const;

     private:
        string tip;
        string icon;
        unique_ptr<WindowHandlerNotifyBalloon> balloon;
        unique_ptr<Com::Wui::Framework::ChromiumRE::Connectors::Detail::WindowHandlerNotifyIconContextMenu> contextMenu;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_DETAIL_WINDOWHANDLERNOTIFYICON_HPP_
