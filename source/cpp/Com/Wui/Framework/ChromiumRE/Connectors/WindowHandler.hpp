/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_WINDOWHANDLER_HPP_
#define COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_WINDOWHANDLER_HPP_

namespace Com::Wui::Framework::ChromiumRE::Connectors {
    /**
     * WindowHandler class provides event manager for cef-query.
     */
    class WindowHandler : public CefMessageRouterBrowserSide::Handler {
     public:
        /**
         * Register cef-query handler used by WindowHandler.
         * @param $handlers Register cef-query handler(s).
         */
        static void RegisterMessageHandler(std::set<CefMessageRouterBrowserSide::Handler *> &$handlers);

        /**
         * Fire event.
         * @param $name Specify event name.
         * @param $args Specify event arguments.
         */
        static void FireEvent(const string &$name, const json &$args = json());

     public:
        /**
         * Constructs default WindowHandler.
         */
        WindowHandler();

        /**
         * This method is called by chromium core when cef-query received. Contains request resolver implementation.
         * @param $browser Instance of browser.
         * @param $frame Instance of frame in browser.
         * @param $queryId Idendifier of current query.
         * @param $request Request string.
         * @param $persistent True if persistent, false if cef-query channel will be closed
         * after cef-query::Succes/Failed() has been called.
         * @param $callback Cef-query callback.
         * @return Returns true if succeed, false otherwise.
         */
        bool OnQuery(CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame, int64 $queryId,
                     const CefString &$request, bool $persistent, CefRefPtr<Callback> $callback) override;

     private:
        string getDefaultId(const json &$args) const;

        scoped_ptr<Com::Wui::Framework::ChromiumRE::Browser::WindowTestRunner> runner;
    };
}

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_CONNECTORS_WINDOWHANDLER_HPP_
