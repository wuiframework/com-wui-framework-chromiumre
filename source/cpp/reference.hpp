/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_CHROMIUMRE_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_CHROMIUMRE_HPP_

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"

#ifdef WIN_PLATFORM
#include <commdlg.h>  // NOLINT
#include <Shobjidl.h>
#endif  // WIN_PLATFORM

#include "interfacesMap.hpp"
#include "Com/Wui/Framework/ChromiumRE/sourceFilesMap.hpp"

// global-cef-includes-start
#include "include/cef_app.h"
#include "include/cef_base.h"
#include "include/cef_stream.h"
#include "include/cef_browser.h"
#include "include/cef_client.h"
#include "include/cef_task.h"
#include "include/cef_render_handler.h"
#include "include/cef_image.h"
#include "include/cef_request.h"
#include "include/cef_response.h"
#include "include/cef_response_filter.h"
#include "include/cef_cookie.h"
#include "include/cef_scheme.h"
#include "include/cef_parser.h"
#include "include/cef_browser_process_handler.h"
#include "include/cef_command_line.h"
#include "include/base/cef_scoped_ptr.h"
#include "include/base/cef_lock.h"
#include "include/base/cef_ref_counted.h"
#include "include/base/cef_thread_checker.h"
#include "include/base/cef_bind.h"
#include "include/base/cef_build.h"
#include "include/wrapper/cef_helpers.h"
#include "include/wrapper/cef_message_router.h"
#include "include/wrapper/cef_resource_manager.h"
#include "include/wrapper/cef_closure_task.h"
#include "include/internal/cef_types_wrappers.h"
// global-cef-includes-stop

#include "Com/Wui/Framework/ChromiumRE/Enums/WindowStateType.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/CookieElement.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/ClientApplication.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/WindowPosition.hpp"
#include "Com/Wui/Framework/ChromiumRE/Enums/NotifyBalloonIconType.hpp"
#include "Com/Wui/Framework/ChromiumRE/Enums/TaskBarProgressState.hpp"

// generated-code-start
#include "Com/Wui/Framework/ChromiumRE/Application.hpp"
#include "Com/Wui/Framework/ChromiumRE/ApplicationDelegateMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/ApplicationHandlerMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/BorderlessWindowMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/BrowserWindow.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/BrowserWindowStdLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/BrowserWindowStdMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/BrowserWindowStdWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/ClientApplicationBrowser.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/ClientHandler.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/ClientHandlerStd.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/ClientHandlerStdLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/ClientTypes.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/DebugWindowWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/DialogHandlerLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/MainContext.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/MainContextImpl.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/MainMessageLoop.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/MainMessageLoopStd.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/NotifyIcon.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/NotifyIconLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/NotifyIconMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/NotifyIconWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/RootWindow.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/RootWindowLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/RootWindowMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/RootWindowManager.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/RootWindowWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/TaskBar.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/TaskBarMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/TaskBarWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/WindowTestRunner.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/WindowTestRunnerLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/WindowTestRunnerMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Browser/WindowTestRunnerWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/ClientApplicationHybrid.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/ClientApplicationOther.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/Configuration.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/CookieVisitor.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/DialogCallback.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/ErrorHandlerLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/SchemeTestCommon.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/SourceVisitor.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/TestCookieCompletionCallback.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/TestSetCookieCallback.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/UtilLinux.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/UtilMac.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/UtilWin.hpp"
#include "Com/Wui/Framework/ChromiumRE/Commons/WaitableEvent.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/Detail/WindowHandlerFileDialog.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/Detail/WindowHandlerNotifyBalloon.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/Detail/WindowHandlerNotifyIcon.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/Detail/WindowHandlerNotifyIconContextMenu.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/Detail/WindowHandlerNotifyIconContextMenuItem.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/QueryContentHandler.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/QueryResponse.hpp"
#include "Com/Wui/Framework/ChromiumRE/Connectors/WindowHandler.hpp"
#include "Com/Wui/Framework/ChromiumRE/Enums/ProcessType.hpp"
#include "Com/Wui/Framework/ChromiumRE/Enums/WindowCornerType.hpp"
#include "Com/Wui/Framework/ChromiumRE/Interfaces/Delegates/IBrowserWindowDelegate.hpp"
#include "Com/Wui/Framework/ChromiumRE/Interfaces/Delegates/IClientApplicationDelegate.hpp"
#include "Com/Wui/Framework/ChromiumRE/Interfaces/Delegates/IClientDelegate.hpp"
#include "Com/Wui/Framework/ChromiumRE/Interfaces/Delegates/IClientRendererDelegate.hpp"
#include "Com/Wui/Framework/ChromiumRE/Interfaces/Delegates/IWindowDelegate.hpp"
#include "Com/Wui/Framework/ChromiumRE/Loader.hpp"
#include "Com/Wui/Framework/ChromiumRE/Renderer/ClientApplicationRenderer.hpp"
#include "Com/Wui/Framework/ChromiumRE/Renderer/ClientRenderer.hpp"
#include "Com/Wui/Framework/ChromiumRE/Structures/ChromiumArgs.hpp"
// generated-code-end

#endif  // COM_WUI_FRAMEWORK_CHROMIUMRE_HPP_  NOLINT
