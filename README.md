# com-wui-framework-chromiumre v2019.0.0

> Cross-platform runtime environment for WUI framework based on chromium.

## Requirements

This application depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

This library has some special requirements:

* 7zip - for more information see the [readme.txt](resource/libs/7zip/readme.txt) file.
* winsdk - for more information see the [README.md](resource/libs/winsdk/README.md) file.

Other requirements are managed automatically through dependency manager:

* [com-wui-framework-xcppcommons](https://bitbucket.org/wuiframework/com-wui-framework-xcppcommons)
* [cef](https://bitbucket.org/chromiumembedded/cef) - a simple framework for embedding Chromium-based browser in other applications.

## Project build

This project build is fully automated. For more information about the project build, 
see the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

An interface with batch/bash scripts is prepared for Windows/Linux users for all common project tasks:

* `install`
* `build`
* `clean`
* `test`
* `run`
* `hotdeploy`
* `docs`

> NOTE: All batch scripts are stored in the **./bin/batch** (or ./bin/bash for linux) sub-folder in the project root folder.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command from the {projectRoot}/bin/batch folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.0.0
Update to 64bit version and general support for all platforms.
### v2018.1.0
Added ability to specify initial maximized state by configuration file. Change of cache path to local APPDATA.
### v2018.0.0
Added external link library according to XCppCommons update. Changed version format. 
### v2.2.2
Added support for window resize and drag over CEF-query. Window is shown after page load. Bug fix for window maximize state.
### v2.2.1
Added support for manage of native directory browser dialog.
### v2.2.0
Fixed minimize/maximize/restore functionalities for Win7.
### v2.1.1
Internal configuration file formats migrated from XML to JSONP. Added CLI options for query and hash attributes.
### v2.1.0
Enums updated to respect changes in BaseEnum.
### v2.0.3
Integration with novel XCppCommons. Minor source code cleanup.
### v2.0.2
Fixed configuration for XCppCommons dependency.
### v2.0.1
Syntax update. Added TaskBar progress support.
### v2.0.0
Namespaces refactoring.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Michal Kelnar, 
Copyright (c) 2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
