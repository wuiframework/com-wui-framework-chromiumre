# CEF to chromiumRE patch for MinGW.

>  This patch allows to build CEF project with MinGW without any direct changes inside CEF project files.

## Requirements

This library requires the Chromium Embedded Framework ([CEF](https://bitbucket.org/chromiumembedded/cef)) binary distribution.

Currently supported/tested builds of CEF:
* v1.1.0: 3.3396.1786.gd3e36d0
* v1.0.0: 3.2704

## Documentation

This patch contains all necessary overridden files of original CEF project to allow build with MinGW toolchain.

## History

### v1.1.0
Updated CEF compatibility [CEF branch 3396](https://bitbucket.org/chromiumembedded/cef/wiki/BranchesAndBuilding).

### v1.0.0
Initial release. Compatible with [CEF branch 2704](https://bitbucket.org/chromiumembedded/cef/wiki/BranchesAndBuilding).

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Authors Michal Kelnar, Karel Burda, Copyright (c) 2016 [Freescale Semiconductor, Inc.](http://freescale.com/), Copyright (c) 2018 [NXP](http://nxp.com/)
