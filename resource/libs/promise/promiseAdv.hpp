#pragma once
#ifndef INC_PROMISE_FULL_HPP_
#define INC_PROMISE_FULL_HPP_

/*
 * Promise API implemented by cpp as Javascript promise style 
 *
 * Copyright (c) 2016, xhawk18
 * at gmail.com
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <memory>
#include <typeinfo>
#include <utility>
#include <algorithm>
#include <iterator>
#include <exception>
#include <vector>

#include "promise/std_addon.hpp"
#include "promise/misc.hpp"
#include "promise/debug.hpp"
#include "promise/stack.hpp"
#include "promise/list.hpp"
#include "promise/allocator.hpp"
#include "promise/any.hpp"

namespace promise {

template<typename T>
struct tuple_remove_reference {
    typedef typename std::remove_reference<T>::type r_type;
    typedef typename std::remove_cv<r_type>::type type;
};

template<typename T, std::size_t SIZE>
struct tuple_remove_reference<T[SIZE]> {
    typedef typename tuple_remove_reference<const T *>::type type;
};

template<typename TUPLE>
struct remove_reference_tuple {
    static const std::size_t size_ = std::tuple_size<TUPLE>::value;

    template<size_t SIZE, std::size_t... I>
    struct converted {
        typedef std::tuple<typename tuple_remove_reference<typename std::tuple_element<I, TUPLE>::type>::type...> type;
    };

    template<std::size_t... I>
    static converted<size_, I...> get_type(const std::index_sequence<I...> &) {
        return converted<size_, I...>();
    }

    typedef decltype(get_type(std::make_index_sequence<size_>())) converted_type;
    typedef typename converted_type::type type;
};

template<typename ...ARG>
struct arg_traits {
    typedef std::tuple<ARG...> type2;
    typedef typename remove_reference_tuple<type2>::type type;
    enum { direct_any = false };
};

template<>
struct arg_traits<pm_any &> {
    typedef pm_any type;
    enum { direct_any = true };
};

template<typename FUNC>
struct func_traits_impl {
    typedef decltype(&FUNC::operator()) func_type;
    typedef typename func_traits_impl<func_type>::ret_type ret_type;
    typedef typename func_traits_impl<func_type>::arg_type arg_type;
    enum { direct_any = func_traits_impl<func_type>::direct_any };
};

template<typename RET, class T, typename ...ARG>
struct func_traits_impl< RET(T::*)(ARG...) const > {
    typedef RET ret_type;
    typedef typename arg_traits<ARG...>::type arg_type;
    enum { direct_any = arg_traits<ARG...>::direct_any };
};

template<typename RET, typename ...ARG>
struct func_traits_impl< RET(*)(ARG...) > {
    typedef RET ret_type;
    typedef typename arg_traits<ARG...>::type arg_type;
    enum { direct_any = arg_traits<ARG...>::direct_any };
};

template<typename RET, typename ...ARG>
struct func_traits_impl< RET(ARG...) > {
    typedef RET ret_type;
    typedef typename arg_traits<ARG...>::type arg_type;
    enum { direct_any = arg_traits<ARG...>::direct_any };
};

template<typename FUNC>
struct func_traits {
    typedef typename func_traits_impl<FUNC>::ret_type ret_type;
    enum { direct_any = func_traits_impl<FUNC>::direct_any };
    typedef typename func_traits_impl<FUNC>::arg_type arg_type;
};



template<typename RET, typename FUNC, bool direct_any_arg, std::size_t ...I>
struct call_tuple_t {
    typedef typename func_traits<FUNC>::arg_type func_arg_type;
    typedef typename remove_reference_tuple<std::tuple<RET>>::type ret_type;

    static ret_type call(const FUNC &func, pm_any &arg) {
        func_arg_type new_arg(*reinterpret_cast<typename std::tuple_element<I, func_arg_type>::type *>(arg.tuple_element(I))...);
        arg.clear();
        return ret_type(func(std::get<I>(new_arg)...));
    }
};

template<typename RET, typename FUNC, std::size_t ...I>
struct call_tuple_t<RET, FUNC, true, I...> {
    typedef typename func_traits<FUNC>::arg_type func_arg_type;
    typedef typename remove_reference_tuple<std::tuple<RET>>::type ret_type;

    static ret_type call(const FUNC &func, pm_any &arg) {
        return ret_type(func(arg));
    }
};

template<typename FUNC, std::size_t ...I>
struct call_tuple_t<void, FUNC, false, I...> {
    typedef typename func_traits<FUNC>::arg_type func_arg_type;
    typedef std::tuple<> ret_type;

    static std::tuple<> call(const FUNC &func, pm_any &arg) {
        func_arg_type new_arg(*reinterpret_cast<typename std::tuple_element<I, func_arg_type>::type *>(arg.tuple_element(I))...);
        arg.clear();
        func(std::get<I>(new_arg)...);
        return std::tuple<>();
    }
};

template<typename FUNC, std::size_t ...I>
struct call_tuple_t<void, FUNC, true, I...> {
    typedef typename func_traits<FUNC>::arg_type func_arg_type;
    typedef std::tuple<> ret_type;

    static std::tuple<> call(const FUNC &func, pm_any &arg) {
        func(arg);
        return std::tuple<>();
    }
};


template<typename RET>
struct call_tuple_ret_t {
    typedef typename remove_reference_tuple<std::tuple<RET>>::type ret_type;
};

template<>
struct call_tuple_ret_t<void> {
    typedef std::tuple<> ret_type;
};

template<typename FUNC, std::size_t ...I>
inline auto call_tuple_as_argument(const FUNC &func, pm_any &arg, const std::index_sequence<I...> &)
    -> typename call_tuple_ret_t<typename func_traits<FUNC>::ret_type>::ret_type
{
    typedef typename func_traits<FUNC>::ret_type ret_type;
    enum { direct_any = func_traits<FUNC>::direct_any };

    return call_tuple_t<ret_type, FUNC, (direct_any ? true : false), I...>::call(func, arg);
}

template<typename FUNC>
inline bool verify_func_arg(const FUNC &func, pm_any &arg) {
    typedef typename func_traits<FUNC>::arg_type func_arg_type;
    type_tuple<func_arg_type> tuple_func;

    if (arg.tuple_size() < tuple_func.size_) {
        return false;
    }

    for (size_t i = tuple_func.size_; i-- != 0; ) {
        if (arg.tuple_type(i) != tuple_func.tuple_type(i)
            && arg.tuple_type(i) != tuple_func.tuple_rcv_type(i)) {
            return false;
        }
    }

    return true;
}

template<typename FUNC>
inline auto call_func(const FUNC &func, pm_any &arg)
    -> typename call_tuple_ret_t<typename func_traits<FUNC>::ret_type>::ret_type
{
    typedef typename func_traits<FUNC>::arg_type func_arg_type;

    return call_tuple_as_argument(func, arg, std::make_index_sequence<type_tuple<func_arg_type>::size_>());
}

struct Bypass {};
struct Promise;

template<typename T>
class pm_shared_ptr_promise {
    typedef pm_shared_ptr_promise Defer;
public:
    ~pm_shared_ptr_promise() {
        pm_allocator::dec_ref(object_);
    }

    explicit pm_shared_ptr_promise(T *object)
        : object_(object) {
    }

    explicit pm_shared_ptr_promise()
        : object_(nullptr) {
    }

    pm_shared_ptr_promise(pm_shared_ptr_promise const &ptr)
        : object_(ptr.object_) {
        pm_allocator::add_ref(object_);
    }

    Defer &operator=(Defer const &ptr) {
        Defer(ptr).swap(*this);
        return *this;
    }

    bool operator==(Defer const &ptr) const {
        return object_ == ptr.object_;
    }

    bool operator!=(Defer const &ptr) const {
        return !(*this == ptr);
    }

    bool operator==(T const *ptr) const {
        return object_ == ptr;
    }

    bool operator!=(T const *ptr) const {
        return !(*this == ptr);
    }

    inline T *operator->() const {
        return object_;
    }

    inline T *obtain_rawptr() {
        pm_allocator::add_ref(object_);
        return object_;
    }

    inline void release_rawptr() {
        pm_allocator::dec_ref(object_);
    }

    void clear() {
        Defer().swap(*this);
    }

    template <typename ...RET_ARG>
    void resolve(const RET_ARG &... ret_arg) const {
        object_->template resolve<RET_ARG...>(ret_arg...);
    }

    Defer then(Defer &promise) {
        return object_->then(promise);
    }

    template <typename FUNC_ON_RESOLVED>
    Defer then(FUNC_ON_RESOLVED on_resolved) const {
        return object_->template then<FUNC_ON_RESOLVED>(on_resolved);
    }

private:
    inline void swap(Defer &ptr) {
        std::swap(object_, ptr.object_);
    }

    T *object_;
};

typedef pm_shared_ptr_promise<Promise> Defer;

typedef void(*FnSimple)();

template <typename RET, typename FUNC>
struct ResolveChecker;
template<typename ARG_TYPE, typename FUNC>
struct ExCheck;

inline Defer newHeadPromise(void);

struct PromiseCaller{
    virtual ~PromiseCaller(){};
    virtual Defer call(Defer &self, Promise *caller) = 0;
};

template <typename FUNC_ON_RESOLVED>
struct ResolvedCaller
    : public PromiseCaller{
    typedef typename func_traits<FUNC_ON_RESOLVED>::ret_type resolve_ret_type;
    FUNC_ON_RESOLVED on_resolved_;

    ResolvedCaller(const FUNC_ON_RESOLVED &on_resolved)
        : on_resolved_(on_resolved){}

    virtual Defer call(Defer &self, Promise *caller) {
        return ResolveChecker<resolve_ret_type, FUNC_ON_RESOLVED>::call(on_resolved_, self, caller);
    }
};

struct Promise {
    Defer next_;
    pm_stack::itr_t prev_;
    pm_any any_;
    PromiseCaller *resolved_;

    enum status_t {
        kInit       = 0,
        kResolved   = 1,
        kFinished   = 2
    };
    uint8_t status_      ;

    Promise(const Promise &) = delete;
    explicit Promise()
        : next_(nullptr)
        , prev_(pm_stack::ptr_to_itr(nullptr))
        , resolved_(nullptr)
        , status_(kInit)
        {
    }

    virtual ~Promise() {
        clear_func();
        if (next_.operator->()) {
            next_->prev_ = pm_stack::ptr_to_itr(nullptr);
        }
    }

    template <typename RET_ARG>
    void prepare_resolve(const RET_ARG &ret_arg) {
        if (status_ != kInit) return;
        status_ = kResolved;
        any_ = ret_arg;
    }

    template <typename ...RET_ARG>
    void resolve(const RET_ARG &... ret_arg) {
        typedef typename remove_reference_tuple<std::tuple<RET_ARG...>>::type arg_type;
        prepare_resolve(arg_type(ret_arg...));
        if(status_ == kResolved)
            call_next();
    }

    Defer call_resolve(Defer &self, Promise *caller){
        if(resolved_ == nullptr){
            self->prepare_resolve(caller->any_);
            return self;
        }
        Defer ret = resolved_->call(self, caller);
        if(ret != self)
            joinDeferObject(self, ret);
        return ret;
    }

    void clear_func() {
        pm_delete(resolved_);
        resolved_ = nullptr;
    }

    template <typename FUNC>
    void run(FUNC func, Defer d) {
        try {
            func(d);
        } catch(const bad_any_cast &ex) {
            //TODO d->reject(ex);
        } catch(...) {
            //TODO d->reject(std::current_exception());
        }
    }

    Defer call_next() {
        if(status_ == kResolved) {
            if(next_.operator->()){
                pm_allocator::add_ref(this);
                status_=kInit;
                Defer d = next_->call_resolve(next_, this);
                pm_allocator::dec_ref(this);
                return d;
            }
        }

        return next_;
    }


    Defer then_impl(PromiseCaller *resolved){
        Defer promise = newHeadPromise();
        promise->resolved_ = resolved;
        return then(promise);
    }

    Defer then(Defer &promise) {
        joinDeferObject(this, promise);
        return call_next();
    }

    template <typename FUNC_ON_RESOLVED>
    Defer then(const FUNC_ON_RESOLVED &on_resolved) {
        return then_impl(static_cast<PromiseCaller *>(pm_new<ResolvedCaller<FUNC_ON_RESOLVED>>(on_resolved)));
    }

    static Promise *get_head(Promise *p){
        while(p){
            Promise *prev = static_cast<Promise *>(pm_stack::itr_to_ptr(p->prev_));
            if(prev == nullptr) break;
            p = prev;
        }
        return p;
    }
    static Promise *get_tail(Promise *p){
        while(p){
            Defer &next = p->next_;
            if(next.operator->() == nullptr) break;
            p = next.operator->();
        }
        return p;
    }

    static inline void joinDeferObject(Promise *self, Defer &next){
        if(next.operator->() != nullptr) {
            Promise *head = get_head(next.operator->());
            Promise *tail = get_tail(next.operator->());

            if (self->next_.operator->()) {
                self->next_->prev_ = pm_stack::ptr_to_itr(reinterpret_cast<void *>(tail));
            }
            tail->next_ = self->next_;
            pm_allocator::add_ref(head);
            self->next_ = Defer(head);
            head->prev_ = pm_stack::ptr_to_itr(reinterpret_cast<void *>(self));
        }
    }

    static inline void joinDeferObject(Defer &self, Defer &next){
        joinDeferObject(self.operator->(), next);
    }
};


template <typename RET, typename FUNC>
struct ResolveChecker {
    static Defer call(const FUNC &func, Defer &self, Promise *caller) {
        try {
            if (verify_func_arg(func, caller->any_))
                self->prepare_resolve(call_func(func, caller->any_));
            else {
                // todo self->prepare_reject(caller->any_);
            }
        } catch(...) {
            // TODO(B58790) self->prepare_reject(std::current_exception());
        }
        return self;
    }
};

template <typename FUNC>
struct ResolveChecker<Defer, FUNC> {
    static Defer call(const FUNC &func, Defer &self, Promise *caller) {
        try {
            //std::cout<<"verify_func_args"<<std::endl;
            if (verify_func_arg(func, caller->any_)) {
                Defer ret = std::get<0>(call_func(func, caller->any_));
                return ret;
            }
            else {
                // TODO(B58790) self->prepare_reject(caller->any_);
                return self;
            }
        } catch(...) {
            // TODO(B58790) self->prepare_reject(std::current_exception());
            return self;
        }
    }
};

template <typename FUNC>
struct ResolveChecker<Bypass, FUNC> {
    static Defer call(const FUNC &func, Defer &self, Promise *caller) {
        try {
            pm_any any = caller->any_;
            func(caller);
            self->prepare_resolve(any);
        }
        catch (...) {
            // TODO(B58790) self->prepare_reject(std::current_exception());
        }
        return self;
    }
};

template <typename RET>
struct ResolveChecker<RET, FnSimple> {
    static Defer call(const FnSimple &func, Defer &self, Promise *caller) {
        try {
            if (func == nullptr)
                self->prepare_resolve(caller->any_);
            else if (verify_func_arg(func, caller->any_))
                self->prepare_resolve(call_func(func, caller->any_));
            else {
                // TODO(B58790) self->prepare_reject(caller->any_);
            }
        } catch(...) {
            // TODO(B58790) self->prepare_reject(std::current_exception());
        }
        return self;
    }
};

inline Defer newHeadPromise(){
    return Defer(pm_new<Promise>());
}

/* Create new promise object */
template <typename FUNC>
inline Defer newPromise(FUNC func) {
    Defer promise = newHeadPromise();
    promise->run(func, promise);
    return promise;
}

/* Return a resolved promise directly */
template <typename ...RET_ARG>
inline Defer resolve(const RET_ARG &... ret_arg){
    return newPromise([=](Defer &d){ d.resolve(ret_arg...); });
}

}


#endif
