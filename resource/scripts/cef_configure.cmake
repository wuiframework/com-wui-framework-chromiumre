# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

if (POLICY CMP0074)
    cmake_policy(SET CMP0074 NEW)
endif ()

macro(CEF_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running CEF_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")
    PARSE_ARGS(attributes)

    set(CEF_ROOT ${CMAKE_SOURCE_DIR}/${path})
    if (IS_ABSOLUTE ${path})
        set(CEF_ROOT ${path})
    endif ()
    set(CEF_PATCH_ROOT ${CMAKE_SOURCE_DIR}/resource/libs/cef-patch)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CEF_PATCH_ROOT}/cmake)
    set(CEF_INCLUDE_DIRS ${CEF_ROOT} ${CEF_PATCH_ROOT})

    if (APPLE)
        set(PROJECT_ARCH "x86_64")
    endif ()

    find_package(CEF REQUIRED)

    if (OS_WINDOWS)
        set(CEF_LIBTYPE STATIC)
    else ()
        set(CEF_LIBTYPE SHARED)
    endif ()

    if (OS_WINDOWS)
        set(CEF_COMPILER_FLAGS
            -include ${CEF_PATCH_ROOT}/include/internal/cef_string_wrappers.h
            -include ${CEF_ROOT}/include/base/cef_atomicops.h
            -include ${CEF_PATCH_ROOT}/include/base/internal/cef_atomicops_x86_gcc.h
            -I ${CEF_ROOT}
            -I ${CEF_PATCH_ROOT})
    endif ()

    string(REPLACE ";" " " __flags1 "${CMAKE_C_FLAGS} ${CEF_COMPILER_FLAGS} ${CEF_C_COMPILER_FLAGS}")
    set(CMAKE_C_FLAGS ${__flags1})

    string(REPLACE ";" " " __flags2 "${CMAKE_C_FLAGS_DEBUG} ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_C_COMPILER_FLAGS_DEBUG}")
    set(CMAKE_C_FLAGS_DEBUG ${__flags2})

    string(REPLACE ";" " " __flags3 "${CMAKE_C_FLAGS_RELEASE}${CEF_COMPILER_FLAGS_RELEASE} ${CEF_C_COMPILER_FLAGS_RELEASE}")
    set(CMAKE_C_FLAGS_RELEASE ${__flags3})

    string(REPLACE ";" " " __flags4 "${CMAKE_CXX_FLAGS} ${CEF_COMPILER_FLAGS} ${CEF_CXX_COMPILER_FLAGS}")
    set(CMAKE_CXX_FLAGS ${__flags4})

    string(REPLACE ";" " " __flags5 "${CMAKE_CXX_FLAGS_DEBUG} ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_CXX_COMPILER_FLAGS_DEBUG}")
    set(CMAKE_CXX_FLAGS_DEBUG ${__flags5})

    string(REPLACE ";" " " __flags6 "${CMAKE_CXX_FLAGS_RELEASE} ${CEF_COMPILER_FLAGS_RELEASE} ${CEF_CXX_COMPILER_FLAGS_RELEASE}")
    set(CMAKE_CXX_FLAGS_RELEASE ${__flags6})

    string(REPLACE ";" " " __flags7 "${CMAKE_EXE_LINKER_FLAGS} ${CEF_LINKER_FLAGS} ${CEF_EXE_LINKER_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS ${__flags7})

    string(REPLACE ";" " " __flags8 "${CMAKE_EXE_LINKER_FLAGS_DEBUG} ${CEF_LINKER_FLAGS_DEBUG} ${CEF_EXE_LINKER_FLAGS_DEBUG}")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG ${__flags8})

    string(REPLACE ";" " " __flags9 "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${CEF_LINKER_FLAGS} ${CEF_SHARED_LINKER_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE ${__flags9})

    string(REPLACE ";" " " __flags10 "${CMAKE_SHARED_LINKER_FLAGS} ${CEF_LINKER_FLAGS} ${CEF_SHARED_LINKER_FLAGS}")
    set(CMAKE_SHARED_LINKER_FLAGS ${__flags10})

    string(REPLACE ";" " " __flags11 "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} ${CEF_LINKER_FLAGS_DEBUG} ${CEF_SHARED_LINKER_FLAGS_DEBUG}")
    set(CMAKE_SHARED_LINKER_FLAGS_DEBUG ${__flags11})

    string(REPLACE ";" " " __flags12 "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} ${CEF_LINKER_FLAGS_RELEASE} ${CEF_SHARED_LINKER_FLAGS_RELEASE}")
    set(CMAKE_SHARED_LINKER_FLAGS_RELEASE ${__flags12})

    FILE(GLOB_RECURSE CEF_SOURCE_FILES ${CEF_ROOT}/include/*.cc ${CEF_ROOT}/libcef_dll/*.cc)
    FILE(GLOB_RECURSE CEF_HEADER_FILES ${CEF_ROOT}/include/*.h ${CEF_ROOT}/libcef_dll/*.h ${CEF_PATCH_ROOT}/include/*.h)
    if (OS_WINDOWS)
        FILE(GLOB_RECURSE CEF_HEADER_PATCH_FILES ${CEF_PATCH_ROOT}/include/*.h)
        list(APPEND CEF_HEADER_FILES "${CEF_HEADER_PATCH_FILES}")
    endif ()

    add_library(libcef_dll_wrapper ${CEF_SOURCE_FILES} ${CEF_HEADER_FILES})
    set_target_properties(libcef_dll_wrapper PROPERTIES PREFIX "")
    target_include_directories(libcef_dll_wrapper PUBLIC ${CEF_INCLUDE_DIRS})

    ADD_LOGICAL_TARGET("libcef_lib" "${CEF_LIB_DEBUG}" "${CEF_LIB_RELEASE}")

    if (APPLE)
        find_library(_framework_cef
                NAMES "Chromium Embedded Framework"
                PATHS ${CEF_BINARY_DIR_RELEASE}
                NO_DEFAULT_PATH
                NO_CMAKE_ENVIRONMENT_PATH
                NO_CMAKE_PATH
                NO_SYSTEM_ENVIRONMENT_PATH
                NO_CMAKE_SYSTEM_PATH
                NO_CMAKE_FIND_ROOT_PATH)

        target_link_libraries(${app_target} ${_framework_cef})
        target_link_libraries(${test_target} ${_framework_cef})
        target_link_libraries(${test_lib_target} ${_framework_cef})

        COPY_FRAMEWORK(${_framework_cef} ${app_target})
        COPY_FRAMEWORK(${_framework_cef} ${test_target})
        COPY_FRAMEWORK(${_framework_cef} ${test_lib_target})
    else ()
        COPY_FILES(${app_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")
        COPY_FILES(${test_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")
        COPY_FILES(${test_lib_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")
    endif ()

    target_include_directories(${app_target} PUBLIC ${CEF_INCLUDE_DIRS})
    target_include_directories(${test_target} PUBLIC ${CEF_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} PUBLIC ${CEF_INCLUDE_DIRS})

    target_link_libraries(${app_target} libcef_dll_wrapper)
    target_link_libraries(${test_target} libcef_dll_wrapper)
    target_link_libraries(${test_lib_target} libcef_dll_wrapper)

    if (NOT APPLE)
        target_link_libraries(${app_target} libcef_lib)
        target_link_libraries(${test_target} libcef_lib)
        target_link_libraries(${test_lib_target} libcef_lib)
    endif ()

    add_dependencies(${app_target} libcef_dll_wrapper)
    add_dependencies(${test_target} libcef_dll_wrapper)
    add_dependencies(${test_lib_target} libcef_dll_wrapper)
endmacro()
