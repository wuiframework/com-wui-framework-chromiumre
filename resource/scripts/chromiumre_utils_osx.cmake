# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(_osx_add_post_build_steps _target)
    message(STATUS "Adding post build steps for target ${_target} on OS X")

    set(_working_dir "${CMAKE_SOURCE_DIR}/build/target/${_target}.app/Contents/MacOS")

    add_custom_command(
            TARGET ${_target}
            POST_BUILD
            COMMAND
            install_name_tool -change
            "@rpath/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework"
            "@executable_path/../Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework"
            ${_working_dir}/${_target}
            WORKING_DIRECTORY ${_working_dir}
            COMMENT "Running install_name_tool"
            VERBATIM
            USES_TERMINAL)
endmacro()

macro(_osx_add_target_properties _target)
    message(STATUS "Setting specific target properties for target ${_target} on OS X")

    set(_icns_path ${CMAKE_SOURCE_DIR}/resource/graphics/icon.icns)
    _osx_convert_ico_to_icns(${CMAKE_SOURCE_DIR}/resource/graphics/icon.ico ${_icns_path})

    set_target_properties(${_target}
            PROPERTIES
                MACOSX_BUNDLE_INFO_PLIST ${CMAKE_SOURCE_DIR}/resource/configs/Info.OSX.plist
                MACOSX_BUNDLE TRUE
                RESOURCE ${_icns_path})

    target_sources(${_target} PRIVATE ${_icns_path})
    set_source_files_properties(${_icns_path} PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
endmacro()

macro(_osx_convert_ico_to_icns _ico _icns)
    message(STATUS "Converting ICO file (${_ico}) to ICNS (${_icns})")

    set(_result 0)
    execute_process(COMMAND sips -s format icns ${_ico} --out ${_icns}
                    RESULT_VARIABLE _result)

    if (NOT _result EQUAL 0)
        message(WARNING "Conversion to ICNS failed with exit code ${_result}")
    endif ()
endmacro()
