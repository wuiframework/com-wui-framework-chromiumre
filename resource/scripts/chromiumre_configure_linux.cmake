# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

find_package(PkgConfig REQUIRED)
# Have to use gtk2 because of the CEF, otherwise CreateBrowser() with windowInfo.SetAsChild() crashes
pkg_check_modules(GTK2 REQUIRED gtk+-2.0>=2.0.0)
pkg_check_modules(X11 REQUIRED x11)
pkg_check_modules(appindicator REQUIRED appindicator-0.1)

list(APPEND _includeDirectories
        ${GTK2_INCLUDE_DIRS}
        ${appindicator_INCLUDE_DIRS})

list(APPEND _compileDefinitions OUT_OF_MEM=ENOMEM)

list(APPEND _libraries
        ${GTK2_LIBRARIES}
        ${X11_LIBRARIES}
        ${appindicator_LIBRARIES}
        pthread
        rt)

# This is a work-around for known issue in CMake where *sometimes* randomly the CMake on Linux fails to find threads package,
# was trying to solve this issues properly, but to no avail
set(CMAKE_THREAD_LIBS_INIT -lpthread)
