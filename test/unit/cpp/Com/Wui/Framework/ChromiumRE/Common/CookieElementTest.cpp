/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Commons {
    class CookieElementTest : public ::testing::Test {
     protected:
        virtual void SetUp() {
        }

        virtual void TearDown() {
        }
    };

    TEST_F(CookieElementTest, name_prop) {
        CookieElement<int> testElement(new CefCookie());
        testElement.setName("name");
        string a = testElement.getName();
        ASSERT_STREQ("name", a.c_str());
    }

    TEST_F(CookieElementTest, value_prop_int) {
        CookieElement<int> testElement(new CefCookie());
        testElement.setValue(101);
        ASSERT_EQ(101, testElement.getValue());
    }

    TEST_F(CookieElementTest, value_prop_bool) {
        CookieElement<bool> testElement(new CefCookie());
        testElement.setValue(true);
        ASSERT_EQ(true, testElement.getValue());
    }

}  // namespace Com::Wui::Framework::ChromiumRE::Common
