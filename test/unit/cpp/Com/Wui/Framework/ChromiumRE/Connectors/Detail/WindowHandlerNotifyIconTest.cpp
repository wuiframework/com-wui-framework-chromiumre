/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    class WindowHandlerNotifyIconTest : public ::testing::Test {
     protected:
        virtual void SetUp() {}

        virtual void TearDown() {}
    };

    TEST_F(WindowHandlerNotifyIconTest, Update) {
        json balloon = {
                {"title",    "__title__"},
                {"message",  "__message__"},
                {"type",     "info"},
                {"userIcon", "__userIcon__"},
                {"noSound",  true}
        };
        json menu = {
                {"items", json::array(
                        {
                                {
                                        {"name", "__name1__"},
                                        {"label", "__label1__"},
                                        {"position", 2}
                                },
                                {
                                        {"name", "__name2__"},
                                        {"label", "__label2__"},
                                        {"position", 4}
                                }
                        })
                }
        };
        json options = {
                {"tip",  "__tip__"},
                {"icon", "__icon__"}
        };

        WindowHandlerNotifyIcon notifyIcon;
        notifyIcon.Update(options);
        ASSERT_STREQ("__tip__", notifyIcon.getTip().c_str());
        ASSERT_STREQ("__icon__", notifyIcon.getIcon().c_str());
        ASSERT_FALSE(notifyIcon.getBalloon());
        ASSERT_FALSE(notifyIcon.getContextMenu());

        options["balloon"] = balloon;
        options["contextMenu"] = menu;
        notifyIcon.Update(options);

        ASSERT_STREQ("__tip__", notifyIcon.getTip().c_str());
        ASSERT_STREQ("__icon__", notifyIcon.getIcon().c_str());
        ASSERT_STREQ("__title__", notifyIcon.getBalloon()->getTitle().c_str());
        ASSERT_STREQ("__message__", notifyIcon.getBalloon()->getMessage().c_str());
        ASSERT_STREQ("__userIcon__", notifyIcon.getBalloon()->getUserIcon().c_str());
        ASSERT_TRUE(notifyIcon.getBalloon()->isNoSound());
        ASSERT_EQ(Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType::INFO, notifyIcon.getBalloon()->getType());
        ASSERT_EQ(2, static_cast<int>(notifyIcon.getContextMenu()->getItems().size()));
        ASSERT_STREQ("__name1__", notifyIcon.getContextMenu()->getItems()[0].getName().c_str());
        ASSERT_STREQ("__label2__", notifyIcon.getContextMenu()->getItems()[1].getLabel().c_str());
        ASSERT_EQ(4, notifyIcon.getContextMenu()->getItems()[1].getPosition());
    }
}  // namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail
