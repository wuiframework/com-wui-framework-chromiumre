/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::ChromiumRE::Enums::NotifyBalloonIconType;

namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail {
    class WindowHandlerNotifyBalloonTest : public ::testing::Test {
     protected:
        virtual void SetUp() {}

        virtual void TearDown() {}
    };

    TEST_F(WindowHandlerNotifyBalloonTest, Update) {
        json options = {
                {"title",    "__title__"},
                {"message",  "__message__"},
                {"type",     "error"},
                {"userIcon", "__userIcon__"},
                {"noSound",  true}
        };

        WindowHandlerNotifyBalloon balloon;
        ASSERT_STREQ("", balloon.getTitle().c_str());
        ASSERT_STREQ("", balloon.getMessage().c_str());
        ASSERT_EQ(NotifyBalloonIconType::NONE, balloon.getType());
        ASSERT_STREQ("", balloon.getUserIcon().c_str());
        ASSERT_FALSE(balloon.isNoSound());

        balloon.Update(options);
        ASSERT_STREQ("__title__", balloon.getTitle().c_str());
        ASSERT_STREQ("__message__", balloon.getMessage().c_str());
        ASSERT_EQ(NotifyBalloonIconType::ERROR, balloon.getType());
        ASSERT_STREQ("__userIcon__", balloon.getUserIcon().c_str());
        ASSERT_TRUE(balloon.isNoSound());
    }
}  // namespace Com::Wui::Framework::ChromiumRE::Connectors::Detail
